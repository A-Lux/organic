<?php $__env->startSection('body'); ?>
  <!-- MAIN -->
  <main class="main">
    <!-- BREAD CRUMBS -->
    <?php if(session()->has('deleted')): ?>
<script type="text/javascript">
  sweetalert('success','Товар успешно удален с избранных!',3000);
</script>
<?php endif; ?>
    <!-- END BREAD CRUMBS -->
        <ul class="bread-crumbs container">
      <li><a href="/">Главная</a></li>
      <li><a href="#">Избранные</a></li>
    </ul>
    <!-- ORGANIC CATALOG -->
    <div class="organic-catalog">
      <div class="container">
        <div class="catalog-section">
          <div class="title">
            <h3>Избранные товары</h3>
          </div>
        </div>
              <?php
              $i=1;
              ?>

              <?php if(empty($products)): ?>

              <p style="color: red;">Нет товаров!</p>
              
              <?php endif; ?>

        <div class="catalog-wrapper">
          <div class="catalog_right-side">
                        <div class="catalog-products-wrapper">
                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="catalog-product-item">
                          <div class="catalog-img">
                            <a href="/product-card/<?php echo e($product->id); ?>"><img src="/storage/<?php echo e($product->cover); ?>"></a>
                            <?php if(!empty($product->discount)): ?>
                            <span class="catalog-stock">-<?php echo e($product->discount); ?></span>
                            <?php endif; ?>
                          </div>
                          <h4><a href="/product-card/<?php echo e($product->id); ?>"><?php echo e($product->name); ?></a> / <?php echo e($product->volume); ?> мл.</h4>
                          <p><?php echo e($product->text_cover); ?></p>
                           <?php if(!empty($product->s_price)): ?>
                          <div class="price"><?php echo e($product->s_price); ?> тнг
                            <span>
                            <?php echo e($product->f_price); ?> тнг
                            </span>
                          </div>
                          <?php else: ?>
                          <div class="price"><?php echo e($product->f_price); ?> тнг </div>
                          <?php endif; ?>
                          
                          <?php 
                          $rating = \App\Product::calculateRating($product->id);
                          ?>
                          <div class="star-line">
                            <ul>
                              <?php for($i = 1; $i <= 5; $i++): ?>
                              <?php if($i <= $rating): ?>
                              <li style="color: #eeb900">&#9733;</li>
                              <?php else: ?>
                              <li style="color: #d4d4d4;">&#9733;</li>
                              <?php endif; ?>
                              <?php endfor; ?>
                            </ul>
                          </div>
                          <button class="btn-basket" name="btn-basket"     id="<?php echo e($product->id); ?>"   onclick="add_basket(this.id)">
                            В КОРЗИНУ
                          </button>
                          <a class="delete" style="position: relative; top:1rem;" href="/del_like?id=<?php echo e($product->id); ?>">Удалить</a>
                        </div>

                        <?php

                        unset($products[$key]);
                        if($i==8){
                        break;
                        }
                        else{
                        $i++;
                        }
                        
                        ?>
                        
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </div>
        <!-- БЛОК ПОКАЗАТЬ ЕЩЕ -->
         <?php if(!empty($products)): ?>
        <div class="catalog-products-wrapper-more" id="moreProduct" style="display: none;">
          <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="catalog-product-item">
                          <div class="catalog-img">
                            <a href="/product-card/<?php echo e($product->id); ?>"><img src="/storage/<?php echo e($product->cover); ?>"></a>
                            <?php if(!empty($product->discount)): ?>
                            <span class="catalog-stock">-<?php echo e($product->discount); ?></span>
                            <?php endif; ?>
                          </div>
                          <h4><a href="/product-card/<?php echo e($product->id); ?>"><?php echo e($product->name); ?></a> / <?php echo e($product->volume); ?> мл.</h4>
                          <p><?php echo e($product->text_cover); ?></p>
                           <?php if(!empty($product->s_price)): ?>
                          <div class="price"><?php echo e($product->s_price); ?> тнг
                            <span>
                            <?php echo e($product->f_price); ?> тнг
                            </span>
                          </div>
                          <?php else: ?>
                          <div class="price"><?php echo e($product->f_price); ?> тнг </div>
                          <?php endif; ?>
                          
                          <?php 
                          $rating = \App\Product::calculateRating($product->id);
                          ?>
                          <div class="star-line">
                            <ul>
                              <?php for($i = 1; $i <= 5; $i++): ?>
                              <?php if($i <= $rating): ?>
                              <li style="color: #eeb900">&#9733;</li>
                              <?php else: ?>
                              <li style="color: #d4d4d4;">&#9733;</li>
                              <?php endif; ?>
                              <?php endfor; ?>
                            </ul>
                          </div>
                          <button class="btn-basket" name="btn-basket"     id="<?php echo e($product->id); ?>"   onclick="add_basket(this.id)">
                            В КОРЗИНУ
                          </button>
                          <a class="delete" href="/del_like?id=<?php echo e($product->id); ?>" style="position: relative; top:1rem;">Удалить</a>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
                   
                      <div class="show-all-products">
                        <a href="#moreProduct" class="more-btn" onclick="show_hide(this.name)" name='0' id="show_more">ПОКАЗАТЬ ЕЩЁ</a>
                      </div>
           <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    <!-- END ORGANIC CATALOG -->


    <!-- SUSCRIPTION BLOCK -->
    <div class="white-bg-wrapp">
      <div class="subscription-block container wow rotateInUpLeft" data-wow-duration="1s">
        <div class="subscription-absolute">
          <div class="subscription-content">
            <h3>Хотите быть в курсе всех новостей?</h3>
            <span>Подпишитесь на рассылку!</span>
            <div class="subscription-wrapp">
              <div class="sub-social">
                <h4>Следите за нами в соц сетях:</h4>
                <ul class="social-networks">
                   <?php $__currentLoopData = $links; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li><a href="<?php echo e($link->link); ?>"><img src="/images/<?php echo e($link->type); ?>.png"></a></li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
              <div class="sub-row">
                <div class="sub-button-input">
                  <form action="/subscription" method="POST">
                    <?php echo csrf_field(); ?>
                    <input type="text" name="email" placeholder="Введите Электронную почту">
                    <input class="sub-btn" type="submit" value="ПОДПИСАТЬСЯ">
                  </form>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END SUSCRIPTION BLOCK -->
  </main>
  <!-- END MAIN -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make("layout", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\organic\resources\views/likes.blade.php ENDPATH**/ ?>