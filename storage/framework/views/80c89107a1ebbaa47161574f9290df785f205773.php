<?php $__env->startSection('body'); ?>
 <!-- MAIN -->
  <main class="main">
    <!-- BREAD CRUMBS -->
    <ul class="bread-crumbs container">
      <li><a href="/home">Личный кабинет</a></li>
      <li><a href="#">Заказы</a></li>
    </ul>
    <!-- END BREAD CRUMBS -->

    <!-- PURCHASE HISTORY -->
    <div class="purchase-history">
      <div class="container">
        <div class="title">
          <h3>История заказов</h3>
        </div>
        <div class="purchase-history__table">
          <ul>
            <li>
              <span>№ заказа</span>
              <span>Дата заказа</span>
              <span>Дата отправки</span>
              <span>Статус</span>
              <span>Итого</span>
            </li>
            <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li>
              <span class="order-btn-more" data-tab="order<?php echo e($loop->iteration); ?>">#<?php echo e($order->id); ?></span>
              <span><?php echo e($order->created_at); ?></span>
              <span><?php echo e($order->send_date); ?></span>
              <span><?php echo e(config('app.status')[$order->status]); ?></span>
              <span><?php echo e($order->sum); ?> тг</span>
            </li>
            <ul id="order<?php echo e($loop->iteration); ?>" style="display: none;">
              <li>
                <span></span>
                <span>№ заказа</span>
                <span>Наименование</span>
                <span>Количество</span>
                <span>Цена за шт.</span>
                <span>Итого</span>
              </li>

              <?php $__currentLoopData = $order->products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <span></span>
                  <span>#<?php echo e($order->id); ?></span>
                  <span><?php echo e($product->name); ?></span>
                  <span><?php echo e($order->kol_vo); ?></span>
                  <span><?php echo e($product->f_price); ?> тг.</span>
                  <span><?php echo e($product->f_price * $order->kol_vo); ?> тг.</span>
                </li>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </ul>
        </div>
        <a class="prev-btn" href="<?php echo e(route('profile')); ?>">Назад</a>
      </div>
    </div>
    <!-- END PURCHASE HISTORY -->
  </main>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layout", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/japananimetime/Projects/organic/resources/views/purchase-history.blade.php ENDPATH**/ ?>