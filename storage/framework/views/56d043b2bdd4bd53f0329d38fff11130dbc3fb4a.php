<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8">
    <title>Organic Market</title>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/multirange.css">
    <link rel="stylesheet" href="/css/select-option.min.css">
    <link rel="stylesheet" type="text/css" href="https://atuin.ru/demo/ui-slider/jquery-ui.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/easyzoom.css">
    <link rel="stylesheet" href="/css/pygments.css">
    <link rel="stylesheet" href="/css/style.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
    <style>
      .srav .comparison-table__feature-name {
        width: 12rem;
      }

      .srav .table {
        border: 1px dashed silver;
        font-family: 'Raleway', sans-serif;
      }

      .srav .table th,
      .srav .table td {
        border: 1px dashed silver;
      }

      .srav .table thead td {
        border: 0;
      }

      .srav .table td {
        width: 25rem;
      }

      .srav .comparison-table__product-title .comparison-table__product {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
      }

      .srav .comparison-table__product-buy {
        background: #d2451f;
        padding: 8px 15px;
        -webkit-border-radius: 30px;
        border-radius: 30px;
        color: #fff;
        text-decoration: none;
        border: 2px solid #d2451f;
      }

      .srav .comparison-table__product-buy:hover {
        -webkit-transition: .3s ease-in-out;
        -o-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
        background: #fff;
        color: #d2451f;
        text-shadow: none;
        cursor: pointer;
      }

      .srav .comparison-table__product-title .comparison-table__product img {
        width: 10rem;
        height: 10rem;
      }

      .srav .comparison-table__product-title {
        position: relative;
      }

      .srav .comparison-table__product-title .comparison-table__product .comparison-table__product-name {
        text-align: center;
      }

      .srav .comparison-table__product-title .comparison-table__product-delete {
        display: block;
        position: absolute;
        top: 0;
        right: 0;
        background: #d2451f;
        padding: 8px 15px;
        -webkit-border-radius: 30px;
                border-radius: 30px;
        color: #fff;
        text-decoration: none;
        border: 2px solid #d2451f;
      }
      .srav .comparison-table__product-title .comparison-table__product-delete:hover {
        -webkit-transition: .3s ease-in-out;
            -o-transition: .3s ease-in-out;
            transition: .3s ease-in-out;
            background: #fff;
            color: #d2451f;
            text-shadow: none;
            cursor: pointer;
      }
      .srav .compras-srav-head{
          height: 20rem;
        }
      @media  only screen and (max-width: 576px){
        .srav .compras-srav-head{
          height: 23rem;
        }
      }

      .comprasion h3{
        font-family: 'raleway black', sans-serif;
        font-size: 25px;
      }
      .comprasion p{
        font-family: 'raleway black', sans-serif;
        font-weight: 700;
        margin-top: 10px;
        margin-bottom: 0;
        font-size: 14px;
      }
      .comprasion .comprasion__box{
        margin-top: 20px;
        padding: 20px;
      }
      .comprasion .comprasion__box .comprasion__head ul li {
        font-family: 'raleway black', sans-serif
      }
      .comprasion .comprasion__box .comprasion__head img{
        margin: 30px 40%;
      }
      .comprasion .comprasion__box .comprasion__body ul li{
        font-family: 'raleway black', sans-serif;
        margin-top: 15px;
        position: relative;
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;
      }
      .comprasion .comprasion__box .comprasion__body  ul li span{
        text-align: right;
        width:23rem;

      }
      .comprasion .comprasion__box .comprasion__footer ul{
        display: flex;
        position: relative;
        justify-content: flex-end;
      }
      .comprasion .comprasion__box .comprasion__footer ul li .in-basket{
        display: block;
        border:1px solid #6d604c;
        color: #6d604c;
        background-color: #fff;
        padding: 10px 40px;
        border-radius: 25px;
        font-family: 'raleway black', sans-serif;
        font-weight: 700;
        font-size: 15px;
      }
      .comprasion .comprasion__box .comprasion__footer{
        margin-top: 20px;
      }
      .comprasion .comprasion__box .comprasion__footer ul li .in-basket:hover{
        color: #fff;
        background-color: #6d604c;
        text-decoration: none;
      }
      .comprasion .comprasion__box .comprasion__footer ul li .delete{
        background-color: #dc1029;
        color: #fff;
        font-family: 'raleway black', sans-serif;
        border-radius: 25px;
        padding: 10px 20px;
        font-weight: 700;
        font-size: 15px;
        float: right;
      }
      .comprasion .comprasion__box .comprasion__footer ul li .delete:hover{
        color: #dc1029;
        background-color: #fff;
        border:1px solid #dc1029;
        text-decoration: none;

      }
      .comprasion .comprasion__box .comprasion__footer ul li{
        margin-right: 20px;

      }
      .ya-share2__list {
          display: inline-block;
          vertical-align: top;
          padding: 0;
          margin: 0;
          list-style-type: none;
          margin-left: 12px !important;
      }
      .ya-share2__link:hover{
        background-color: white !important;
      }
      /*.ya-share2__link .ya-share2__badge {
          background-color: #f6f6f6 !important;
          padding: 14px;
      }
      .ya-share2__container_size_m .ya-share2__icon {
          height: 13px !important;
          width: 24px;
          background-size: 24px 13px !important;
      }
      .ya-share2__item_service_vkontakte .ya-share2__icon {
          background: url('/images/vk.png') !important;
          background-repeat: no-repeat !important;
      }
      .ya-share2__item_service_facebook .ya-share2__icon {
          background: url('/images/fb.png') !important;
          background-repeat: no-repeat !important;
      }
      .ya-share2__item_service_twitter .ya-share2__icon {
          background: url('/images/tw.png') !important;
          background-repeat: no-repeat !important;
      }*/
    </style>
    <script src="//code.jivosite.com/widget/teIS6i6A9i" async></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="/js/main.js" defer></script>
    <script type="text/javascript">
      function catalog_show(amount, id){
        switch(id){
          case "amount50":
            $('#amount100').removeClass('active');
            $('#amount200').removeClass('active');
            $('#amount50').addClass('active');
            break;
          case "amount100":
            $('#amount100').addClass('active');
            $('#amount200').removeClass('active');
            $('#amount50').removeClass('active');
            break;
          case "amount200":
            $('#amount100').removeClass('active');
            $('#amount200').addClass('active');
            $('#amount50').removeClass('active');
            break;
          default: break;
        }
        $.get('/set/amount/catalog/items/'+amount).done(function(){
          window.location.replace(document.getElementById('currentURL').innerHTML);
        })
      }
      function sweetalert(type,message,limit){
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: limit
        });

        Toast.fire({
            type: type,
            title: message
        })
      }
    </script>
  </head>
  <body>

    <?php if(session('message')=='question_sent'): ?>
      <?php
        session()->forget('message')
      ?>
      <script type="text/javascript">
        sweetalert('success','Ваш запрос успешно отправлен в обработку!Скоро с вами свяжутся!',4500);
      </script>
    <?php endif; ?>

    <?php if(session('message')=='subscribed'): ?>
      <?php
        session()->forget('subscribed')
      ?>
      <script type="text/javascript">
        sweetalert('success','Вы успешно подписаны!',3000);
      </script>
    <?php endif; ?>

    <?php if(session()->has('registered')): ?>
      <?php
        session()->forget('registered')
      ?>
      <script type="text/javascript">
        sweetalert('success','Вы успешно зарегестрированы!',3000);
      </script>
    <?php endif; ?>

    <?php if(session()->has('auth')): ?>
      <?php
        session()->forget('auth')
      ?>
      <script type="text/javascript">
        sweetalert('success','Вы успешно авторизованы!',3000);
      </script>
    <?php endif; ?>

    <?php if(session()->has('invalidData')): ?>
      <script type="text/javascript">
        <?php if(session('invalidData')->has('name')): ?>
          toastr.error('Заполните поле имя');
        <?php endif; ?>

        <?php if(session('invalidData')->has('surname')): ?>
          toastr.error('Заполните поле фамилия');
        <?php endif; ?>

        <?php if(session('invalidData')->has('email')): ?>
          toastr.error('Заполните поле email');
        <?php endif; ?>

        <?php if(session('invalidData')->has('password')): ?>
          <?php $__currentLoopData = session('invalidData')->messages()['password']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($error == "The password must be at least 8 characters."): ?>
              toastr.error('пароль должен быть длиннее 7 символов');
            <?php endif; ?>
            <?php if($error == "The password confirmation does not match."): ?>
              toastr.error('Пароли не совпадают');
            <?php endif; ?>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
      </script>
    <?php endif; ?>

    <?php if(session()->has('called')): ?>
      <?php
        session()->forget('called')
      ?>
      <script type="text/javascript">
        sweetalert('success','Ваш запрос успешно отправлен в обработку!',3000);
      </script>
    <?php endif; ?>

    <?php if(session()->has('not_auth')): ?>
      <?php session()->forget('not_auth') ?>
      <script type="text/javascript">
        sweetalert('error','Не верный логин или пароль!',3000);
      </script>
    <?php endif; ?>

    <?php if(session()->has('not_autorized')): ?>
      <?php
        session()->forget('not_autorized')
      ?>
      <script type="text/javascript">
        sweetalert('error','Вы не авторизованы!',3000);
      </script>
    <?php endif; ?>

    <!-- MODAL -->
    <div class="overlay">

    </div>

    <div class="modal-auth">
      <div class="close-modal-btn">
          &#10006;
      </div>
      <h5>
        Авторизация
      </h5>
      <form action="/enter" method="POST">
        <?php echo csrf_field(); ?>
        <input
          type="email"
          placeholder="Логин"
          name="email"
          required />
        <input
          type="password"
          placeholder="Пароль"
          name="password"
          minlength="8"
          required />
        <input
          class="button-modal"
          type="submit"
          name=""
          value="Войти" />
        <div class="forgot-pass">
          <p>
            <a href="#">
              Забыли пароль?
            </a>
          </p>
          <p>
            <a href="#" class="modal-auth-regbtn">
              У меня нет аккаунта
            </a>
          </p>
        </div>
      </form>
    </div>

    <!-- Модальной окно перезвоните мне -->

    <div class="modal-feedback" style="height: auto;">
      <div class="close-modal-btn">
          &#10006;
      </div>
      <h5>
        Перезвоните мне
      </h5>
      <form action="/home/call" method="POST">
        <?php echo csrf_field(); ?>
        <input
          type="text"
          placeholder="Имя"
          pattern="[A-Za-zА-Яа-яЁё]{1,}"
          name="name"
          required />
        <input
          type="tel"
          placeholder="Номер
          телефона"
          name="mobile"
          maxlength="16"
          pattern="+[0-9]{1}-[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}"
          required />
          <input
            type="text"
            placeholder="Проблема или предолжение"
            name="text" />
        <input
          class="button-modal"
          type="submit"
          name=""
          value="Отправить" />
        <div class="forgot-pass">
        </div>
      </form>
    </div>

    <div class="modal-samov">
      <div class="close-modal-btn">
          &#10006;
      </div>
      <h5>Адрес</h5>
      <form action="/home/call" method="POST">
        <?php echo csrf_field(); ?>
        <p>
          г.Алматы, Сейфуллина 498/1, уг.ул. Богенбай батыра, ТЦ "Fashion Avenue", 25 бутик
        </p>
      </form>
    </div>

    <div class="modal-courier">
      <div class="close-modal-btn">
          &#10006;
      </div>
      <h5>Оплата курьеру</h5>
      <form action="/home/call" method="POST">
        <?php echo csrf_field(); ?>
        <input
            type="tel"
            placeholder="ИИН"
            name="mobile"
            maxlength="12"
            pattern="[0-9]{12}"
            required />
        <input
            type="text"
            placeholder="Фамилия"
            pattern="[A-Za-zА-Яа-яЁё]{1,}"
            name="name"
            required />
        <input
            type="text"
            placeholder="Имя"
            pattern="[A-Za-zА-Яа-яЁё]{1,}"
            name="name"
            required />
        <input
            type="text"
            placeholder="Отечество"
            pattern="[A-Za-zА-Яа-яЁё]{1,}"
            name="name"
            required />
        <input
            type="tel"
            placeholder="Номер
            телефона"
            name="mobile"
            maxlength="16"
            pattern="+[0-9]{1}-[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}"
            required />
        <input
            type="text"
            placeholder="Адрес"
            name="name"
            required />
        <input
            class="button-modal"
            type="submit"
            name=""
            value="Отправить" />
        <div class="forgot-pass">
        </div>
      </form>
    </div>
    <div class="modal-register">
      <div class="close-modal-btn">
          &#10006;
      </div>
      <h5>
        Регистрация
      </h5>
      <h6>
        Основные данные
      </h6>
      <form
        method="POST"
        action="/register"
        enctype="multipart/form-data"
        id="registerForm"
        class="reg-modal">

        <?php echo csrf_field(); ?>
        <div class="row">
          <div class="col input-row">
              <input
              type="text"
              placeholder="Фамилия
              *"
              pattern="[A-Za-zА-Яа-яЁё]{1,}"
              name="surname"
              required />
          </div>
          <div class="col input-row">
              <input
                type="text"
                placeholder="Имя
                *"
                pattern="[A-Za-zА-Яа-яЁё]{1,}"
                name="name"
                required />
          </div>
        </div>
        <div class="row">
          <div class="col input-row">
              <input
                type="email"
                placeholder="E-mail
                *"
                name="email"
                required />
          </div>
          <div class="col input-row">
              <input
                type="email"
                placeholder="Дополнительный
                e-mail"
                name="dop_email" />
          </div>
          <div class="col input-row">
              <input
                type="file"
                name="avatar"
                accept=".jpg, .jpeg, .png" />
          </div>
        </div>
        <h6>
          Ваш адрес
        </h6>
        <div class="row">
          <div class="col input-row">
              <input type="text" placeholder="Компания" pattern="[A-Za-zА-Яа-яЁё]{1,}" name="company">
          </div>
          <div class="col input-row">
              <input type="text" placeholder="Адрес"  name="address">
          </div>
        </div>
        <div class="row">
          <div class="col modal-select" >

            <?php
              if(!isset($countries)) {
                $countries = \App\Country::get();
              }
            ?>

            <select name="country" id="country_dropdown">
              <option value="">
                Страна
              </option>

              <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($country->id); ?>">
                  <?php echo e($country->name); ?>

                </option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          </div>
          <div class="col modal-select">
              <select name="region" disabled id="region_dropdown">
                <option value="">Регион</option>
              </select>
          </div>
        </div>
        <div class="row">
          <div class="col input-row">
            <div class="col modal-select px-0">
              <select name="city" id="city_dropdown" disabled>
                <option value="">Город</option>
              </select>
            </div>
          </div>
          <div class="col input-row">
              <input
                type="text"
                placeholder="Почтовый
                индекс"
                pattern="[0-9]{6}"
                name="index" />
          </div>
        </div>
        <h6>
          Ваш пароль
        </h6>
        <div class="row">
          <div class="col input-row">
            <input
              type="password"
              placeholder="Пароль *"
              id="txtNewPassword"
              name="password"
              minlength="8"
              required />
          </div>
          <div class="col input-row">
            <input
              type="password"
              placeholder="Подвердить пароль *"
              name="password_confirmation"
              id="txtConfirmPassword"
              minlength="8"
              required />
            <div
              class="registrationFormAlert"
              id="divCheckPasswordMatch">
            </div>
          </div>
        </div>
        <div class="modal-subscribe">
          <h6 class="text-center">Подписаться на новости</h6>
          <div class="radio-row">
            <label for="radio">
              <input type="radio" name="radio" id="radio">
              Да
            </label>
            <label for="radio">
              <input type="radio" name="radio" id="radio">
              Нет
            </label>
          </div>
        </div>
        <button
          class="button-modal btn-modal-reg"
          type="submit">
          Зарегистрироваться
        </button>
        <div class="forgot-pass">
          <p>
            <a class="haveaccount-btn" href="">
              У меня есть аккаунт
            </a>
          </p>
        </div>
      </form>
    </div>
    <!-- END MODAL -->

    <!-- HEADER -->
    <header class="header" id="header">
      <!-- HEADER TOP -->
      <div class="header-top container">
        <div class="header-top-row1">
          <div>
            <a href="/" class="logo">
              <img src="/images/logo.png">
            </a>
          </div>
          <div class="header-calendar">
            <span>
              <img src="/images/calendar.png" alt="calendar" />
            </span>
            <a href="#">
              <?php echo e(\App\Contact::first()->weekday_start); ?>. -
              <?php echo e(\App\Contact::first()->weekday_end); ?>.,с
              <?php echo e(\App\Contact::first()->work_start); ?> до
              <?php echo e(\App\Contact::first()->work_end); ?>

            </a>
          </div>

          <div class="header-tel-num">
            <span>
              <img src="/images/tel.png" alt="tel" />
            </span>
            <a href="tel://<?php echo e(\App\Contact::first()->phone); ?>">
              <?php echo e(\App\Contact::first()->phone); ?>

            </a>
          </div>

          <div class="header-wp">
            <span>
              <img src="/images/wp.png" alt="whatsapp" />
            </span>
            <a href="https://wa.me/<?php echo e(\App\Contact::first()->whatsapp); ?>">
              <?php echo e(\App\Contact::first()->whatsapp); ?>

            </a>
          </div>

          <div class="mobile-btn">
            <button class="btn-call-us">
              Перезвоните мне
            </button>

            <button
              class="btn btn-success mx-1 py-0 text-sm btn-call-us"
              style="font-size: 17px;"
              onclick="
                this.nextElementSibling.style.display == 'none' ?
                this.nextElementSibling.style.display = 'block' :
                this.nextElementSibling.style.display = 'none'">
                  Промокод
            </button>
          </div>
        </div>
        <div class="header-top-row2">
          <span class="promo-wrapper" style="padding-right: 37px;">
            <span style="display: none">
              <?php echo e(\App\ActivePromocode::first()->promocode ?
                \App\ActivePromocode::first()->promocode->promocode :
                ''); ?>

            </span>
          </span>
          <div class="sign-and-signup">
            <?php if(Auth::check()): ?>
              <a href="/login">
                <?php echo e(Auth::user()->name); ?>

              </a>
            <?php else: ?>
              <a href="#" class="signin">
                Вход
              </a>
            <?php endif; ?>
            <?php if(!Auth::check()): ?>
              <a href="#" class="signup">
                Регистрация
              </a>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <!-- END HEADER TOP -->

      <!-- HEADER BOTTOM -->
      <div class="header-bottom">
        <div class="container">
          <div class="header-bottom-rowws">
            <div class="header-bottom-row1">
              <nav class="nav-menu">
                <ul>
                  <?php $__currentLoopData = $head_sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                      <a href="/section/<?php echo e($section->id); ?>">
                        <?php echo e($section->name); ?>

                      </a>
                    </li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <li>
                    <a href="/section/contact">
                      Контакты
                    </a>
                  </li>
                  <li>
                    <a href="/news">
                      Блог
                    </a>
                  </li>
                  <li>

                  </li>
                </ul>
              </nav>
              <form class="search-form" action="/products/search" method="get">
                <div class="search-input">
                  <input type="search" name="search" placeholder="Поиск" required>
                  <button type="submit">
                    <img src="/images/search.png">
                  </button>
                </div>
              </form>
              <div class="header-some-icons">
                <ul>
                  <li>
                    <a href="/likes">
                      <img src="/images/heart.png" />
                      <span id="likes">
                        <?php echo e($likes); ?>

                      </span>
                    </a>
                  </li>
                  <li>
                    <a href="/comparisons">
                      <img src="/images/libra.png" />
                      <span id="comparisons">
                        <?php echo e($comparisons); ?>

                      </span>
                    </a>
                  </li>
                  <li>
                    <a href="/basket">
                      <img src="/images/basket.png" />
                      <span id="cartAmount"></span>
                    </a>
                  </li>
                </ul>
              </div>
              <a class="header-menu-toggle" href="#0">
                <span class="header-menu-icon"></span>
                <span class="header-menu-icon mt-2"></span>
                <span class="header-menu-icon mt-2"></span>
              </a>
            </div>
          </div>
          <!-- MOBILE MENU -->
          <nav class="header-nav shadow">
            <a href="#0" class="header-nav__close" title="close">
              <span>
                Close
              </span>
            </a>
            <div class="header-nav__content">
              <ul>
                <?php $__currentLoopData = $head_sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                 <li><a href="/section/<?php echo e($section->id); ?>"><?php echo e($section->name); ?></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <li>
                  <a href="/section/contact">
                    Контакты
                  </a>
                </li>
                <li>
                  <a href="/news">
                    Блог
                  </a>
                </li>
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li class="mobile-drop"><a href="#"><?php echo e($category->name); ?></a></li>
                  <?php if($subcategories[$category->id] != null): ?>
                    <div class="mobile-menu-dropdown">
                      <?php $__currentLoopData = $subcategories[$category->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <p>
                          <a class="text-white" href="/products/subcategory/<?php echo e($subcategory->id); ?>">
                            <?php echo e($subcategory->name); ?>

                          </a>
                        </p>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                  <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </ul>
            </div>
          </nav>
          <!-- END MOBILE MENU -->
        </div>
      </div>
      <!-- END HEADER BOTTOM -->
      <div class="header-bottom-row2">
        <div class="container">
          <nav class="header-bottom-menu">
            <ul class=" menu">
              <li class="stock">
                <a href="/products/stock">
                  АКЦИИ
                </a>
              </li>
              <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if(!empty($subcategories[$category->id])): ?>
                  <li>
                    <div class="dropdown">
                    <button
                      class="btn btn-secondary dropdown-toggle"
                      type="button"
                      id="dropdownMenuButton"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false">
                        <?php echo e($category->name); ?>

                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <?php $__currentLoopData = $subcategories[$category->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a
                          class="dropdown-item"
                          href="/products/subcategory/<?php echo e($subcategory->id); ?>">
                            <?php echo e($subcategory->name); ?>

                        </a>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                  </div>
                </li>
                <?php else: ?>
                  <li>
                    <a href="/products/category/<?php echo e($category->id); ?>">
                        <?php echo e($category->name); ?>

                    </a>
                  </li>
                <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
          </nav>
        </div>
      </div>
    </header>

    <?php echo $__env->yieldContent("body"); ?>

    <?php
      $pViews = \App\Product::getViewedProducts();
    ?>
    <?php if($pViews && url()->current() != url('/')): ?>
      <div class="catalogs-slider">
        <div class="container">
          <h3>
            Ранее вы смотрели
          </h3>
          <a
            href="#"
            class="banner-arrow__left"
            style="position:absolute; top: 10rem; left: .5rem;">
              <img src="<?php echo e(asset('/images/slide-prev.png')); ?>" />
          </a>
          <!-- arrow left end -->
          <div id="catalogCarousel" class="owl-carousel owl-theme container wow zoomIn">
            <?php $__currentLoopData = $pViews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $view): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <div class="catalog-product-item">
                <div class="catalog-img">
                  <a href="/product-card/<?php echo e($view->id); ?>"><img src="/storage/<?php echo e($view->cover); ?>"></a>
                  <?php if(!empty($view->discount)): ?>
                    <span class="catalog-stock"><?php echo e($view->discount); ?></span>
                  <?php endif; ?>
                </div>
                <h4>
                  <a href="/product-card/<?php echo e($view->id); ?>">
                    <?php echo e($view->name); ?>

                  </a> / <?php echo e($view->volume); ?> мл.
                </h4>
                <p>
                  <?php echo e($view->text_cover); ?>

                </p>
                <?php if(!empty($view->s_price)): ?>
                  <div class="price">
                    <?php echo e($view->s_price); ?> тнг
                    <span>
                      <?php echo e($view->f_price); ?> тнг
                    </span>
                  </div>
                <?php else: ?>
                  <div class="price">
                    <?php echo e($view->f_price); ?> тнг
                  </div>
                <?php endif; ?>

                <?php
                  $rating = \App\Product::calculateRating($view->id);
                ?>
                <div class="star-line">
                  <ul>
                    <?php for($i = 1; $i <= 5; $i++): ?>
                      <?php if($i <= $rating): ?>
                        <li style="color: #eeb900">
                          &#9733;
                        </li>
                      <?php else: ?>
                        <li style="color: #d4d4d4;">
                          &#9733;
                        </li>
                      <?php endif; ?>
                    <?php endfor; ?>
                  </ul>
                </div>
                <button
                  class="btn-basket"
                  name="btn-basket"
                  id="<?php echo e($view->id); ?>"
                  onclick="addToCart(this.id)">
                    В КОРЗИНУ
                </button>
              </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
          <!-- arrow right -->
          <a
            href="#"
            class="banner-arrow__right"
            style="position:absolute; top: 10rem; right: .5rem;">
              <img src="<?php echo e(asset('/images/slide-nxt.png')); ?>" />
          </a>
        </div>
      </div>
    <?php endif; ?>
    <!-- FOOTER -->
    <div>
      <img src="/images/up-icon.png" alt="" class="scrollup" />
    </div>

    <footer class="footer">
      <div class="container">
        <div class="footer-row">
          <div class="logo">
            <img src="/images/logo.png" />
            <p>
              Натуральная продукция ручной работы<br>
              по самым выгодным ценам в Казахстане
            </p>
          </div>
          <ul class="footer-menu">
            <li>
              <p>
                Категории
              </p>
            </li>
            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php if(!empty($subcategories[$category->id])): ?>
                <li class="footer-menu-item">
                  <div class="dropdown">
                    <a
                      class=""
                      id="dropdownMenuButton"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false">
                        <?php echo e($category->name); ?>

                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <?php $__currentLoopData = $subcategories[$category->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a
                          class="dropdown-item"
                          href="/products/subcategory/<?php echo e($subcategory->id); ?>">
                            <?php echo e($subcategory->name); ?>

                        </a>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                  </div>
                </li>
              <?php else: ?>
                <li class="footer-menu-item">
                  <a href="/products/category/<?php echo e($category->id); ?>">
                    <?php echo e($category->name); ?>

                  </a>
                </li>
              <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </ul>
          <ul class="footer-menu">
            <li>
              <p>
                Для клиентов
              </p>
            </li>
            <?php $__currentLoopData = $head_sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <li>
                <a href="/section/<?php echo e($section->id); ?>">
                  <?php echo e($section->name); ?>

                </a>
              </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <li>
            <a href="/products/stock">
              Акции
            </a>
          </li>
          <li>
            <a href="/news">
              Блог
            </a>
          </li>
        </ul>
        <div class="contacts">
          <p>КОНТАКТЫ</p>
          <ul>
            <li>
              <button class="btn-call-us">
                Свяжитесь с нами
              </button>
            </li>
            <li>
              <img src="/images/wp.png" />
              <a href="https://wa.me/<?php echo e(\App\Contact::first()->whatsapp); ?>">
                <?php echo e(\App\Contact::first()->whatsapp); ?>

              </a>
            </li>
            <li>
              <img src="/images/tel.png" />
              <a href="tel:<?php echo e(\App\Contact::first()->phone); ?>">
                <?php echo e(\App\Contact::first()->phone); ?>

              </a>
            </li>
          </ul>
          <h4>
            Ждем Вас в соц. сетях:
          </h4>
          <ul class="social-networks">
            <?php $__currentLoopData = $links; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <li>
                <a href="<?php echo e($link->link); ?>">
                  <img src="/images/<?php echo e($link->type); ?>.png" />
                </a>
              </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <!-- END FOOTER -->

  <script src="https://unpkg.com/popper.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/superfish.min.js"></script>
  <script src="/js/wow.min.js"></script>
  <script src="/js/owl.carousel.min.js"></script>
  <script src="/js/easyzoom.js"></script>
  <script src="/js/select-option.min.js"></script>
  <script src="https://atuin.ru/demo/ui-slider/jquery-ui.min.js"></script>
  <script src="https://atuin.ru/demo/ui-slider/jquery.ui.touch-punch.min.js"></script>
  <script>

  </script>

  <!-- НУЖНО ДОБАВИТЬ СТИЛИ КАСАЕМО ДИЗАЙНА САЙТА -->
  <!-- АЛЕРТ ПРИ УСПЕШНОЙ ПОДПИСКЕ -->
  <?php if(Session::has('podpisan')): ?>
    <?php
      session()->forget('podpisan');
    ?>
    <script type="text/javascript">
      Swal.fire(
        'Спасибо!',
        'Вы успешно подписаны!',
        'success'
      )
    </script>
  <?php endif; ?>

  <?php if(Session::has('orderComplete')): ?>
    <?php
      session()->forget('podpisan');
    ?>
    <script type="text/javascript">
      Swal.fire(
        'Спасибо!',
        'Ваш заказ принят',
        'success'
      )
    </script>
  <?php endif; ?>

  <!-- ДОБАВЛЕНИЕ В КАРЗИНУ -->
  <script type="text/javascript">
    function addToCart(id) {
      refreshCartNumber();
      if(getCookie('product'+id) != ""){
        pop_exists()
      }
      else{
        pop();
      }
      setCookie('product'+id, 1, { expires: 7, path: '/' });
    }

    function pop(){
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });

      Toast.fire({
        type: 'success',
        title: 'Товар в Корзине!'
      })
    }

    function pop_exists(){
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });

      Toast.fire({
        type: 'error',
        title: 'Товар уже в Корзине!'
      })
    }

    $("#registerForm").submit(function(e){
      e.preventDefault();
      if(document.getElementById('txtNewPassword').value == document.getElementById('txtConfirmPassword').value){
        $("#registerForm").off();
        $("#registerForm").submit();
      }
      else{
        toastr.error('Пароли не совпадают');
      }
    });

    function setCookie(name,value,days) {
      var expires = "";
      if (days) {
          var date = new Date();
          date.setTime(date.getTime() + (days*24*60*60*1000));
          expires = "; expires=" + date.toUTCString();
      }
      document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }

    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

    function refreshCartNumber() {
      var cartCount = document.cookie.split(';').filter(function(c) {
        return c.trim().indexOf('product') === 0;
      }).map(function(c) {
         return c.trim();
      });
      $("#cartAmount").text(cartCount.length);
    }

    function basket_add(id,card_count){
      $.ajax({
        url: "/addToCart",
        method: "GET", // Что бы воспользоваться POST методом, меняем данную строку на POST
        data: {
          "id":id,
          card_count:card_count
        },
        success: function(data) {
          basket_count.innerHTML=data;
          sweetalert('success','Товар успешно добавлен в корзину!',3000);
        },
        error:function(){
          sweetalert('error','Вы не авторизованы!',3000);
        }
      });
    }

    function add_like(id){
      $.ajax({
        url: "/add_like",
        method: "GET", // Что бы воспользоваться POST методом, меняем данную строку на POST
        data: {"id":id},
        success: function(data) {
          if (data!=0) {
            likes.innerHTML=data;
            sweetalert('success','Товар успешно добавлен в избранные!',3000);
          }
          else{
            sweetalert('warning','Данный товар уже в избранных!',3000);
          }
        },
        error:function(){
          sweetalert('error','Вы не авторизованы!',3000);
        }
      });
    }


    function add_comparison(id){
      $.ajax({
        url: "/add_comparison",
        method: "GET", // Что бы воспользоваться POST методом, меняем данную строку на POST
        data: {"id":id},
        success: function(data) {
          if (data==777){
            sweetalert('warning','В сравнения можно добавить максимум 5 товаров!',3000);
          }
          else if (data!=0) {
            comparisons.innerHTML=data;
            sweetalert('success','Товар успешно добавлен в сравнения!',3000);
          }
          else{
            sweetalert('warning','Данный товар уже в сравнениях!',3000);
          }
        },
        error:function(){
          sweetalert('error','Вы не авторизованы!',3000);
        }
      });
    }
  </script>
  <script src="<?php echo e(asset('js/app.js')); ?>"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      refreshCartNumber();
    })
  </script>
  <!--   <?php if(isset($filter)): ?>
  <script type="text/javascript">

  </script>
  <?php endif; ?> -->
  <?php echo $__env->yieldPushContent('scripts'); ?>
</body>
</html>
<?php /**PATH C:\OSPanel\domains\organic\resources\views/layout.blade.php ENDPATH**/ ?>