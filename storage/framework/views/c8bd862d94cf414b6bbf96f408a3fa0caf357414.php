<?php $__env->startSection('body'); ?>
  <!-- MAIN -->
  <main class="main">
    <!-- BREAD CRUMBS -->
    <ul class="bread-crumbs container">
      <li><a href="/">Главная</a></li>
      <?php if(!isset($stock)): ?>
      <li><a class="now-page" href="#"><?php echo e($main_data->name); ?></a></li>
      <?php else: ?>
       <li><a href="#"><?php echo e($stock); ?></a></li>
      <?php endif; ?>
    </ul>
    <!-- END BREAD CRUMBS -->
    <?php if(isset($action)): ?>
    <script type="text/javascript">
      window.action='<?php echo e($action); ?>';
    </script>
    <?php else: ?>
    <script type="text/javascript">
      window.action='/products/stock';
    </script>
    <?php endif; ?>
    <!-- ORGANIC CATALOG -->
    <div class="organic-catalog">
      <div class="container">
        <div class="catalog-section">
          <div class="title">
            <?php if(!isset($stock)): ?>
            <h3><?php echo e($main_data->name); ?></h3>
            <p style="word-wrap: break-word;"><?php echo e($main_data->description); ?></p>
            <?php else: ?>
            <h3><?php echo e($stock); ?></h3>
            <?php endif; ?>
          </div>
        </div>

        <div class="catalog-wrapper">
          <div class="catalog_left-side">
            <div class="panel">
              <!-- first panel start here -->
              <div class="tab_panel sections">
                <div class="tab_heading">
                  <h4>Разделы</h4>
                  <span><i class="material-icons">keyboard_arrow_up</i></span>
                </div>
                <div class="tab_content">
                  <ul>
                    <?php $__currentLoopData = $sales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="sale_category" data-id="<?php echo e($sale->id); ?>"><?php echo e($sale->name); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </ul>
                </div>
              </div>  
            </div> <!-- main panel close here -->
          </div>

          <div class="catalog_right-side">
            <div id="catalog_products"> 
              <div class="catalog-products-wrapper" id="products_wrapper">
              </div>
            <div class="catalog-products-wrapper-more" id="moreProduct" style="display: none;">
              
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END ORGANIC CATALOG -->

    <!-- SUSCRIPTION BLOCK -->
    <div class="white-bg-wrapp">
      <div class="subscription-block container wow rotateInUpLeft" data-wow-duration="1s">
        <div class="subscription-absolute">
          <div class="subscription-content">
            <h3>Хотите быть в курсе всех новостей?</h3>
            <span>Подпишитесь на рассылку!</span>
            <div class="subscription-wrapp">
              <div class="sub-social">
                <h4>Следите за нами в соц сетях:</h4>
                <ul class="social-networks">
                   <?php $__currentLoopData = $links; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li><a href="<?php echo e($link->link); ?>"><img src="/images/<?php echo e($link->type); ?>.png"></a></li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
              <div class="sub-row">
                <div class="sub-button-input">
                  <form action="/subscription" method="POST">
                    <?php echo csrf_field(); ?>
                    <input type="email" name="email" placeholder="Введите Электронную почту">
                    <input type="submit" class="sub-btn" value="ПОДПИСАТЬСЯ">
                  </form>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END SUSCRIPTION BLOCK -->
  </main>
  <!-- END MAIN -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make("layout", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/japananimetime/Projects/organic/resources/views/catalog_stock.blade.php ENDPATH**/ ?>