<?php $__env->startSection('body'); ?>
<main class="main">
    <!-- BANNER -->
    <div id="carouselOne" class="organic-banner owl-carousel owl-theme container wow zoomIn">
      <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <div class="banner-item">
        <img src="/storage/<?php echo e($banner->image); ?>">
      </div>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <!-- END BANNER -->
    <!-- ADVANTAGES -->
    <div class="advantages container wow fadeInUp" data-wow-duration="1.5s">
      <ul>
        <li>
          <img src="images/advantage1.png">
          <p>100%</p>
          <span>Натуральная косметика</span>
        </li>
        <li>
          <img src="images/advantage2.png">
          <p>Выгодная цена</p>
          <span>Прямые поставки от производителя</span>
        </li>
        <li>
          <img src="images/advantage3.png">
          <p>Бесплатная</p>
          <span>Быстрая доставка</span>
        </li>
        <li>
          <img src="images/advantage4.png">
          <p>Скидки</p>

          <span>Постоянным клиентам</span>
        </li>
        <li>
          <img src="images/advantage5.png">
          <p>С гарантией</p>
          <span>Лёгкого возврата в течении 14 дней</span>
        </li>
      </ul>
    </div>
    <!-- END ADVANTAGES -->

    <!-- OFFERS -->
    <div class="offers">
      <div class="title">
        <h3>Предложение дня</h3>
      </div>
      <div id="carouselExampleControls" class="carousel slide wow zoomIn" data-wow-duration="1.5s" data-ride="carousel">
        <div class="carousel-inner">
		<?php $__currentLoopData = collect($day_products)->chunk(4); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $day_products): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="carousel-item <?php echo e($loop->first ? 'active' : ''); ?>">

				<div class="container">
				<div class="offers-slide-wrapp">
				<?php $__currentLoopData = $day_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $day_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="offers-slide-item">
                  <div class="offers-item-img" style="padding: 10px;">
                    <a href="/product-card/<?php echo e($day_product->id); ?>"><img src="/storage/<?php echo e($day_product->cover); ?>"></a>
                  </div>
                  <div class="offers-item-content">
                    <div class="product-name">
                      <a href="/product-card/<?php echo e($day_product->id); ?>"><?php echo e($day_product->name); ?>/</a>
                      <span><?php echo e($day_product->volume); ?> мл</span>
                    </div>
                <!--    <div class="product-about">
                     <span><?php echo e($day_product->description); ?></span>
                    </div> -->
                    <div class="product-mark-price">

                        <?php
                        $rating = \App\Product::calculateRating($day_product->id);
                        ?>
                        <div class="star-line">
                          <ul>
                            <?php for($i = 1; $i <= 5; $i++): ?>
                            <?php if($i <= $rating): ?>
                            <li style="color: #eeb900">&#9733;</li>
                            <?php else: ?>
                            <li style="color: #d4d4d4;">&#9733;</li>
                            <?php endif; ?>
                            <?php endfor; ?>
                          </ul>
                        </div>
                      <div class="price">
                        <span><?php echo e($day_product->f_price); ?> тнг</span>
                      </div>
                    </div>
                    <div class="product-button">
                      <div class="product-buttons-row">
                        <button class="btn-like" name="btn-like" id="<?php echo e($day_product->id); ?>"  onclick="add_like(this.id)">
                          <img src="images/like-btn.png">
                        </button>
                        <button class="btn-libra" name="btn-libra" onclick="add_comparison('<?php echo e($day_product->id); ?>')">
                          <img src="images/libra-btn.png">
                        </button>
                      </div>
                      <button class="btn-basket" name="btn-basket"     id="<?php echo e($day_product->id); ?>"   onclick="addToCart(this.id)">
                        В КОРЗИНУ
                      </button>

                    </div>

                  </div>
                </div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</div></div>
			</div>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <button>
            <img src="/images/slide-left.png">
          </button>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <button>
            <img src="/images/slide-left.png">
          </button>
        </a>
      </div>
      <div class="offer-stocks">
        <div class="container">
          <ul class="tabs">
        		<li class="tab-link current" data-tab="tab-1">
              <img src="images/bakset.png">
              <span>Распродажа</span>
            </li>
        		<li class="tab-link" data-tab="tab-2">
              <img src="images/news.png">
              <span>Новинки</span>
            </li>
        		<li class="tab-link" data-tab="tab-3">
              <img src="images/favorite.png">
              <span>Хиты продаж</span>
            </li>
        	</ul>

        	<div id="tab-1" class="tab-content current wow fadeInUp">
		
           <?php $__currentLoopData = $sale_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sale_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="offers-slide-item">
              <div class="offers-item-img" style="padding-right: 10px;">
                <a href="/product-card/<?php echo e($sale_product->id); ?>"><img src="/storage/<?php echo e($sale_product->cover); ?>"></a>
              </div>
              <div class="offers-item-content">
                <div class="product-name">
                  <a href="/product-card/<?php echo e($sale_product->id); ?>"><?php echo e($sale_product->name); ?> /</a>
                  <span><?php echo e($sale_product->volume); ?> мл</span>
                </div>
              <!--  <div class="product-about">
                  <span><?php echo e($sale_product->description); ?></span>
                </div> -->
                <div class="product-mark-price">
                  <div class="price">
                    <span><?php echo e($sale_product->f_price); ?> тнг</span>
                  </div>
                </div>
				  <div class="d-flex">
					<div class="product-button">
					  <button class="btn-basket" name="btn-basket"     id="<?php echo e($sale_product->id); ?>"   onclick="addToCart(this.id)">
							В КОРЗИНУ
						  </button>
					</div>
					  <div class="add-btns d-flex">
						  <a href="javascript:void()"><img src="/images/libra-btn.png" alt="" onclick="add_comparison('<?php echo e($sale_product->id); ?>')"></a>
						  <a href="javascript:void()"><img src="/images/like-btn-green.png" alt="" id="<?php echo e($sale_product->id); ?>"
										   onclick="add_like(this.id)"></a>
				  	</div>
				  </div>
              </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				
        	</div>
        	<div id="tab-2" class="tab-content wow fadeInUp">
            <?php $__currentLoopData = $new_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="offers-slide-item">
              <div class="offers-item-img" style="padding-right: 10px;">
                <a href="/product-card/<?php echo e($new_product->id); ?>"><img src="/storage/<?php echo e($new_product->cover); ?>"></a>
              </div>
              <div class="offers-item-content">
                <div class="product-name">
                  <a href="/product-card/<?php echo e($new_product->id); ?>"><?php echo e($new_product->name); ?> /</a>
                  <span><?php echo e($new_product->volume); ?> мл</span>
                </div>
              <!--  <div class="product-about">
                  <span><?php echo e($new_product->description); ?></span>
                </div> -->
                <div class="product-mark-price">
                  <div class="price">
                    <span><?php echo e($new_product->f_price); ?> тнг</span>
                  </div>
                </div>
                <div class="product-button">
                  <button class="btn-basket" name="btn-basket"     id="<?php echo e($new_product->id); ?>"   onclick="addToCart(this.id)">
                        В КОРЗИНУ
                      </button>
                </div>
              </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        	</div>
        	<div id="tab-3" class="tab-content wow fadeInUp">
            <?php $__currentLoopData = $hit_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hit_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="offers-slide-item">
              <div class="offers-item-img" style="padding-right: 10px;">
                <a href="/product-card/<?php echo e($hit_product->id); ?>"><img src="/storage/<?php echo e($hit_product->cover); ?>"></a>
              </div>
              <div class="offers-item-content">
                <div class="product-name">
                  <a href="/product-card/<?php echo e($hit_product->id); ?>"><?php echo e($hit_product->name); ?> /</a>
                  <span><?php echo e($hit_product->volume); ?> мл</span>
                </div>
               <!-- <div class="product-about">
                  <span><?php echo e($hit_product->description); ?></span>
                </div> -->
                <div class="product-mark-price">
                  <div class="price">
                    <span><?php echo e($hit_product->f_price); ?> тнг</span>
                  </div>
                </div>
                <div class="product-button">
                  <button class="btn-basket" name="btn-basket"     id="<?php echo e($hit_product->id); ?>"   onclick="addToCart(this.id)">
                        В КОРЗИНУ
                      </button>
                </div>
              </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- END OFFERS -->

    <!-- WANT question-block -->
    <div class="question-block container">
      <div class="question-absolute wow rotateInUpLeft" data-wow-duration="1.5s">
        <h3>Хотите задать вопрос?</h3>
        <p>Позвоните нам на номер  +7(747) 257 - 6949<br>
или заполните форму и мы перезвоним в течении трёх минут!</p>
        <div class="question-inputs">
          <form action="/home/question" method="get">
            <input type="text" placeholder="Ваше имя" pattern="[A-Za-zА-Яа-яЁё]{1,}" name='name' required>
            <input type="tel" placeholder="Номер телефона: 8-777-777-77-77" name='mobile' pattern="[0-9]{1}-[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}" required style="width: 290px;" maxlength="16">
            <input type="submit" class="btn-form" value="отправить">
          </form>
        </div>
      </div>
    </div>
    <!-- END WANT question-block -->


    <!-- REVIEWS AND NEWS -->
    <div class="reviews-and-news">
      <div class="container">
        <div class="title">
          <h3>Отзывы и новости</h3>
        </div>
        <div class="full-side">
          <div class="left-side" data-wow-duration="1.5s">
            <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="news-item">
              <div class="news-img">
                <img src="<?php echo e(Voyager::image($new->cover)); ?>">
              </div>
              <h4><?php echo e($new->title); ?></h4>
              <!-- <div style="word-wrap: break-word !important;"><?php echo substr($new->body,0,79); ?> ...</div> -->
              <a href="/news-card/<?php echo e($new->id); ?>">ЧИТАТЬ ДАЛЕЕ</a>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
          <div class="right-side" data-wow-duration="1.5s">
            <ul class="reviews-imgs">
            <?php
            $i=1;
            ?>
              <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php if($i==2): ?>
              <li class="tab-link active" review-tab="review-<?php echo e($comment->id); ?>">
              <?php else: ?>
              <li class="tab-link" review-tab="review-<?php echo e($comment->id); ?>">
              <?php endif; ?>
              <?php
              $i++;
              ?>
                <img src="<?php echo e(Voyager::image($comment->avatar)); ?>">
              </li>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
            <?php
            $i=1;
            ?>
            <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($i==2): ?>
            <div id="review-<?php echo e($comment->id); ?>" class="review-content active">
            <?php else: ?>
            <div id="review-<?php echo e($comment->id); ?>" class="review-content">
            <?php endif; ?>
             <?php
              $i++;
            ?>
              <p><?php echo e(substr($comment->text,0,150)); ?></p>
              <span><?php echo e($comment->name); ?></span>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- END REVIEWS AND NEWS -->


    <!-- SUSCRIPTION BLOCK -->
    <div class="white-bg-wrapp">
      <div class="subscription-block container" data-wow-duration="1s">
        <div class="subscription-absolute">
          <div class="subscription-content">
            <h3>Хотите быть в курсе всех новостей?</h3>
            <span>Подпишитесь на рассылку!</span>
            <div class="subscription-wrapp">
              <div class="sub-social">
                <h4>Следите за нами в соц сетях:</h4>
                <ul class="social-networks">
                   <?php $__currentLoopData = $links; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li><a href="<?php echo e($link->link); ?>"><img src="images/<?php echo e($link->type); ?>.png"></a></li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
              <div class="sub-row">
                <div class="sub-button-input">
                  <form action="/home/subscription" method="POST">
                    <?php echo csrf_field(); ?>
                    <input type="email" name="email" placeholder="Введите Электронную почту" required>
                    <input type="submit" class="sub-btn" value="ПОДПИСАТЬСЯ">
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END SUSCRIPTION BLOCK -->
  </main>
  <!-- END MAIN -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layout", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/japananimetime/Projects/organic/resources/views/index.blade.php ENDPATH**/ ?>