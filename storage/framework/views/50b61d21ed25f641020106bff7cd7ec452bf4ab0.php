<?php if(empty($products)): ?>
    <h3 class="no-products-title">По данному запросу нет товаров!</h3>
<?php endif; ?>

<?php
    $i=1;
?>

<div class="catalog-products-wrapper">
    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="catalog-product-item">
            <div class="catalog-img">
                <?php if(!empty($product->discount)): ?>
                    <span class="catalog-stock">-<?php echo e($product->discount); ?></span>
                <?php endif; ?>
            </div>
            <h4>
                <a href="/product-card/<?php echo e($product->id); ?>">
                    <?php echo e($product->name); ?>

                </a> / <?php echo e($product->volume); ?> мл.
            </h4>
            <p>
                <?php echo e(substr(
                            $product->text_cover,
                            0,
                            strrpos(
                                substr($product->text_cover, 0, 50),
                                ' '
                            )
                    )); ?>

            </p>
            <?php if(!empty($product->s_price)): ?>
                <?php if(!empty($product->s_price)): ?>
                    <?php if($product->s_price != $product->f_price): ?>
                        <div class="price"><?php echo e($product->s_price); ?> тнг
                            <?php if(trim($product->f_price) != 0): ?>
                                <span>
                                    <?php echo e($product->f_price); ?> тнг
                                </span>
                            <?php endif; ?>
                        </div>
                    <?php else: ?>
                        <div class="price"><?php echo e($product->f_price); ?> тнг </div>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php
                $product->rating = \App\Product::calculateRating($product->id);
            ?>
            <ul style="display: flex">
                <?php for($i = 1; $i <= 5; $i++): ?>
                    <?php if($i <= $product->rating): ?>
                        <li style="color: #eeb900">&#9733;</li>
                    <?php else: ?>
                        <li style="color: #d4d4d4;">&#9733;</li>
                    <?php endif; ?>
                <?php endfor; ?>
            </ul>
            <button class="btn-basket" name="btn-basket"     id="<?php echo e($product->id); ?>"   onclick="add_basket(this.id)">
                В КОРЗИНУ
            </button>
        </div>
        <?php

            unset($products[$key]);

            if($i==9){
                break;
            }
            else{
                $i++;
            }

        ?>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<!-- БЛОК ПОКАЗАТЬ ЕЩЕ -->

<?php if(!empty($products)): ?>
    <div class="catalog-products-wrapper-more" id="moreProduct" style="display: none;">
        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="catalog-product-item">
                <div class="catalog-img">
                    <a href="/product-card/<?php echo e($product->id); ?>">
                        <img src="/storage/<?php echo e($product->cover); ?>">
                    </a>
                    <?php if(!empty($product->discount)): ?>
                        <span class="catalog-stock">-<?php echo e($product->discount); ?></span>
                    <?php endif; ?>
                </div>
                <h4>
                    <a href="/product-card/<?php echo e($product->id); ?>">
                        <?php echo e($product->name); ?>

                    </a> / <?php echo e($product->volume); ?> мл.
                </h4>
                <p>
                    <?php echo e($product->text_cover); ?>

                </p>
                <?php if(!empty($product->s_price)): ?>
                    <div class="price"><?php echo e($product->s_price); ?> тнг
                        <span>
                            <?php echo e($product->f_price); ?> тнг
                        </span>
                    </div>
                <?php else: ?>
                    <div class="price">
                        <?php echo e($product->f_price); ?> тнг
                    </div>
                <?php endif; ?>
                <?php
                    $product->rating = \App\Product::calculateRating($product->id);
                ?>
                <div class="star-line">
                    <ul style="display: flex">
                        <?php for($i = 1; $i <= 5; $i++): ?>
                        <?php if($i <= $product->rating): ?>
                        <li style="color: #eeb900">&#9733;</li>
                        <?php else: ?>
                        <li style="color: #d4d4d4;">&#9733;</li>
                        <?php endif; ?>
                        <?php endfor; ?>
                    </ul>
                </div>
                <button class="btn-basket"
                        name="btn-basket"
                        id="<?php echo e($product->id); ?>"
                        onclick="add_basket(this.id)">
                    В КОРЗИНУ
                </button>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>

    <div class="show-all-products">
    <!--<a href="#moreProduct" class="more-btn">ПОКАЗАТЬ ЕЩЁ</a>-->
        <?php echo e($products->links()); ?>

        <span id="currentURL" style="display: none"><?php echo e($url); ?></span>
    </div>
<?php endif; ?>
<?php /**PATH /home/japananimetime/Projects/organic/resources/views/catalog_ajax.blade.php ENDPATH**/ ?>