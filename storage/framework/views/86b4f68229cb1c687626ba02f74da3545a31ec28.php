<?php $__env->startSection('body'); ?>
<!-- MAIN -->
<main class="main">
  <!-- BREAD CRUMBS -->
  <ul class="bread-crumbs container">
    <li><a href="/">Главная</a></li>
    <li><a href="http://organic.ibeacon.kz/products/category/23">Продукты</a></li>
    <li><a href="/products/category/<?php echo e($product->category->id); ?>"><?php echo e($product->category->name); ?></a></li>
    <li><a href="/products/subcategory/<?php echo e($product->sub_type); ?>"><?php echo e($product->subcategory); ?></a></li>
    <li><a href="#"><?php echo e($product->name); ?></a></li>
  </ul>
  <!-- END BREAD CRUMBS -->

  <!-- PRODUCT CARD -->
  <div class="product-card">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="product-card-title">
            <h3><?php echo e($product->name); ?></h3>
          </div>
        </div>
        <div class="col-sm-5 col-md-12 col-lg-5">
          <div class="product-card-wrapper">
            <div class="product-card-images">
              <?php if(!empty($product->images)): ?>

              <?php

              $images = json_decode($product->images);
              $max = count($images) - 1;
              if ($max > 4) {
                $max = 4;
              }
              ?>

              <?php $i = 0; ?>

              <?php if(count($images)>1): ?>
              <div class="product-card__mini-items">

                <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a class="products-cards__item sb" href="/storage/<?php echo e($image); ?>"><img src="/storage/<?php echo e($image); ?>" style="width: 71px;height: 83px"></a>
                <?php
                unset($images[$i]);
                $i++;
                if ($i == $max) {
                  break;
                }
                ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div>
              <div class="product-card__big-img">
                <a class="sb" href="/storage/<?php echo e($images[$max]); ?>">
                  <img id="expandedImg" src="/storage/<?php echo e($images[$max]); ?>" style="width: 396px;height: 329px;">
                  <span class="zoom">
                    <p data-zoom="1">Увеличить <img src="/images/search.png" alt=""></p>
                  </span>
                </a>
              </div>
              <?php else: ?>
              <div class="product-card__big-img">
                <a class="sb" href="/storage/<?php echo e($images[0]); ?>">
                  <img id="expandedImg" src="/storage/<?php echo e($images[0]); ?>" style="width: 396px;height: 329px;">
                  <span class="zoom">
                    <p>Увеличить <img src="/images/search.png" alt=""></p>
                  </span>
                </a>
              </div>
              <?php endif; ?>
              <?php endif; ?>
            </div>
          </div>
          <div class="share">
            <p>Поделиться:</p>
            <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="https://yastatic.net/share2/share.js"></script>
            <div class="ya-share2" data-services="vkontakte,facebook,twitter,whatsapp,telegram"></div>
            <!-- <ul class="social-networks">
            <?php $__currentLoopData = $links; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li><a href="<?php echo e($link->link); ?>"><img src="/images/<?php echo e($link->type); ?>.png"></a></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </ul> -->
          </div>
        </div>

        <div class="col-sm-7 col-md-12 col-lg-7">
          <div class="product-card-wrapper">
            <div class="product-card-info">
              <div class="product-card__brand mb-3">
                <p>Штрих-код:</p>
                <p><?php echo e($product->shtrih); ?></p>
              </div>
              <div class="product-card__brand">
                <p>Бренд:</p>
                <a href="#"><?php echo e($product->brand); ?></a>
                <div class="brand-img">
                  <img src="/storage/<?php echo e($product->icon); ?>" style="width: 115px;height: 49px;">
                </div>
                <p><?php echo e($product->country); ?></p>
                <div class="country-img">
                  <img src="/storage/<?php echo e($product->flag); ?>" style="width: 30px;height: 16px;">
                </div>
              </div>
              <div class="star-line">
                <ul>
                  <?php for($i = 1; $i <= 5; $i++): ?> <?php if($i <=$rating): ?> <li style="color: #eeb900">&#9733;</li>
                    <?php else: ?>
                    <li style="color: #d4d4d4;">&#9733;</li>
                    <?php endif; ?>
                    <?php endfor; ?>
                </ul>
              </div>
              <div class="price">
                <?php if(!empty($product->s_price)): ?>
                <h6><?php echo e($product->s_price); ?> тнг</h6>
                <p>
                  <span><?php echo e($product->f_price); ?> тнг</span>
                  / <?php echo e($product->volume); ?> мл</p>
                <?php else: ?>
                <h6><?php echo e($product->f_price); ?> тнг</h6>
                <?php endif; ?>
              </div>
              <div class="count">
                <div class="number-input operations-block">
                  <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation"><img src="/images/minus.png" alt=""></span>
                  <input class="quantity input-text" min="1" name="count" value="1" type="number" id="card_count">
                  <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation"><img src="/images/plus.png" alt=""></span>
                </div>
                <?php if(!empty($product->count) AND $product->count!=0): ?>
                <p><img src="/images/ok-sm.png" alt="">В наличии</p>
                <?php else: ?>
                <p><img src="/images/ok-sm.png" alt="">Нет в наличии</p>
                <?php endif; ?>
              </div>
              <?php if(!empty($product->count) AND $product->count!=0): ?>
                <div class="purchase-row">
                    <div class="basket-btn">
                    <button id="<?php echo e($product->id); ?>" onclick="addToCart(this.id,card_count.value)">В КОРЗИНУ</button>
                    </div>
                    <div class="one-click">
                    <a data-toggle="modal" data-target="#popUpWindow" href="#"><img src="/images/click.png" alt="">Заказ в
                        1 клик</a>
                    </div>
                    <!-- popUpWindow zakaz v 1 klik -->
                    <div class="modal fade" id="popUpWindow">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="container">
                            <div class="row">
                            <div class="col-sm-12">
                                <!-- header -->
                                <div class="modal-header">
                                <h5 class="modal-title">Заказать</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <!-- body -->
                                <div class="modal-header">
                                <form role="form" id="one_click_form">
                                    <div class="form-group">
                                    <input type="hidden" name="product_id" value="<?php echo e($product->id); ?>">
                                    <input type="text" class="form-control" name="name" placeholder="Ваше имя" />
                                    <input type="text" class="form-control" name="phone" placeholder="Номер телефона">
                                    <textarea name="message" placeholder="Сообщение"></textarea>
                                    </div>
                                </form>
                                </div>
                                <!-- footer -->
                                <div class="modal-footer">
                                <a href="#" id="one_click_order">Заказать</a>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- popUpWindow zakaz v 1 klik -->
                    <div class="add-btns">
                    <a href="#"><img src="/images/libra-btn.png" alt="" onclick="add_comparison('<?php echo e($product->id); ?>')"></a>
                    <a href="#"><img src="/images/like-btn-green.png" alt="" id="<?php echo e($product->id); ?>" onclick="add_like(this.id)"></a>
                    </div>

                </div>
              <?php endif; ?>
              <div class="product-tabs">
                <div class="item active-product">
                  <a href="#tab1" onclick="e.preventDefault();">Состав</a>
                </div>
                <div class="item">
                  <a href="#tab2" onclick="e.preventDefault();">Способ использования</a>
                </div>
              </div>
              <div class="product-tabs-content">
                <div class="product-tab-item product-tab-item-active" id="tab1">
                  <?php echo e($product->composition); ?>

                </div>
                <div class="product-tab-item" id="tab2">
                  <?php echo e($product->mode); ?>

                </div>
              </div>
              <!-- <div class="product-description">
                  <h5>Описание товара:</h5>
                  <p><?php echo $desc1; ?> </p>
                  <div class="show-more">
                    <p data-toggle="collapse" data-tab="1" data-target="#demo">Узнать больше <img src="/images/collapse-arrow.png" alt=""></p>
                  </div>
                  <div id="demo" class="collapse product-description">
                    <p><?php echo $desc2; ?></p>
                  </div>
                </div> -->
            </div>
          </div>
        </div>
        <div class="col-xl-7">
          <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Преимущества </a>
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Оплата</a>
              <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Доставка
              </a>
            </div>
          </nav>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
              <div class="row">
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item">
                    <div class="icon">
                      <img src="/images/tab-icon1.png" alt="">
                    </div>
                    <div class="text-block">
                      <h6>Оперативно</h6>
                      <p>Товар на складе,доставим <br>за минимальный срок </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 ">
                  <div class="tab-item">
                    <div class="icon">
                      <img src="/images/tab-icon2.png" alt="">
                    </div>
                    <div class="text-block">
                      <h6>Богатый ассортимент</h6>
                      <p>Поступления каждую неделю, <br>у нас самая свежая продукция</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item">
                    <div class="icon">
                      <img src="/images/tab-icon3.png" alt="">
                    </div>
                    <div class="text-block">
                      <h6>Приятный сюрприз</h6>
                      <p>Положим подарочек в каждый заказ </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="tab-item">
                    <div class="icon">
                      <img src="/images/tab-icon4.png" alt="">
                    </div>
                    <div class="text-block">
                      <h6>Вкусная цена</h6>
                      <p>Поступления каждую неделю,у нас самая свежая продукция</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab" style="padding: 1.5rem 3rem 2rem 2rem !important;">
              <div class="row">
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/dostavka1.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>На сайте картой VISA/Mastercard</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/dostavka2.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Наличными курьеру</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/dostavka3.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Оплата при самовывозе</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/dostavka4.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>На карту “Каспи”</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
              <div class="row">
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/advantage1.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Бесплатная доставка по Алматы при заказе от 7 000 тнг </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/advantage2.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Бесплатная доставка по РК при заказе от 10 000 тнг</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/advantage3.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Доставка до двери</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/advantage4.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Минимальные сроки (по Алматы 1раб день, по РК 2-4 раб дня)</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-12 col-lg-12">


            <!-- ОТЗЫВЫ!НАДО ДОБАВИТЬ СТИЛИ -->
            <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="main-box__review">
              <img src="<?php echo e(Voyager::image($comment->avatar)); ?>" alt="..." style="margin-top: 7px;">
              <div class="review__info">
                <p class="name"><?php echo e($comment->name); ?></p>
                <p class="reviews__text"><?php echo e($comment->text); ?></p>
                <p class="date"><?php echo e($comment->created_at); ?></p>
              </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <!-- //// -->

            <div class="add-review">
              <button onclick="diplay_hide('#block_id');return false;"><img src="/images/quote-icon.png" alt="">Добавить
                отзыв</button>
            </div>
            <div class="container comment" id="block_id" style="display: none;">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form__box">
                    <p>Оставить отзыв</p>
                    <div class="rating">
                      <div class="rating-item active" data-rate="1">&#9733;</div>
                      <div class="rating-item" data-rate="2">&#9733;</div>
                      <div class="rating-item" data-rate="3">&#9733;</div>
                      <div class="rating-item" data-rate="4">&#9733;</div>
                      <div class="rating-item" data-rate="5">&#9733;</div>
                    </div>
                    <form action="/product-card/comment/<?php echo e($product->id); ?>" method="POST">
                      <?php echo csrf_field(); ?>
                      <input type="text" name="rating" required style="width: 1px; height:1px; padding: 0" id="rate_field">
                      <textarea type="textarea" placeholder="Сообщение" style="height: 150px;" name="text" required></textarea>
                      <input type="submit" class="button" name="" value="Отправить">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-4 col-md-12 col-lg-4 mt-2">
          <div class="advised-title">
            <h5>Товары, схожие по результату действия:</h5>
          </div>
          <?php $__currentLoopData = $similar_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="advised-item">
            <a href="/product-card/<?php echo e($product->id); ?>">
              <div class="icon">
                <img src="<?php echo e(Voyager::image($product->cover)); ?>" alt="" style="width: 100%;height: 100%">
              </div>
              <p><?php echo e($product->name); ?></p>
            </a>
          </div>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          <!-- <div class="subscribe">
            <form action="/home/subscription" method="POST">
              <?php echo csrf_field(); ?>
              <h6>Хотите быть в курсе <br> всех новостей?</h6>
              <p>Подпишитесь на рассылку!</p>
              <input type="email" name="email" placeholder="Введите Электронную почту" required>
              <input type="submit" value="ПОДПИСАТЬСЯ">
            </form>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <!-- END PRODUCT CARD -->
</main>


<?php if(Session::has('send')): ?>
<script type="text/javascript">
  sweetalert('success', 'Спасибо,ваш отзыв находится в обработке!', 3000);
</script>
<?php endif; ?>
<!-- END MAIN -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layout", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/japananimetime/Projects/organic/resources/views/product-card.blade.php ENDPATH**/ ?>