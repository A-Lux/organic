<?php $__env->startSection('body'); ?>
<main>
	  <div class="container new3">
    <div class="row">
      <div class="col-sm-12"> 
        <p class="title wow fadeInLeft"><a href="/news">Все новости</a>/<?php echo e($news->title); ?></p>
        <span class="line wow fadeInLeft"></span>
        <div class="box wow fadeInLeft">
          <!-- <div class="box-video">
            <img src="images/play-button.png">
            <a align="center" href="#">Organic Market</a>
          </div> -->
          <div class="box-text" style="word-wrap: break-word;">
            <?php echo $news->body; ?>

          </div>
          <div class="page">
            <?php if(session('hide')!='prev'): ?>
            <a id="down" class="wow fadeInLeft" href="/news-card/<?php echo e($news->id); ?>?type=prev">Предыдущая новость</a>
            <?php endif; ?>

            <?php if(session('hide')!='next'): ?>
            <a id="up" class=" wow fadeInRight" href="/news-card/<?php echo e($news->id); ?>?type=next">Следующая новость</a>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("layout", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\organic\resources\views/news.blade.php ENDPATH**/ ?>