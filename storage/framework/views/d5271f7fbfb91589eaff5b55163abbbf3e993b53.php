<?php $__env->startSection('body'); ?>

<?php if(session()->has('deleted')): ?>
<script type="text/javascript">
  sweetalert('success','Товар успешно удален с сравнения!',3000);
</script>
<?php endif; ?>
<div class="main-srav">
    <ul class="bread-crumbs container">
      <li><a href="/">Главная</a></li>
      <li><a href="#">Сравнение</a></li>
    </ul>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <?php if(empty($products)): ?>
                <h3 style="font-family: 'Poppins', sans-serif; color: red; font-size: 36px; margin-bottom: 40px; margin-top: 30px;">Нет товаров!</h3>
            <?php else: ?>
        </div>
    </div>
</div>
    <section class="srav">
        <div class="container">
            <div class="row">
                <div class="srav-title">
                    <h3>Сравнение товаров</h3>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <td class="comparison-table__feature-name"></td>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td>
                                    <div class="comparison-table__product-title">
                                        <a class="comparison-table__product-delete" href="/del_comparison?id=<?php echo e($product->id); ?>">x</a>
                                        <div class="comparison-table__product">
                                            <div class="compras-srav-head" style="display: flex;flex-direction: column;justify-content: center;align-items: center;">
                                            <img
                                                src="<?php echo e(Voyager::image($product->cover)); ?>">

                                            <a href="/product-card/<?php echo e($product->id); ?>" class="comparison-table__product-name"><?php echo e($product->name); ?></a>
                                            <p class="comparison-table__product-price"><?php echo e($product->final_price); ?> тг</p>
</div>
                                            <button class="comparison-table__product-buy" onclick="addToCart(<?php echo e($product->id); ?>)">Купить</button>
                                        </div>
                                    </div>
                                </td>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row" class="comparison-table__feature-name">Цена : </th>
                                 <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td><?php echo e($product->final_price); ?> тг</td>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tr>
                            <tr>
                                <th scope="row" class="comparison-table__feature-name">Производитель : </th>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td><?php echo e($product->country); ?></td>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tr>
                            <tr>
                                <th scope="row" class="comparison-table__feature-name">Доступность : </th>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($product->count>0): ?>
                                <td>В наличии</td>
                                <?php else: ?>
                                <td>Нет в наличии</td>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tr>
                            <!-- <tr>
                                <th scope="row" class="comparison-table__feature-name">Рейтинг: </th>
                                <td>На основании 0 отзыва(ов)</td>
                                <td>На основании 0 отзыва(ов)</td>
                            </tr> -->
                            <tr>
                                <th scope="row" class="comparison-table__feature-name">Краткое описание : </th>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <td><?php echo e($product->description); ?></td>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layout", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\organic\resources\views/comparisons.blade.php ENDPATH**/ ?>