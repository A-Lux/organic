<?php $__env->startSection('body'); ?>
  <!-- MAIN -->
  

<style>
	.catalog_right-side .catalog-filter .filter-wrap.showAmount li::after {
		display: none;
	}
</style>

  <main class="main">
    <!-- BREAD CRUMBS -->
    <ul class="bread-crumbs container">
      <li><a href="/">Главная</a></li>
      <?php if(!isset($stock)): ?>
      <li><a class="now-page" href="#"><?php echo e($main_data->name); ?></a></li>
      <?php else: ?>
       <li><a href="#"><?php echo e($stock); ?></a></li>
      <?php endif; ?>
    </ul>
    <!-- END BREAD CRUMBS -->
    <?php if(isset($action)): ?>
    <script type="text/javascript">
      window.action='<?php echo e($action); ?>';
    </script>
    <?php else: ?>
    <script type="text/javascript">
      window.action='/products/stock';
    </script>
    <?php endif; ?>
    <!-- ORGANIC CATALOG -->
    <div class="organic-catalog">
      <div class="container">
        <div class="catalog-section">
          <div class="title">
            <?php if(!isset($stock)): ?>
            <h3><?php echo e($main_data->name); ?></h3>
            <p style="word-wrap: break-word;"><?php echo e($main_data->description); ?></p>
            <?php else: ?>
            <h3><?php echo e($stock); ?></h3>
            <?php endif; ?>
          </div>
        </div>

        <div class="catalog-wrapper">
          <div class="catalog_left-side">
            <div class="panel">
              <!-- first panel start here -->
              <div class="tab_panel sections">
                <div class="tab_heading">
                  <h4>Разделы</h4>
                  <span><i class="material-icons">keyboard_arrow_up</i></span>
                </div>
                <div class="tab_content">
                  <ul>
                    <?php $__currentLoopData = $section_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if(!empty($subcategories[$category->id])): ?>
                    <li id="toggle-btn" data-toggle="collapse" data-target="#toggle-example<?php echo e($loop->iteration); ?>">
						<span><?php echo e($category->name); ?></span>
                     
                    </li>
					   <div id="toggle-example<?php echo e($loop->iteration); ?>" class="collapse in">
                        <ul>
                          <?php $__currentLoopData = $subcategories[$category->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <li><a class="prevent_collapse" onclick="catalog_filter('/products/subcategory/<?php echo e($subcategory->id); ?>',0)"><span><?php echo e($subcategory->name); ?></span></a></li>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                      </div>
                    <?php else: ?>
                    <li><a href="/products/category/<?php echo e($category->id); ?>"><span><?php echo e($category->name); ?></span></a></li>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </ul>
                </div>
              </div>

              <!-- second panel start here -->
              <div class="tab_panel brands">
                <div class="tab_heading">
                  <h4>Бренды</h4>
                  <span><i class="material-icons">keyboard_arrow_down</i></span>
                </div>
                <div class="tab_content">
                  <ul class="checkbox-list">
                    <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                      <div class="round">
                        <input type="checkbox" id="<?php echo e($brand->id); ?>" onclick="catalog_filter(0,'<?php echo e($brand->id); ?>')"/>
                        <label for="<?php echo e($brand->id); ?>"></label>
                      </div>
                      <p><?php echo e($brand->name); ?></p>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </ul>
                </div>
              </div>

            <!-- three panel start here -->
            <div class="tab_panel prices">
              <div class="tab_heading">
                <h4>Цена (тнг.)</h4>
                <span><i class="material-icons">keyboard_arrow_down</i></span>
              </div>
              <div class="tab_content">
                <div slider id="slider-distance">
                  <!-- <div>
                    <div inverse-left style="width:70%;"></div>
                    <div inverse-right style="width:70%;"></div>
                    <div range style="left:30%;right:40%;"></div>
                    <span thumb style="left:30%;"></span>
                    <span thumb style="left:60%;"></span>
                    <div sign style="left:30%;">
                      <span id="value">30</span>
                    </div>
                    <div sign style="left:60%;">
                      <span id="value">60</span>
                    </div>
                  </div> -->
                  <!-- <input type="range" tabindex="0" value="30" max="100" min="0" step="1" oninput="
                  this.value=Math.min(this.value,this.parentNode.childNodes[5].value-1);
                  var value=(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.value)-(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.min);
                  var children = this.parentNode.childNodes[1].childNodes;
                  children[1].style.width=value+'%';
                  children[5].style.left=value+'%';
                  children[7].style.left=value+'%';children[11].style.left=value+'%';
                  children[11].childNodes[1].innerHTML=this.value;" />

                  <input type="range" tabindex="0" value="60" max="100" min="0" step="1" oninput="
                  this.value=Math.max(this.value,this.parentNode.childNodes[3].value-(-1));
                  var value=(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.value)-(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.min);
                  var children = this.parentNode.childNodes[1].childNodes;
                  children[3].style.width=(100-value)+'%';
                  children[5].style.right=(100-value)+'%';
                  children[9].style.left=value+'%';children[13].style.left=value+'%';
                  children[13].childNodes[1].innerHTML=this.value;" /> -->
                </div>
                <div class="slider-price">
                  <div class="slider-price-input">
                    <input type="text" id="amount_1">
                    <input type="text" id="amount_2">
                  </div>
                    <div class="slide" id="slider-range"></div>
                </div>
                <div class="prices-buttons">
                  <button type="button" name="price-btn" onclick="catalog_filter(0,0)">
                    Применить
                  </button>
                  <button type="button" name="default-btn" onclick="sbros()">
                    Сбросить
                  </button>
                </div>
              </div>
            </div>
            </div> <!-- main panel close here -->
          </div>

          <div class="catalog_right-side">
            <div class="catalog-filter">
              <p>Сортировать по:</p>
              <ul class="filter-wrap">
                <li onclick="catalog_sort(this.value,this.id)" id="seen" value="0">Популярности</li>
                <li onclick="catalog_sort(this.value,this.id)" id="final_price" value="0">Цене</li>
                <li onclick="catalog_sort(this.value,this.id)" id="name" value="0">Названию</li>
                <li onclick="catalog_sort(this.value,this.id)" id="in_stock" value="0">В наличии</li>
                <li onclick="catalog_sort(this.value,this.id)" id="discount" value="0">Скидки</li>
                <!-- <li onclick="catalog_sort(this.value,this.id)" id="skin_type" value="0">Типу кожи</li> -->
              </ul>
            </div>
			<div class="catalog_right-side">
            
              <?php
              $i=1;
              ?>
            <div id="catalog_products"> 

              <!-- ЕСЛИ НЕ ТОВАРОВ В ДАННОЙ КАТЕГОРИИ -->
                <?php if(empty($products)): ?>
                <h3 class="no-products-title">В данной категории нет товаров!</h3>
                <?php endif; ?>


                        <div class="catalog-products-wrapper">
                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="catalog-product-item">
                          <div class="catalog-img">
                            <a href="/product-card/<?php echo e($product->id); ?>"><img src="/storage/<?php echo e($product->cover); ?>"></a>
                            <?php if(!empty($product->discount)): ?>
                            <span class="catalog-stock">-<?php echo e($product->discount); ?></span>
                            <?php endif; ?>
                          </div>
                          <h4><a href="/product-card/<?php echo e($product->id); ?>"><?php echo e($product->name); ?></a> / <?php echo e($product->volume); ?> мл.</h4>
                          <p><?php echo e(substr($product->text_cover, 0, strrpos(substr($product->text_cover, 0, 50), ' '))); ?></p>
                           <?php if(!empty($product->s_price)): ?>
							<?php if($product->s_price != $product->f_price): ?>
                          <div class="price"><?php echo e($product->s_price); ?> тнг
                            <span>
                            	<?php echo e($product->f_price); ?> тнг
                            </span>
                          </div>
							<?php else: ?>
								<div class="price"><?php echo e($product->f_price); ?> тнг </div>
							<?php endif; ?>
                          <?php else: ?>
                          <div class="price"><?php echo e($product->f_price); ?> тнг </div>
                          <?php endif; ?>
							<?php if(!empty($product->count) AND $product->count!=0): ?>
							<p><img src="/images/ok-sm.png" alt="">В наличии</p>
							<?php else: ?>
							<p><img src="/images/notok-sm.png" alt="">Нет в наличии</p>
							<?php endif; ?>
                          <?php 
                          $rating = \App\Product::calculateRating($product->id);
                          ?>
                          <div class="star-line">
                            <ul>
                              <?php for($i = 1; $i <= 5; $i++): ?>
                              <?php if($i <= $rating): ?>
                              <li style="color: #eeb900">&#9733;</li>
                              <?php else: ?>
                              <li style="color: #d4d4d4;">&#9733;</li>
                              <?php endif; ?>
                              <?php endfor; ?>
                            </ul>
                          </div>
							<div class="correction__wrapper">
								<?php if(!empty($product->count) AND $product->count!=0): ?>
							  <button class="btn-basket" name="btn-basket"     id="<?php echo e($product->id); ?>"   onclick="addToCart(this.id)">
								В КОРЗИНУ
							  </button>
							<?php endif; ?>
								<a href="/likes"><img src="/images/heart.png"></a>
							</div>
                        </div>

                        <?php

                        unset($products[$key]);
                        if($i==9){
                        break;
                        }
                        else{
                        $i++;
                        }
                        
                        ?>
                        
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </div>
        <!-- БЛОК ПОКАЗАТЬ ЕЩЕ -->
         <?php if(!empty($products)): ?>
        <div class="catalog-products-wrapper-more" id="moreProduct" style="display: none;">
          <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="catalog-product-item">
                          <div class="catalog-img">
                            <a href="/product-card/<?php echo e($product->id); ?>"><img src="/storage/<?php echo e($product->cover); ?>"></a>
                            <?php if(!empty($product->discount)): ?>
                            <span class="catalog-stock">-<?php echo e($product->discount); ?></span>
                            <?php endif; ?>
                          </div>
                          <h4><a href="/product-card/<?php echo e($product->id); ?>"><?php echo e($product->name); ?></a> / <?php echo e($product->volume); ?> мл.</h4>
                          <p><?php echo e($product->text_cover); ?></p>
                           <?php if(!empty($product->s_price)): ?>
                          <div class="price"><?php echo e($product->s_price); ?> тнг
                            <span>
                            <?php echo e($product->f_price); ?> тнг
                            </span>
                          </div>
                          <?php else: ?>
                          <div class="price"><?php echo e($product->f_price); ?> тнг </div>
                          <?php endif; ?>
                        
                          <?php 
                          $rating = \App\Product::calculateRating($product->id);
                          ?>
                          <div class="star-line">
                            <ul>
                              <?php for($i = 1; $i <= 5; $i++): ?>
                              <?php if($i <= $rating): ?>
                              <li style="color: #eeb900">&#9733;</li>
                              <?php else: ?>
                              <li style="color: #d4d4d4;">&#9733;</li>
                              <?php endif; ?>
                              <?php endfor; ?>
                            </ul>
                          </div>
                          <button class="btn-basket" name="btn-basket"     id="<?php echo e($product->id); ?>"   onclick="addToCart(this.id)">
                            В КОРЗИНУ
                          </button>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
					  <!-- Fixing pagination-->                    
                      <div class="show-all-products">
						  
                        <!--<a href="#moreProduct" class="more-btn">ПОКАЗАТЬ ЕЩЁ</a>-->
						  <?php echo e($products->links()); ?>

						  <span id="currentURL" style="display: none"><?php echo e($url); ?></span>
                      </div>
           <?php endif; ?>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- END ORGANIC CATALOG -->

    <!-- SUSCRIPTION BLOCK -->
    <div class="white-bg-wrapp">
      <div class="subscription-block container wow rotateInUpLeft" data-wow-duration="1s">
        <div class="subscription-absolute">
          <div class="subscription-content">
            <h3>Хотите быть в курсе всех новостей?</h3>
            <span>Подпишитесь на рассылку!</span>
            <div class="subscription-wrapp">
              <div class="sub-social">
                <h4>Следите за нами в соц сетях:</h4>
                <ul class="social-networks">
                   <?php $__currentLoopData = $links; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li><a href="<?php echo e($link->link); ?>"><img src="/images/<?php echo e($link->type); ?>.png"></a></li>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
              <div class="sub-row">
                <div class="sub-button-input">
                  <form action="/subscription" method="POST">
                    <?php echo csrf_field(); ?>
                    <input type="email" name="email" placeholder="Введите Электронную почту">
                    <input type="submit" class="sub-btn" value="ПОДПИСАТЬСЯ">
                  </form>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END SUSCRIPTION BLOCK -->
  </main>
  <!-- END MAIN -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make("layout", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/japananimetime/Projects/organic/resources/views/catalog.blade.php ENDPATH**/ ?>