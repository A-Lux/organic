$(document).ready(function(){
 
$(window).scroll(function(){
if ($(this).scrollTop() > 350) {
$('.scrollup').fadeIn();
} else {
$('.scrollup').fadeOut();
}
});
 
$('.scrollup').click(function(){
$("html, body").animate({ scrollTop: 0 }, 600);
return false;
});
 
});
(function($) {

  new WOW().init();


  $('.bonus-section__line ul li a').click(function(e){
    e.preventDefault();
    var target = $(e.target);
    var attr = $(this).parent('li')[0].getAttribute('data-img');
    var activeImg = $(this).parent('li')[0].getAttribute('data-active');
    var defaultImg = 'http://organic.ibeacon.kz/images/'+attr+ '.png'
    var img = 'http://organic.ibeacon.kz/images/'+attr+'-active.png';
    if( attr == "shape1"){
        if(activeImg == "1"){
            $(this) + target.css("background-image","url("+img+")");
            $(this).parent('li')[0].setAttribute("data-active", "0");
            $($(this).parent('li')[0]).find('span')[0].style.fontSize = "16px";

        }else{
            $(this) + target.css("background-image","url("+defaultImg+")");
            $(this).parent('li')[0].setAttribute("data-active", "1");
            $($(this).parent('li')[0]).find('span')[0].style.fontSize = "14px";
        }
    }else if( attr == "shape2"){
        if(activeImg == "1"){
            $(this) + target.css("background-image","url("+img+")");
            $(this).parent('li')[0].setAttribute("data-active", "0");
            $($(this).parent('li')[0]).find('span')[0].style.fontSize = "16px";
        }else{
            $(this) + target.css("background-image","url("+defaultImg+")");
            $(this).parent('li')[0].setAttribute("data-active", "1");
            $($(this).parent('li')[0]).find('span')[0].style.fontSize = "14px";
        }
    }else if( attr == "shape3"){
        if(activeImg == "1"){
            $(this) + target.css("background-image","url("+img+")");
            $(this).parent('li')[0].setAttribute("data-active", "0");
            $($(this).parent('li')[0]).find('span')[0].style.fontSize = "16px";
        }else{
            $(this) + target.css("background-image","url("+defaultImg+")");
            $(this).parent('li')[0].setAttribute("data-active", "1");
            $($(this).parent('li')[0]).find('span')[0].style.fontSize = "14px";
        }
    }
    
});

  // superfish
	$('.menu').superfish({
        delay:200,
        animation: {opacity:'show', height:'show'},
        animationOut: {opacity:'hide', height:'hide'}
    });
// end superfish


    $('.bar-btn').click(function () {
        $('.bar-list').show();
    });

    $(document).ready(function(){
        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        $('.phone').mask('0000-0000');
        $('.index').mask('000000');
        $('.phone_with_ddd').mask('(00) 0000-0000');
        $('.phone_us').mask('+7(000) 000-00-00');
        $('.mixed').mask('AAA 000-S0S');
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.money2').mask("#.##0,00", {reverse: true});
        $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
                'Z': {
                    pattern: /[0-9]/, optional: true
                }
            }
        });
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
        $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
        $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
        $('.fallback').mask("00r00r0000", {
            translation: {
                'r': {
                    pattern: /[\/]/,
                    fallback: '/'
                },
                placeholder: "__/__/____"
            }
        });
        $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
    });
	
	$('.bread-crumbs li .now-page').click(function(e){
		e.preventDefault();
	});
    
    $(document).ready(function(){
        $('.more-btn').click(function(e){
            e.preventDefault();
    
            var attr = $(this).attr('href');
      
            if ($(attr).css('display') == 'none'){ 
                $(attr).animate({height: 'show'}, 500); 
            }else{     
                $(attr).animate({height: 'hide'}, 50); 
            }
            
        });
        $('.mobile-drop').click(function(e){
            e.preventDefault();

            $(this).next().slideToggle('mobile-menu-dropdown-active');
        });

        $('.order-btn-more').click(function(){
            let attr = $(this).attr('data-tab');

            $("#" + attr).toggleClass('show-orders');
        });

        $('.more-btn').click(function(e){
            e.preventDefault();
    
            var attrHtml = $(this).attr('data-tab');

            if(attrHtml == '1'){
                $('.more-btn').html('СКРЫТЬ');
                $('.more-btn').attr('data-tab','0');
            }else{
                $('.more-btn').html('ПОКАЗАТЬ ЕЩЕ');
                $('.more-btn').attr('data-tab','1');
            }
           
            
        });

        $('.show-more p').click(function(e){
            e.preventDefault();

            var attrHtml = $(this).attr('data-tab');

            if(attrHtml == '1'){
                $('.show-more p').html('Скрыть');
                $('.show-more p').attr('data-tab','0');
            }else{
                $('.show-more p').html('Узнать больше  <img src="/images/collapse-arrow.png" alt="">');
                $('.show-more p').attr('data-tab','1');
            }
           

        });

        $('.zoom p').click(function(e){
            e.preventDefault();

            var attrZoom = $(this).attr('data-zoom');

            if(attrZoom == '1'){
                $('.zoom p').html('Уменьшить <img src="/images/search.png" alt="">');
                $('.zoom p').attr('data-zoom','0');
                $('#expandedImg').css('transform','scale(1.5)');
            }else{
                $('.zoom p').html('Увеличить <img src="/images/search.png" alt="">');
                $('.zoom p').attr('data-zoom','1');
                $('#expandedImg').css('transform','scale(1)');
            }
        });

        $('.product-card__mini-items img').click(function(e){
            e.preventDefault();

            var attrSrc = $(this).attr('src');
            
            $('#expandedImg').attr('src',attrSrc);
        });

    });

    
	
	$(document).ready(function(){
		$('.close').click(function(e){
			e.preventDefault();
			$('#product').hide();
		});
	});
    $(document).ready(function() {
        $('#carouselOne').owlCarousel({
            loop:true, //Зацикливаем слайдер
            margin:50, //Отступ от элемента справа в 50px
            nav:true, //Отключение навигации
            autoplay:true, //Автозапуск слайдера
            smartSpeed:1000, //Время движения слайда
            autoplayTimeout:3000, //Время смены слайда
            responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
        
        var owlCarous = $('#products-wrapper');

        $('#products-wrapper').owlCarousel({
            loop:false, //Зацикливаем слайдер
            margin:50, //Отступ от элемента справа в 50px
            nav:false, //Отключение навигации
            autoplay:false, //Автозапуск слайдера
            smartSpeed:1000, //Время движения слайда
            autoplayTimeout:2000, //Время смены слайда
            responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                0:{
                    items:2
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        });
        $('.product-arrow__left').click(function(e) {
			e.preventDefault();
			owlCarous.trigger('prev.owl.carousel', [300]);
		})

		$('.product-arrow__right').click(function(e) {
			e.preventDefault();
			owlCarous.trigger('next.owl.carousel');
		})
		// arrows
	// 	$('.banner-arrow__left').click(function(e) {
	// 		e.preventDefault();
	// 		owlCar.trigger('prev.owl.carousel', [300]);
	// 	})

	// 	$('.banner-arrow__right').click(function(e) {
	// 		e.preventDefault();
	// 		owlCar.trigger('next.owl.carousel');
	// 	})
	// 	// arrows end
    // });
		var owlCar = $('#catalogCarousel');

        $('#catalogCarousel').owlCarousel({
            loop:false, //Зацикливаем слайдер
            margin:50, //Отступ от элемента справа в 50px
            nav:false, //Отключение навигации
            autoplay:false, //Автозапуск слайдера
            smartSpeed:1000, //Время движения слайда
            autoplayTimeout:2000, //Время смены слайда
            responsive:{ //Адаптивность. Кол-во выводимых элементов при определенной ширине.
                0:{
                    items:2
                },
                600:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        });
		// arrows
		$('.banner-arrow__left').click(function(e) {
			e.preventDefault();
			owlCar.trigger('prev.owl.carousel', [300]);
		})

		$('.banner-arrow__right').click(function(e) {
			e.preventDefault();
			owlCar.trigger('next.owl.carousel');
		})
		// arrows end
    });

   /* OffCanvas Menu
    * ------------------------------------------------------ */
    var clOffCanvas = function() {

        var menuTrigger     = $('.header-menu-toggle'),
            nav             = $('.header-nav'),
            closeButton     = nav.find('.header-nav__close'),
            siteBody        = $('body'),
            mainContents    = $('section, footer');

        // open-close menu by clicking on the menu icon
        menuTrigger.on('click', function(e){
            e.preventDefault();
            // menuTrigger.toggleClass('is-clicked');
            siteBody.toggleClass('menu-is-open');
        });

        // close menu by clicking the close button
        closeButton.on('click', function(e){
            e.preventDefault();
            menuTrigger.trigger('click');
        });

        // close menu clicking outside the menu itself
        siteBody.on('click', function(e){
            if( !$(e.target).is('.header-nav, .header-nav__content, .header-menu-toggle, .header-menu-toggle span') ) {
                // menuTrigger.removeClass('is-clicked');
                
            }
        });

    };

   /* Initialize
    * ------------------------------------------------------ */
    (function ssInit() {
        clOffCanvas();
    })();


})(jQuery);
function add() {
    let x;
    x = document.getElementById("width").value;
    x++;
    document.getElementById("width").innerHTML = x;
// END

}

// ONLINE PAY
$(function() {
    $('.online-pay ul').hide();
    $('.online-pay-btn').on('click', function(e) {
        e.preventDefault()
        $('.online-pay ul').slideToggle(500);
        if (!$(this).hasClass('display')) {
            $(this).addClass('display');
            $('.online-pay').css('top', '200px');
            $('.online-pay').css('width', '250px');
        }
        else {
            $('.online-pay').css('top', '350px');
            $('.online-pay').css('width', '');
            $(this).removeClass('display');
        }
    })
})
// END ONLINE PAY

// TABS
$(document).ready(function(){

	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

})

// slider price

 $( function() {
	$('#amount_1').val(11)
	$('#amount_2').val(999999)
    $( "#slider-range" ).slider({
      range: true,
      min: 10,
      max: 1000000,
      values: [ 10, 1000000 ],
      slide: function( event, ui ) {
		  
		//if(isNaN(ui.values[0]) || isNaN(ui.values[1])){
		   	//ui.values[0] = 0;
			//ui.values[1] = 1000000
		//}
		  console.log(ui.values)
        $( "#amount_1" ).val(ui.values[ 0 ] );
        $("#amount_2").val(ui.values[ 1 ]);
      }
    });
    function attachSlider() {
        $('#amount_1').val($('#slider-range').slider("values", 0));
        $('#amount_2').val($('#slider-range').slider("values", 1));
    }
    $('input').change(function(e) {
        var setIndex = (this.id == "amount_2") ? 1 : 0;
        $('#slider-range').slider("values", setIndex, $(this).val())
    })

  } );

$(document).ready(function(){

    $('ul.reviews-imgs li').click(function(){
        var tab_id = $(this).attr('review-tab');

        $('ul.reviews-imgs li').removeClass('active');
        $('.review-content').removeClass('active');

        $(this).addClass('active');
        $("#"+tab_id).addClass('active');
    })
	
	$('.item').click(function(e){
        e.preventDefault();
		$('.item').removeClass('active-product');
		$(this).addClass('active-product');
		var tab = $(this).find('a').attr('href');
		
		$('.product-tab-item').removeClass('product-tab-item-active');
		$(tab).addClass('product-tab-item-active');
    });
    $('.product-tabs .item a').click(function(e){
        e.preventDefault();
    });
    
    $('.sb').click(function(e){
        e.preventDefault();
    });

})
// END TABS

$(document).ready(function() {
    $('.minus').click(function () {
      var $input = $(this).parent().find('input');
      var count = parseInt($input.val()) - 1;
      count = count < 1 ? 1 : count;
      $input.val(count);
      $input.change();
      return false;
    });
    $('.plus').click(function () {
      var $input = $(this).parent().find('input');
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      return false;
    });
  });


  $(function(){
    var $tab_content = $(".tab_panel .tab_content");
    var $tab_heading = $(".tab_panel .tab_heading");
    var $tab_panel = $(".tab_panel");
    var $arrow_class_up = 'keyboard_arrow_up'; // change your icon class
    var $arrow_class_down = 'keyboard_arrow_down'; // change your icon class

    $tab_content.hide(); //hide all the .tab_content

    $tab_panel.find(".tab_content").eq(0).show();
    $tab_heading.on("click", function(){

      $(this).find("span i").text($(this).find("span i").text() ==   $arrow_class_up ? $arrow_class_down : $arrow_class_up);

      // unhide below code if you want one panel to be always opened
      // $(this).parents(".panel").find(".tab_content").stop().slideUp();
      // $(this).next(".tab_content").stop().slideDown();
        $(this).next(".tab_content").stop().slideToggle();
    });
  });

  $(document).ready(function(){
    $("#toggle-btn").click(function(){
      $("#toggle-example").collapse('toggle'); // toggle collapse
    });
  });

  $('.filter-wrap:first-child  li').click(function(){
    $(this).toggleClass('active');
  });

  /*modals show hide*/
$('.overlay,.modal-auth,.modal-register,.modal-feedback').hide();
$('.signin').click(function () {
    $('.modal-auth').slideDown(500);
    $('.overlay').show();
});
$('.signup').click(function () {
    $('.modal-register').slideDown(500);
    $('.overlay').show();
});
// $('.pay-courier').click(function () {
//     $('.modal-courier').slideDown(500);
//     $('.overlay').show();
// });
$('.bonus-btn-more-info').click(function (e) {
    e.preventDefault();
    $('.modal-bonus-info').slideDown(500);
    $('.overlay').show();
});
$('.pay-samov').click(function () {
    $('.modal-samov').slideDown(500);
    $('.overlay').show();
});
$('.btn-call-us').click(function () {
    $('.modal-feedback').slideDown(500);
    $('.overlay').show();
});
$('.close-modal-btn').click(function () {
    $('.modal-register,.modal-auth,.modal-feedback,.modal-courier,.modal-samov,.modal-bonus-info').hide(0);
    $('.overlay').hide();
});
$('.modal-auth-regbtn').click(function(){
    $('.modal-auth').hide(0);
    $('.modal-register').slideDown(500);
});
$('.haveaccount-btn').click(function(e){
    e.preventDefault();
    $('.modal-register').hide(0);
    $('.modal-auth').slideDown(500);
});

/*function myFunction(img) {
    var expandImg = document.getElementById("expandedImg");
    var imgText = document.getElementById("imgtext");
    expandImg.src = imgs.src;
    imgText.innerHTML = imgs.alt;
    expandImg.parentElement.style.display = "block";
}
*/

function diplay_hide(blockId){ 
    if ($(blockId).css('display') == 'none'){ 
        $(blockId).animate({height: 'show'}, 500); 
    }else{     
        $(blockId).animate({height: 'hide'}, 500); 
    }} 





    function catalog_filter(action,brand_id){
      
        if (window.brands==undefined) {
            window.brands={};
        }
        

        if (action!=0) {
            window.action=action;
        }

        if (brand_id!=0) {
            if (!$.isEmptyObject(window.brands)) {

                if (brand_id in window.brands) {
                    delete window.brands[brand_id];
                }
                else{
                    window.brands[brand_id]=brand_id;
                }
            }   
            else{
                window.brands[brand_id]=brand_id;
            }

            
        }
        
        if (!$.isEmptyObject(window.brands)) {
            var data={brands:window.brands};
        }
        else {
            var data={brands:0};
        }

        if (action==0 && brand_id==0) {
           window.amount1=amount_1.value;
           window.amount2=amount_2.value;
        }
        data.amount_1=window.amount1;
        data.amount_2=window.amount2; 

       console.log(window.brands);


        $.ajax({
            url:window.action,
            type:"GET",
            data:data,
            success:function(data){
                catalog_products.innerHTML=data;
            },
            error:function(){
                alert("Ошибка!!!");
            }
        });


    }
	


    function catalog_sort(value,id){


      if (value==0) {
        var sort_type='ASC';
          document.getElementById("seen").style.color='#4c4c4c';
          document.getElementById("final_price").style.color='#4c4c4c';
          document.getElementById("name").style.color='#4c4c4c';
          document.getElementById("in_stock").style.color='#4c4c4c';
          document.getElementById("discount").style.color='#4c4c4c';
          document.getElementById(id).value=1;
          document.getElementById(id).style.color='#d2451f';
      }
      else{
        var sort_type='DESC';
        document.getElementById(id).value=0;
        document.getElementById(id).style.color='#4c4c4c';
      }

        
       $.ajax({ 
           url: window.action,
           method: "GET", // Что бы воспользоваться POST методом, меняем данную строку на POST
           data: {"column":id,"sort_type":sort_type},
           success: function(data) {
           catalog_products.innerHTML=data;
           }
       });

    } 

    function sbros(){
        amount_1.value="";
        amount_2.value="";
    }

    function show_hide(name){
      if (name==0) {
        show_more.innerHTML="Свернуть";
        show_more.name=1;
      }else{
        show_more.innerHTML="Показать еще";
        show_more.name=0;
      }

    }

   let wa = document.getElementsByClassName('contacts')[0].children[1].children[1].children[1].attributes.href.value.replace(/[(-), , -]/g, "");
	document.getElementsByClassName('contacts')[0].children[1].children[1].children[1].attributes.href.value = wa
 let wa2 = document.getElementsByClassName('contacts')[0].children[1].children[1].children[1].attributes.href.value.replace('8', '7');
	document.getElementsByClassName('contacts')[0].children[1].children[1].children[1].attributes.href.value = wa2
