<?php

use Illuminate\Foundation\Inspiring;
use App\Product;
use App\Subcategory;
use Symfony\Component\DomCrawler\Crawler;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('import', function () {
ini_set('max_execution_time', '0');
	
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"http://proxy.paloma365.com/login_ajax.php");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch , CURLOPT_COOKIEJAR, __DIR__.'/cookies.txt');
curl_setopt($ch , CURLOPT_COOKIEFILE, __DIR__.'/cookies.txt');
curl_setopt($ch, CURLOPT_POSTFIELDS, "login=organic2018&password=organic2018");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);

curl_setopt_array(
    $ch, array(
    CURLOPT_URL => 'http://proxy.paloma365.com/company/ajax.php?direct_output=yes&class=guide2xml&method=to_file',
    CURLOPT_POST => 1,
    CURLOPT_POSTFIELDS => "tables[]=s_items&groups[s_items]=0&groups[s_clients]=0&groups[s_type_inout]=0&output_format=json"
));

$output = curl_exec($ch);

$products = json_decode($output, true)['s_items'];

//foreach ($products as $key => $product) {
//    if($product['isgroup'] == 1) {
//        $cat = Subcategory::where('name', $product['name'])->first();
//        if(!$cat) {
//            $cat = Subcategory::create(['name' => $product['name'], 'cat_type' => 7, 'paloma_temp' => substr($product['UID'], 1, -1)]);
//        }
//    }
//}

foreach ($products as $key => $product) {
    if($product['isgroup'] == 0) {
        $prod = Product::where('name', $product['name'])->first();
		//$cat = Subcategory::where('paloma_temp', substr($product['parentid'], 1, -1))->first();
		$cat = Subcategory::where('id', 2)->first();
		if(!$cat || empty($product['name']) || $product['name'] == 'NULL') continue;
		$arr = [
			'name' => $product['name'] != 'NULL' ? $product['name'] : '',
			'description' => $product['description'] != 'NULL' ? $product['description'] : '',
			'f_price' => $product['price'] != 'NULL' ? $product['price'] : 0,
			's_price' => $product['price'] != 'NULL' ? $product['price'] : 0,
			'final_price' => $product['price'] != 'NULL' ? $product['price'] : 0,
			'shtrih' => $product['mainShtrih'] != 'NULL' ? $product['mainShtrih'] : 0,
			//'count' => $product['critical_min'],
		];
		
        if(!$prod) {
			
			$arr['sub_type'] = $cat->id;
            $prod = Product::create($arr);
        } else {
			$prod->fill($arr);
			$prod->save();
		}
    }
}
	
    $today = now()->format('d.m.Y');
    curl_setopt_array(
        $ch, array(
        CURLOPT_URL => 'http://proxy.paloma365.com/company/warehouse/warehouse.php?do=remainscompact',
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => "chb=zaperiod&chb_zaperiod1=".$today."+00%3A00&chb_zaperiod2=".$today."+23%3A59&criticalRemains=no&do_print=true&groupBy=ByArticles&itemid=0&notnullRemains=no&print_type=xls&producerid=0&remainsapid=0&showArticul=no&showBarcode=no&showOnlyStale=no&showPLU=no&showPictures=no&staledays=30&warehouseid=0"
    ));
    
    $output = curl_exec($ch);
    
    curl_close ($ch);

    $fp = fopen('data.xls', 'w');
    fwrite($fp, $output);
    fclose($fp);

    $crawler = new Crawler(file_get_contents('data.xls'));

    $crawler->filter('tbody [class="separate"]')->each(function ($node) {
        $name = $node->parents()->first()->filter('td')->eq(1)->text();
        $count = $node->parents()->first()->filter('td')->eq(3)->filter('.sum')->first()->text();
		$prod = Product::where('name', $name)->first();
		if($prod) {
			$prod->count = (int)$count;
			$prod->save();
		}
    });
	
});



