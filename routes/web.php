<?php
use App\Product;
use App\Promocode;
use App\Subcategory;
use App\ActivePromocode;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::post('promocodes/generate', function(Request $request) {
        Promocode::insert(Promocode::generate($request->amount, $request->percentage));
        return back();
    })
    ->name('generate-promocodes');
    Route::post('/products/{product}/clear-sale', function(Product $product) {
        $product->fill([
            'sale_id' => null
        ])->save();

        return response(200);
    });
    Route::post('promocodes/{promocode}/active', function(Request $request, Promocode $promocode) {
        if(ActivePromocode::first()) {
           ActivePromocode::first()
            ->fill([
                'promocode_id' => $promocode->id
            ])->save();
        }else {
            ActivePromocode::create([
                'promocode_id' => $promocode->id
            ]);
        }
        return back();
    })
    ->name('active-promocode');
	Route::get('product/pages', function(Request $request){
		session(['show' => $request->show]);
		return redirect('admin/products');
	});
});

Route::get('/',"IndexController@show");

Route::get('/set/amount/catalog/items/{amount}', 'ProductsController@setAmount');

Route::get("/news","NewsController@show_all");
Route::get("/news-card/{id}","NewsController@show_card");

Route::get("/section/contact","SectionController@contact");
Route::get("/section/send_message","SectionController@send_message");
Route::get("/section/{id}","SectionController@show");


Route::get('/products/{type}/{id}',"ProductsController@show");
Route::get('/products/filter/{type}/{id}',"ProductsController@filter");
Route::get('/products/search',"ProductsController@search");
Route::get('/products/stock',"ProductsController@stock");

Route::get("/product-card/{id}","ProductCardController@show");
Route::post("/product-card/comment/{id}","ProductCardController@comment")->middleware("auth");

Route::get("/basket","BasketController@show");
Route::post("/basket/pay-result","BasketController@payResult");
Route::get("/basket/pay-result", function() {
    return redirect('/');
});
Route::get("/addToCart","BasketController@add")->middleware("auth");
Route::post("/order","BasketController@order")->middleware("auth");
Route::get('/order', 'BasketController@orderForm')->middleware("auth");
Route::post('/process-order', 'BasketController@processOrder')->name('process-order')->middleware("auth");
Route::get("/likes","LikesController@show");
Route::get("/add_like","LikesController@add")->middleware("auth");
Route::get("/del_like","LikesController@del")->middleware("auth");

Route::post('/excel/synk', 'ProductsController@synk')->middleware("auth");
Route::post('/excel/synk/delete', 'ProductsController@deleteNotUsedProducts')->middleware("auth");
Route::get("/comparisons","ComparisonsController@show");
Route::get("/add_comparison","ComparisonsController@add")->middleware("auth");
Route::get("/del_comparison","ComparisonsController@del")->middleware("auth");
// ЛИЧНЫЙ КАБИНЕТ
Route::get("/home","ProfileController@show")->name('profile')->middleware("auth");
Route::get("/home/reviews","ProfileController@review")->middleware("auth");
Route::get("/home/contact","ProfileController@contact")->middleware("auth");
Route::get("/home/purchase","ProfileController@purchase")->middleware("auth");
Route::middleware(['auth'])
    ->prefix('home')
    ->group(function() {
        Route::get('recomended', 'ProfileController@recomended')
            ->name('user.recomended');
    });
Route::get("/home/bonuses","ProfileController@bonuses")->middleware("auth");


Route::get("/home/question","ProfileController@question");
Route::get("/home/logout","ProfileController@logout");
Route::get("/home/account","ProfileController@account");
Route::post("/home/update","ProfileController@update");
Route::post("/home/subscription","ProfileController@subscription");
Route::post("/home/call","ProfileController@call");


// Route::get("/home",function(){
// 	return view("home");
// });

Route::post("/test","TestController@show");

Auth::routes();

Route::post('register', 'Auth\RegisterController@register');
Route::post('/enter', 'Auth\LoginController@enter');
// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/import', function() {

//$product = Product::create(['name' => 'asdf']);

ini_set('max_execution_time', 0);
	
	
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"http://proxy.paloma365.com/login_ajax.php");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch , CURLOPT_COOKIEJAR, __DIR__.'/cookies.txt');
curl_setopt($ch , CURLOPT_COOKIEFILE, __DIR__.'/cookies.txt');
curl_setopt($ch, CURLOPT_POSTFIELDS, "login=organic2018&password=organic2018");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);

curl_setopt_array(
    $ch, array(
    CURLOPT_URL => 'http://proxy.paloma365.com/company/ajax.php?direct_output=yes&class=guide2xml&method=to_file',
    CURLOPT_POST => 1,
    CURLOPT_POSTFIELDS => "tables[]=s_items&groups[s_items]=0&groups[s_clients]=0&groups[s_type_inout]=0&output_format=json"
));

$output = curl_exec($ch);

curl_close ($ch);

$products = json_decode($output, true)['s_items'];

$products_name = collect($products)->pluck('name');

$_sub_categories = Subcategory::all();

foreach($_sub_categories as $_sub_category) {
    if(!$products_name->contains($_sub_category->name)) {
        $_sub_category->delete();
    }
}

$_products = Product::all();

foreach($_products as $_product) {
    if(!$products_name->contains($_product->name)) {
        $_product->delete();
    }
}
	
foreach ($products as $key => $product) {
    if($product['isgroup'] == 1) {
        $cat = Subcategory::where('name', $product['name'])->first();
        if(!$cat) {
            $cat = Subcategory::create(['name' => $product['name'], 'cat_type' => 7, 'paloma_temp' => substr($product['UID'], 1, -1)]);
        }
    }
}

foreach ($products as $key => $product) {
    if($product['isgroup'] == 0) {
        $prod = Product::where('name', $product['name'])->first();
        if(!$prod) {
            $cat = Subcategory::where('paloma_temp', substr($product['parentid'], 1, -1))->first();
            if(!$cat) continue;
            $prod = Product::create([
                'name' => $product['name'],
                'sub_type' => $cat->id,
                'description' => $product['description'],
                'f_price' => $product['price'],
                's_price' => $product['price'],
                'final_price' => $product['price'],
                'shtrih' => $product['mainShtrih'],
            ]);
        }
    }
}
	
});

// 
Route::post('/spend-bonuses', function(Request $request) {
    if($request->user()->balance->balance > 0) {
        session(['bonus_spent' => true]);
    //     $total = session('total_sum') - $request->user()->balance->balance;
    //     session(['total_sum' => $total]);
    //     $request
    //         ->user()
    //         ->balance
    //         ->fill([
    //             'balance' => 0
    //         ])->save();
        
        return response(['bonuses' => $request->user()->balance->balance], 200);
    // }
    }
    return response(['error' => ['balance' => 'Недостаточно средств на счете']], 422);
});

Route::post('/use-promocode', 'BasketController@usePromocode')->name('use-promocode');

Route::post('/order/one-click', 'ProductsController@oneClickOrder');

Route::get('/regions', 'RegionController@index');
Route::get('/cities', 'CityController@index');
Route::get('/categories/{id}', 'SubcategoryController@index');
Route::get('/sales/{id}', 'SaleController@index');

Route::get('/promo/{promo}', 'BasketController@promo');