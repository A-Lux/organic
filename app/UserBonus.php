<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserBonus extends Model
{
    protected $fillable = ['user_id', 'balance'];
}
