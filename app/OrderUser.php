<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderUser extends Model
{
    protected $table = 'order_user';

    protected $fillable = [
        'user_id',
        'order_id',
        'fields'
    ];
}
