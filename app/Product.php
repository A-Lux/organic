<?php

namespace App;
use Mail;
use App\Traits\ManagesRating;
use Illuminate\Support\Facades\DB;
use App\Traits\ShowsViewedProducts;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use ShowsViewedProducts, ManagesRating, SoftDeletes;
    
	protected $perPage = 50;
	
	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
		if(session()->has('show') && !is_null(session()->get('show'))){
			$this->perPage = intval(session()->get('show'));
		}
		else{
			$this->perPage = 50;
		}
	}
	
	public $fillable = [
		'name',
		'sub_type',
		'description',
		'f_price',
		's_price',
		'final_price',
		'shtrih',
        'count',
        'sale_id',	
		'stock',
		'shtrih'
	];
	
public function setCatTypeAttribute($value)
    {	
    	
    	if (!is_null($value)) {
    		$this->attributes["cat_type"]=$value;
    		$this->attributes["sub_type"] = NULL;
    	}
        
    }

public function setSPriceAttribute($value)
    {
        if (!empty($value)) {
        	$this->attributes['final_price']=$value;
        	$this->attributes['s_price']=$value;
        }
        else{
        	$this->attributes['final_price']=$this->attributes['f_price'];
        	$this->attributes['s_price']=$value;
        }
    }

    // public function setNameAttribute($value){

        // $this->attributes['name'] = $value;
        
        // $subscriptions=DB::select("SELECT email FROM subscriptions");
        // if (!empty($subscriptions)) {
        

        //      foreach ($subscriptions as $key => $value) {
        //         $emails[]=$value->email;
        //      }

        //      $id=DB::select('SELECT id FROM products ORDER BY id DESC LIMIT 1')[0]->id+1;

        //   Mail::send(['html'=>'mail'], ['id'=>$id,'name'=>$this->attributes['name']], function($message) use ($emails){

        //      $message->to($emails, 'Tutorials Point')->subject
        //         ('Новый продукт на ORGANIC');
        //      $message->from('dildabekov95@gmail.com','ORGANIC');

        //   });
        // }
    // }

    public function orders() {
        return $this->belongsToMany(\App\Order::class, 'about_orders');
    }

}
