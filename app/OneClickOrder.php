<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OneClickOrder extends Model
{
    protected $fillable = ['product_id', 'name', 'phone', 'message'];
}
