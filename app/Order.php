<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    public $fillable = ['user_id', 'kol_vo', 'sum', 'status', 'pay_success', 'name',
    'surname',
    'iin',
    'delivery_type',
    'payment_type',
    'address'];

    public function products() {
        return $this->belongsToMany(\App\Product::class, 'about_orders');
    }

    public function productsInOrder() {
        return $this->hasMany(\App\AboutOrder::class);
    }
}
