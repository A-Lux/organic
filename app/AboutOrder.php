<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AboutOrder extends Model
{
    public $fillable = ['order_id', 'user_id', 'product_id', 'kol_vo'];

    public function product() {
        return $this->belongsTo(\App\Product::class);
    }
}
