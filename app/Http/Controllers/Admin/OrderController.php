<?php

namespace App\Http\Controllers\Admin;

use App\Bonus;
use App\UserBonus;
use App\BonusesAmount;
use Illuminate\Http\Request;
use App\Mail\OrderStatusChanged;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use App\Http\Controllers\Admin\VoyagerBaseController;
use App\OrderUser;
use Illuminate\Support\Facades\Auth;

class OrderController extends VoyagerBaseController
{
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $status = $data->status;
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
        event(new BreadDataUpdated($dataType, $data));
        if ($status != $data->status) {
            Mail::to($request->user())->send(new OrderStatusChanged($data, config('app.status')[$data->status]));
        }
        if ($data->status == 2 && !Bonus::where('user_id', $data->user_id)->where('order_id', $data->id)->exists()) {
            if (BonusesAmount::where('amount', '<=', $data->sum)->exists()) {
                $bonus = BonusesAmount::where('amount', '<=', $data->sum)->first();
                $balance = UserBonus::where('user_id', $data->user_id)->first();
                if (!$balance) {
                    $balance = UserBonus::create([
                        'user_id' => $data->user_id,
                        'balance' => 0
                    ]);
                }
                $bonus_calculated = $data->sum * ($bonus->percentage / 100);
                $balance->fill([
                    'balance' => $balance->balance + $bonus_calculated
                ])->save();

                Bonus::create([
                    'user_id' => $data->user_id,
                    'order_id' => $data->id,
                    'bonus' => $bonus_calculated,
                    'enrol_date' => DB::raw('now()')
                ]);
            }
        }

        OrderUser::create([
            'user_id' => Auth::user()->id,
            'order_id' => $id,
            'fields' => json_encode($request->all())
        ]);
        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }
}
