<?php
namespace App\Http\Controllers;

use App\Sale;
use App\Product;
use App\OneClickOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
	
class ProductsController extends Controller
{
    public function show($type, $id, Request $request)
    {
        $data = $this->layout();
        $data['filter_prices'] = DB::select('SELECT min,max FROM prices LIMIT 1');

        $filter["type"] = $type;
        $filter["id"] = $id;

        $productsNew = DB::table('products')
            ->select('id', 'sale', 'cover', 'text_cover', 'name', 'volume', 'description', 'discount', 'f_price', 's_price', 'count');
        if ($request->brands != 0) {
            // $brands_id=implode(',',$request->brands);
            // $brands_id=" AND brand IN (".$brands_id.")";
            $productsNew = $productsNew->whereIn('brand', $request->brands);
        }
        // else{
        //     $brands_id="";
        // }

        if (isset($request->column) && isset($request->sort_type)) {
            //$order=" ORDER BY ".$request->column." ".$request->sort_type;
            if($request->column == "in_stock"){
                if($request->sort_type == "ASC"){
                    $productsNew = $productsNew->where('count', '>', 0);
                }
            }elseif ($request->column == "discount"){
                if($request->sort_type == "ASC") {
                    $productsNew = $productsNew->where('s_price', '>', 0);
                }
            }else{
                $productsNew = $productsNew->orderBy($request->column, $request->sort_type);
            }

        }
        // else{
        //     $order="";
        // }

        if (isset($request->amount_1) and !empty($request->amount_1)) {
            $productsNew = $productsNew->where('final_price', '>=', $request->amount_1);
            //$price=" AND final_price BETWEEN ".$request->amount_1." AND ".$request->amount_2;
        }
        // else{
        //     $price="";
        // }

        if (isset($request->amount_2) and !empty($request->amount_2)) {
            $productsNew = $productsNew->where('final_price', '=<', $request->amount_2);
            //$price=" AND final_price BETWEEN ".$request->amount_1." AND ".$request->amount_2;
        }

        if ($type == "subcategory") {
            $productsNew = $productsNew->where('sub_type', $id);
            $main_data = DB::select("SELECT name,description,sorting FROM subcategories WHERE id=" . $id)[0];
            //$products=DB::select("SELECT id,sale,cover,text_cover,name,volume,description,discount,f_price,s_price FROM products WHERE sub_type=".$id.$brands_id.$price.$order);
        } else {
            $productsNew = $productsNew->where('cat_type', $id);
            $main_data = DB::select("SELECT name,description FROM ocategories WHERE id=" . $id)[0];
            // $products=DB::select("SELECT id,sale,cover,text_cover,name,volume,description,discount,f_price,s_price FROM products WHERE cat_type=".$id.$brands_id.$price.$order);
        }
		$productsNew = $productsNew->whereNull('deleted_at')->where('can_show', 1);
        if ($request->session()->get('itemsAmount')) {
            $productsNew = $productsNew->paginate($request->session()->get('itemsAmount'));
        } else {
            $productsNew = $productsNew->paginate(50);
        }

        $user_id = Auth::id();
        if (!empty($user_id)) {
            $data["views"] = DB::select("SELECT products.id,sale,cover,text_cover,name,volume,description,discount,f_price,s_price FROM products INNER JOIN views ON products.id=views.product_id WHERE views.user_id=" . $user_id . " ORDER BY views.id DESC");
        }


        $brands = DB::select("SELECT id,name,sorting FROM brands");
        usort($brands, function ($a, $b) {
            return strcmp($a->sorting, $b->sorting);
        });
        $data["main_data"] = $main_data;
        $data["products"] = $productsNew;
        $data["brands"] = $brands;
        $data["filter"] = $filter;
        $data["action"] = "/products/" . $type . "/" . $id;
		$data["url"] = ($_SERVER['REQUEST_URI']);
        if (!$request->ajax()) {
            return view("catalog", $data);
        } else {
            return view("catalog_ajax", $data);
        }
    }

    public function filter($type, $id)
    {
        if ($type == "subcategory") {
            $column_type = "sub_type";
        } else if ($type == "categories") {
            $column_type = "cat_type";
        }
        $products = DB::select("SELECT * FROM products WHERE " . $column_type . " = " . $id . " AND deleted_at is null AND count>0 ORDER BY " . $_GET["order"] . " " . $_GET["filter_type"]);
        $html = "";

        foreach ($products as $product) {

            if (!empty($product->s_price)) {
                $s_price = $product->s_price;
            } else {
                $s_price = " ";
            }

            if (!empty($product->discount)) {
                $discount = '<span class="catalog-stock">-' . $product->discount . '</span>';
            } else {
                $discount = " ";
            }
            $html .= '
            <div class="catalog-product-item">
                <div class="catalog-img">
                  <img src="/storage/' . $product->cover . '">
                  ' . $discount . '
                </div>
                <h4><a href="/product-card/' . $product->id . '">' . $product->name . '</a> / ' . $product->volume . ' мл.</h4>
                <p>' . $product->text_cover . '</p>
                <div class="price">' . $product->f_price . ' тнг
                  <span>' . $s_price . 'тнг </span>
              </div>
                <ul class="stars">
                    <li class="active"></li>
                    <li class="active"></li>
                    <li class="active"></li>
                    <li class="active"></li>
                    <li></li>
                </ul>
                <button type="button" name="basket-btn">
                  В КОРЗИНУ
                </button>
              </div>';
        }

        echo $html;

        // print_r($products);
    }

    public function search(Request $request)
    {
        $data = $this->layout();

        $data["products"] = DB::select("SELECT id,sale,cover,text_cover,name,volume,description,discount,f_price,s_price FROM products WHERE  deleted_at is null AND name LIKE '%" . $request->search . "%'");
        $data["search"] = $request->search;
        return view("search", $data);
    }

    public function stock()
    {
        $data = $this->layout();

        $products = DB::select("SELECT id,sale,cover,text_cover,name,volume,description,discount,f_price,s_price FROM products WHERE deleted_at is null and stock=1");
        $data["products"] = $products;
        $brands = DB::select("SELECT id,name FROM brands");
        $data["brands"] = $brands;
        $data["stock"] = "Акции";
        $data['sales'] = Sale::select(['sales.id', 'sales.name'])->join('products', 'products.sale_id', 'sales.id')->get();
        return view("catalog_stock", $data);
    }

    public function oneClickOrder(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|integer',
            'name' => 'required|string',
            'phone' => 'required|string',
            'message' => 'required|string'
        ]);

        $order = OneClickOrder::create([
            'product_id' => $request->product_id,
            'name' => $request->name,
            'phone' => $request->phone,
            'message' => $request->message
        ]);

        return response(['order_id' => $order->id], 200);
    }
	
	public function setAmount(Request $request, $amount){
		$request->session()->put('itemsAmount', $amount);
		return response('', 200);
	}
	
	public function synk(Request $request){
		//$request->validate([
		//	'file' => 'required|file'
		//]);
		$file = Storage::put('/', $request->file);
		$DOM = new \DOMDocument;
		$config = array(
			'indent'         => true,
			'output-xhtml'   => true,
			'wrap'           => 200);
		//print_r('<pre>');
		$tidy = new \tidy;
		$tidy->parseString(Storage::get($file), $config, 'utf8');
		$tidy->cleanRepair();

    	$DOM->loadHTML(mb_convert_encoding($tidy, 'HTML-ENTITIES', 'UTF-8'));
		
		$theads = $DOM->getElementsByTagName('thead');
		$names = [];
		foreach ( $theads as $thead )  {
			$tr = $thead->getElementsByTagName('tr');
			foreach ( $tr[0]->getElementsByTagName('th') as $th )  {
				$names[] = trim($th->textContent);
			}
		}
		$tbody = $DOM->getElementsByTagName('tbody');
		$items = $tbody->item(0)->getElementsByTagName('tr');
		$rows = [];
		foreach ( $items as $tr )  {
			if(!$tr->getElementsByTagName("td")->item(1)){
				continue;
			}
			if($tr->getElementsByTagName("td")->item(1)->hasAttribute('colspan')){
				continue;
			}
			$row = [];
			foreach ( $tr->getElementsByTagName("td") as $td )  {
				$row[] = trim($td->textContent);
			}
			$rows[] = $row;
		}
		$products = [];
		foreach ($rows as $row)  {
			$row[7] = str_replace(' ', '', $row[7]);
			$product = Product::where('shtrih', 'like', '%'.$row[1].'%')->first();
			if($product){
				$product->update([
					'name' => $row[2],
					'f_price' => empty($row[7]) ? 0 : (int)$row[7],
					'count' => empty($row[4]) ? 0 : (int)$row[4]
				]);
			}
			else{
				$product = Product::create([
					'name' => $row[2],
					'f_price' => empty($row[7]) ? 0 : (int)$row[7],
					'shtrih' => $row[1],
					'count' => empty($row[4]) ? 0 : (int)$row[4]
				]);
			}
		}
		return redirect('admin/products');
	}
	
	public function deleteNotUsedProducts(Request $request){
		$file = Storage::put('/', $request->file);
		$DOM = new \DOMDocument;
		$config = array(
			'indent'         => true,
			'output-xhtml'   => true,
			'wrap'           => 200);
		//print_r('<pre>');
		$tidy = new \tidy;
		$tidy->parseString(Storage::get($file), $config, 'utf8');
		$tidy->cleanRepair();

    	$DOM->loadHTML(mb_convert_encoding($tidy, 'HTML-ENTITIES', 'UTF-8'));
		
		$theads = $DOM->getElementsByTagName('thead');
		$names = [];
		foreach ( $theads as $thead )  {
			$tr = $thead->getElementsByTagName('tr');
			foreach ( $tr[0]->getElementsByTagName('th') as $th )  {
				$names[] = trim($th->textContent);
			}
		}
		$tbody = $DOM->getElementsByTagName('tbody');
		$items = $tbody->item(0)->getElementsByTagName('tr');
		$rows = [];
		foreach ( $items as $tr )  {
			if(!$tr->getElementsByTagName("td")->item(1)){
				continue;
			}
			if($tr->getElementsByTagName("td")->item(1)->hasAttribute('colspan')){
				continue;
			}
			$row = [];
			foreach ( $tr->getElementsByTagName("td") as $td )  {
				$row[] = trim($td->textContent);
			}
			$rows[] = $row;
		}
		$products = [];
		foreach ($rows as $row)  {
			$row[7] = str_replace(' ', '', $row[7]);
			$product = Product::where('shtrih', 'like', '%'.$row[1].'%')->first();
			if($product){
				$product->update([
					'name' => $row[2],
					'f_price' => empty($row[7]) ? 0 : (int)$row[7],
					'count' => empty($row[4]) ? 0 : (int)$row[4]
				]);
			}
			else{
				$product = Product::create([
					'name' => $row[2],
					'f_price' => empty($row[7]) ? 0 : (int)$row[7],
					'shtrih' => $row[1],
					'count' => empty($row[4]) ? 0 : (int)$row[4]
				]);
			}
			$products[] = $product;
		}
		foreach(Product::get()->diff(collect($products)) as $product){
			$product->delete();
		}
	}
}
