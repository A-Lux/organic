<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    public function show_all()
    {
    	$news=DB::select("SELECT * FROM news ORDER BY id DESC");
    	$data=$this->layout();
    	$data["news"]=$news;

    	return view("news-group",$data);
    }

    public function show_card($id,Request $request){
    	$data=$this->layout();

        if (isset($request->type)) {

            if ($request->type=='next') {
            	$news=DB::select("SELECT * FROM news WHERE id<".$id." ORDER BY id DESC LIMIT 1");  
                }
            else{
               $news=DB::select("SELECT * FROM news WHERE id>".$id." ORDER BY id ASC LIMIT 1"); 
            }  

    		if (!empty($news)) {  
    				
    				if ($request->type=='next') {

    					$min=DB::select('SELECT min(id) as id FROM news')[0];

    					if ($news[0]->id==$min->id) {
    						session(['hide'=>'next']);
    						}
    						else{
    							session()->forget('hide');
    						}

    				}
    				else{
    					$max=DB::select('SELECT max(id) as id FROM news')[0];

    						if ($news[0]->id==$max->id) {
    						session(['hide'=>'prev']);
    						}
    						else{
    							session()->forget('hide');
    						}
    				}

    			}	

    }
    else{

    	$news=DB::select("SELECT * FROM news WHERE id=".$id);

        $max_min=DB::select('SELECT IF(max(id)='.$id.',true,false) as max,IF(min(id)='.$id.',true,false) as min FROM news')[0];

        if ($max_min->max) {
        session(['hide'=>'prev']);
        }
        else if ($max_min->min) {
        session(['hide'=>'next']);
        }
        else{
            session()->forget('hide');
        }

    }

    $data["news"]=$news[0];	


    return view("news",$data);
}
}