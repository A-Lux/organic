<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SectionController extends Controller
{
    public function show($id){
    	$data=$this->layout();

    	$section=DB::select("SELECT name,body FROM sections WHERE id=".$id)[0];

    	if ($section->name=='Доставка и оплата') {
    	return view("dostavka",$data);
    	}
    	else{
    	$data["section"]=$section;
    	return view("section",$data);	
    	}
    	

    	

    }

    public function contact(){
    	$data=$this->layout();
		$contact = Contact::first();
    	$data["contact"]=$contact;
    	return view("contact",$data);
    }

    public function send_message(Request $request){

    	DB::table('letters')->insert([
    		'name'=>$request->name,
    		'email'=>$request->email,
    		'message'=>$request->message,
    		'created_at'=>NOW(),
    	]);

    	return back()->with('send_message',true);
    }
}
