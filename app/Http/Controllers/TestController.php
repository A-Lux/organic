<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
class TestController extends Controller
{
    public function show(Request $request){
	   $epay = new \KkbEpay\Epay([
		'MERCHANT_CERTIFICATE_ID' => '12345678',
		'MERCHANT_NAME' => 'Test Shop',
		'PRIVATE_KEY_FN' => './epay/cert.prv',
		'PRIVATE_KEY_PASS' => '1q2w3e4r',
		'PUBLIC_KEY_FN' => './epay/cert.cer',
		'BANK_PUBLIC_KEY_FN' => './epay/kkbca.pub',
		'MERCHANT_ID' => '12345678',
	]);
	dd($epay);
	// Основной запрос оплаты (Signed_Order_B64)
	$request = $epay->buildRequest('payment', [
		'order_id' => 1234,
		'amount' => 1000,
		'currency' => 398,
		'fields' => [
			'RL' => 1234567,
			'abonent_id' => 1234567,
		],
		'links' => [
			'BackLink' => '...',
			'FailureBackLink' => '...',
			'PostLink' => '...',
			'FailurePostLink' => '...',
		]
	]);
	
	// Статус платежа
	$request = $epay->buildRequest('status', ['order_id' => 1234]);
	//Обработка ответов Epay
	
	// PostLink
	$response = $epay->parseResponse('payment', $_POST['response']);
	
	// Ответ на запрос о статусе платежа
	$curl = curl_init('https://testpay.kkb.kz/jsp/remote/checkOrdern.jsp?' . urlencode($request));
	
	curl_setopt_array($curl, array(
		CURLOPT_RETURNTRANSFER => 1,
	));
	
	$result = curl_exec($curl);
	curl_close($curl);
	
	$response = $epay->parseResponse('status', $result);
	
	//verify() проверяет подпись ответа.
	if($response->verify()) {
		print_r($response->get());
		// get возвращает массив с параметрами ответа
	} else {
		echo 'Response not valid';
	}
	dd($curl);
    	
//
//    	// echo file_get_contents(asset('cert.prv'));
//    define("MERCHANT_CERT_ID",	"c183d786");	// Ñåðèéíûé íîìåð ñåðòèôèêàòà
//	define("MERCHANT_NAME",		"ORGANIC MARKET");	// Íàçâàíèå ìàãàçèíà (ïðîäàâöà)
//	define("ORDER_ID",			"450001");		// Óíèêàëüíûé íîìåð çàêàçà
//	define("CURRENCY",			"398");			// ID âàëþòû. 840 - USD
//	define("MERCHANT_ID",		"92395631");			// ID ïðîäàâöà â ñèñòåìå
//	define("AMOUNT", 			"50");			// ñóììà çàêàçà
//
//
//	// -----------------------------------------------------------------------------------------------------------------
//	// Include ìîäóëÿ
//
//
//	// -----------------------------------------------------------------------------------------------------------------
//	// Ñîçäàíèå îáúåêòà
//
//	// -----------------------------------------------------------------------------------------------------------------
//	// Ïåðåâîðà÷èâàåì êëþ÷
//
//	$this->invert();
//
//	// -----------------------------------------------------------------------------------------------------------------
//	// Îòêðûâàåì ïóáëè÷íûé êëþ÷.  "êëþ÷", "ïàðîëü"
//
//	$this->load_private_key('cert.prv', "1q2w3e4r");
//
//
//	// -----------------------------------------------------------------------------------------------------------------
//	// Øàáëîí çàêàçà
//
//	$merchant = '<merchant cert_id="%certificate%" name="%merchant_name%"><order order_id="%order_id%" amount="%amount%" currency="%currency%"><department merchant_id="%merchant_id%" amount="%amount%"/></order></merchant>';
//
//	// -----------------------------------------------------------------------------------------------------------------
//	// Ïîäãîòîâêà äàííûõ çàêàçà, îáðàáîòêà øàáëîíà
//
//	$merchant = preg_replace('/\%certificate\%/', 		MERCHANT_CERT_ID , 	$merchant);
//	$merchant = preg_replace('/\%merchant_name\%/', 	MERCHANT_NAME , 	$merchant);
//	$merchant = preg_replace('/\%order_id\%/', 			ORDER_ID, 			$merchant);
//	$merchant = preg_replace('/\%currency\%/', 			CURRENCY, 			$merchant);
//	$merchant = preg_replace('/\%merchant_id\%/', 		MERCHANT_ID, 		$merchant);
//	$merchant = preg_replace('/\%amount\%/', 			AMOUNT,				$merchant);
//
//	// -----------------------------------------------------------------------------------------------------------------
//	// Ïîäãîòàâëèâàåì ïîäïèñü
//
//	$merchant_sign = '<merchant_sign type="RSA">'.$this->sign64($merchant).'</merchant_sign>';
//
//
//	// -----------------------------------------------------------------------------------------------------------------
//	// Ñîñòàâëÿåì ïàêåò
//
//	$xml = "<document>".$merchant.$merchant_sign."</document>";
//
//
//	// -----------------------------------------------------------------------------------------------------------------
//	// Âûâîäèì ïàêåò
//
//	echo $xml;

    }
}
