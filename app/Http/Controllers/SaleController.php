<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function index(Request $request, $id) {
        $products = Product::where('sale_id', $id)->get();

        return response($products, 200);
    }
}
