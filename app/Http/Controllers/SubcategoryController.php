<?php

namespace App\Http\Controllers;

use App\Product;
use App\Subcategory;
use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
    public function index(Request $request, $id) {
        $orders = $request->user()->orders->load('products');
        $products = Product::where('sub_type', $id)->limit(5)->get();
        
        return response($products, 200);
    }
}
