<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    public function index(Request $request) {
        $this->validate($request, [
            'country' => 'required'
        ]);

        $regions = Region::select(['id', 'name'])->where('country_id', $request->country)->get();

        return response($regions, 200);
    }
}
