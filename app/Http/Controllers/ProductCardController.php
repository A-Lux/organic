<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Product;
use App\Ocategory as Category;
use App\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProductCardController extends Controller
{
    public function show($id){
        
        Product::addToViewed($id);
    	$data=$this->layout();

    	$user_id=Auth::id();
        $product_id=$id;
        
        // $product = Product::calculateRating($id);
        // dd($ratingCalculated);
        // $comments = Comment::select(['rating'])
        //     ->where('published', 1)
        //     ->whereNotNull('rating')
        //     ->where('product_id', $id)
        //     ->get();
        // $ratings = $comments->pluck('rating');
        // $ratingCalculated = 0;

        // foreach($ratings as $rating) {
        //     $ratingCalculated += $rating;
        // }

        // $ratingCalculated != 0 ? $ratingCalculated /= $ratings->count() : null;

    	//ДОБАВЛЕНИЕ ПРОСМОТРА В ТАБЛИЦУ ДЕТАЛЬНЫХ ПРОСМОТРОВ АВТОРИЗОВАНАННОГО ПОЛЬЗОВАТЕЛЯ
    	if (!empty(Auth::id())) {
    	DB::insert("INSERT INTO views (user_id,product_id,created_at) VALUES (".$user_id.",".$product_id.",NOW())");
    	}
    	

    	//ОБНОВЛЕНИЕ КОЛ-ВО  СВОДНЫХ ПРОСМОТРОВ ВЫБРАННОГО ТОВАРА
    	DB::update("UPDATE products SET seen=seen+1 WHERE id=".$id);

    	$product=DB::select("SELECT products.id,sub_type,cat_type,products.name,description,images,f_price,s_price,volume,count,composition,mode,brands.name as brand,icon,products.shtrih,country,flag
        FROM products LEFT JOIN brands ON products.brand=brands.id WHERE products.id=".$id)[0];
        // dd($product->cat_type);
        if(Subcategory::find($product->sub_type)){
            $product->subcategory = Subcategory::find($product->sub_type)->name;
            if(Category::find(Subcategory::find($product->sub_type))){
                $product->category = Category::find(Subcategory::find($product->sub_type)->cat_type);
            }
            else{
                $product->category = "";
            }
        }
        else{
            $product->subcategory = "";
            $product->category = new \stdClass();
            $product->category->id = 1;
            $product->category->name = "";
        }

        

        $data['rating'] = Product::calculateRating($id);
        $data["product"]=$product;

		$data['desc1']='';
		$data['desc2']='';
		
        if (!empty($product->description) && ($product->description != 'NULL')) {
            $description = wordwrap($product->description,strlen($product->description)/2, "<br />");
            $description = explode('<br />',$description);
            $desc1=$description[0];
            $desc2=$description[1];

            $data['desc1']=$desc1;
            $data['desc2']=$desc2;
        }

        $data["comments"]=DB::select("SELECT users.name,users.avatar,comments.text,comments.created_at FROM comments INNER JOIN users ON users.id=comments.user_id WHERE published=1 AND product_id=".$id);


        if (!empty($product->sub_type) AND !empty($product->cat_type)) {
           $where="WHERE sub_type=".$product->sub_type." AND cat_type=".$product->cat_type;
        }  
        else if (!empty($product->sub_type)) {
           $where="WHERE sub_type=".$product->sub_type;
        }
        else if (!empty($product->cat_type)) {
            $where="WHERE cat_type=".$product->cat_type;
        }
        else{
            $where="";
        }

		
        $data["similar_products"]=DB::select("SELECT id,cover,name FROM products 
           ".$where." ORDER BY seen DESC LIMIT 3");
		//dd($data["similar_products"]);
    	return view("product-card",$data);
    }

    public function comment(Request $request, $id){
        $this->validate($request, [
            'rating' => 'required|integer|min:1|max:5',
            'text' => 'required'
        ]);

    	$user_id = Auth::id();
    	DB::insert("INSERT INTO comments (user_id,product_id,created_at,text, rating) VALUES (".$user_id.",".$id.",NOW(),'".$_POST["text"]."', '".$request->rating."')");
    	return redirect('/product-card/'.$id)->with('send',true);
    
    }
}
