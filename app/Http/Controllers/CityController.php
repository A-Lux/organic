<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index(Request $request) {
        $this->validate($request, [
            'region' => 'required'
        ]);

        $cities = City::select(['id', 'name'])->where('region_id', $request->region)->get();

        return response($cities, 200);
    }
}
