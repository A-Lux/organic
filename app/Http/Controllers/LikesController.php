<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LikesController extends Controller
{   

public function add(){

  $user_id=Auth::id();
  $product_id=$_GET["id"];

  $select=DB::select("SELECT id FROM likes WHERE user_id=".$user_id." AND product_id=".$product_id);
  if (empty($select)) {
    
    if (session()->has('likes')) {
      session(['likes'=>session('likes')+1]);
    }
    else{
      session(['likes'=>1]);
    }
    
    DB::insert("INSERT INTO likes (user_id,product_id,created_at) VALUES (".$user_id.",".$product_id.",NOW())");
    echo session('likes');
  }
  else{
    echo 0;
  }

  
  
}

public function show(){

  if (Auth::check()) {
  $data=$this->layout();
  $user_id=Auth::id();

  $data["products"]=DB::select("SELECT products.id,sale,cover,text_cover,name,volume,description,discount,f_price,s_price FROM products 
          INNER JOIN likes ON likes.product_id=products.id WHERE  likes.user_id=".$user_id);

  return view('likes',$data);
  }
  else{
    return back()->with('not_autorized',true);
  }
  

}

public function del(Request $request){

  $user_id=Auth::id();

  $delete=DB::delete("DELETE FROM likes WHERE product_id=".$request->id." AND user_id=".$user_id);
  session(['likes'=>session('likes')-1]);
  return redirect('/likes')->with('deleted',true);

}

}
