<?php

namespace App\Http\Controllers;

use App\Bonus;
use App\Order;
use App\Promocode;
use App\UserBonus;
use App\AboutOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Product;

require_once(app_path() . "/../qazkom_pay/kkb.utils.php");

class BasketController extends Controller
{   

    public function show(Request $request){
        $data=$this->layout();
        if (Auth::check()) {
          $balance = $request  
            ->user()
            ->balance
            ->balance;
          $data['balance'] = $balance;
       }
        $products=[];
        // $total_sum = 0;
        // if (session()->has('basket')) {
        // 	foreach (session('basket') as $key => $value) {
        //   	$product=DB::select("SELECT * FROM products WHERE id=".$key)[0];
            
		          
          // $products[]=["id"=>$product->id,"name"=>$product->name,"count"=>$value,"cover"=>$product->cover,"price"=>$value*$one_product_price,"one_product_price"=>$one_product_price];
            
        //     $total_sum+=$value*$one_product_price;
        // 	}

        // }
        // // dd($total_sum);
        // // session()->forget('total_sum');
        // // return redirect('/');
        // if(session()->has('total_sum')) {
        //     $total_sum = session('total_sum');
        // }elseif($total_sum != 0){
        //   session(['total_sum' => $total_sum]);   
        // }

        $only_foo = array();
        $amounts = array();
        foreach ($_COOKIE as $key => $value) {
            if (strpos($key, 'product') === 0) {
        $only_foo[$key] = substr($key, strpos($key, "t") + 1);
        $amounts[substr($key, strpos($key, "t") + 1)] = $value;
            }
        }
        $only_foo = array_values($only_foo);
        $products = Product::find($only_foo);
        foreach($products as $product){
          if($product->price == ""){
            $product->price = 0;        
          }
          $product->amount = $amounts[$product->id];
          if (!empty($product->s_price)) {
            $product->one_product_price=$product->s_price;
          }
          else{
            $product->one_product_price=$product->f_price; 
          }
        }

        $data["products"]=$products;
        // $data["total_sum"]=$total_sum;
        

        return view("basket",$data);
      }

    public function add(Request $request){

      if ($request->action!="del") {
       
      //Проверяем есть ли данный товар в корзине
      //Увеличиваем кол-во данного товара 
      if (session()->has('basket.'.$request->id)) {
      $product_count=session('basket.'.$request->id);


            if (isset($request->card_count)) {
            session(['basket.'.$request->id=>$product_count+$request->card_count]); 
            }
            else if (isset($request->count)) {
            session(['basket.'.$request->id=>$request->count]); 
            }
            else{
            session(['basket.'.$request->id=>$product_count+1]);
            }
      
      }
      //Добавляем данный товар в корзине
      else{

            if (isset($request->card_count)) {
            session(['basket.'.$request->id=>$request->card_count]); 
            }
            else{
             session(['basket.'.$request->id=>1]);  
            }
       
      }

      }
      //Удаляем с корзины
      else{
        session()->forget('basket.'.$request->id);
      }

      //Общее кол-во товаров
      $basket_total=0;
      if (!empty(session('basket'))) {
        foreach (session('basket') as $key => $value) {
        $basket_total=$basket_total+$value;
        session(['basket_total'=>$basket_total]);
      }
      }
      else{
        session(['basket_total'=>$basket_total]);
      }

      $total_sum = 0;
        if (session()->has('basket')) {
        	foreach (session('basket') as $key => $value) {
          	$product=DB::select("SELECT * FROM products WHERE id=".$key)[0];
            
		          if (!empty($product->s_price)) {
		          	$one_product_price=$product->s_price;
		          }
		          else{
		          	$one_product_price=$product->f_price;	
		          }

          $products[]=["id"=>$product->id,"name"=>$product->name,"count"=>$value,"cover"=>$product->cover,"price"=>$value*$one_product_price,"one_product_price"=>$one_product_price];
            
            $total_sum+=$value*$one_product_price;
        	}

        }
        
        session(['total_sum' => $total_sum]);
        echo $basket_total;
    }

    public function order(Request $request) {
		$user = Auth::id();
		$count_all = 0;
		$price_all = session('total_sum');
		
		$order = Order::create([
			'user_id' => $user,
			'kol_vo' => 0,
      'status' => 0,
			'sum' => 0,
			'pay_success' => 0,
		]);

		foreach($request->product as $id => $arr) {
			$count_all += (int)$arr['count'];
			
			$order_product = AboutOrder::create([
				'order_id' => $order->id,
				'user_id' => $user,
				'product_id' => $id,
				'kol_vo' => $arr['count']
			]);
		}
		
		$order->kol_vo = $count_all;
		$order->sum = $price_all;
		$order->save();
		
    if ($price_all>=10000 AND $price_all<20000) {
      $order = Order::create([
      'user_id' => $user,
      'status' => 0,
      'created_at' => NOW(),
      'order_id' => $order->id,
      'bonus'=>round($price_all*100/3) 
    ]);
    }

    $data=$this->layout();
    $data['order'] = $order;

		$path1			 = app_path() . "/../qazkom_pay/config.txt"; // Путь к файлу настроек config.dat
		$order_id		 = $order->id; // Порядковый номер заказа - преобразуется в формат "000001"
		$currency_id	 = "398"; // Шифр валюты  - 840-USD, 398-Tenge 643
		$amount = $price_all;
		$content		 = process_request($order_id, $currency_id, $amount, $path1);
		$data['pay_content'] = $content;
		
		
		return view('pay', $data);
    }
	
	public function payResult(Request $request) {
		
		$path1 = app_path() . "/../qazkom_pay/config.txt"; // Путь к файлу настроек config.dat
        $response = $request->response;
        $result = process_response(stripslashes($response), $path1);
		$error = 0;
		file_put_contents(app_path() . '/../qazkom_pay/' . $result['ORDER_ORDER_ID'] . '.json', json_encode($result));

        if ($result['CHECKRESULT'] == '[SIGN_GOOD]') {
            try {
                $content = process_complete($result['PAYMENT_REFERENCE'], $result['PAYMENT_APPROVAL_CODE'], $result['ORDER_ORDER_ID'], $result['ORDER_CURRENCY'], $result['ORDER_AMOUNT'], $path1);
                $answer	 = file_get_contents('https://epay.kkb.kz/jsp/remote/control.jsp?' . urlencode($content));
                $res	 = process_response($answer, $path1);
				file_put_contents(app_path() . '/../qazkom_pay/' . $result['ORDER_ORDER_ID'] . '_complete.json', json_encode($res));
                if ($res['CHECKRESULT'] == '[SIGN_GOOD]') {
                    if ($res['RESPONSE_CODE'] == '00') {
                        if ($res['RESPONSE_MESSAGE'] == 'Approved') {
							// УСПЕШНЫЙ ПЛАТЕЖ
							$order = Order::where('id', $result['ORDER_ORDER_ID'])->first();
							$order->pay_success = 1;
							$order->save();
                        } else {
                            $error = 1;
                        }
                    } else {
						$error = 1;
                    }
                } else {
					$error = 1;
                }
            } catch (Exception $e) {
				$error = 1;
            }
		}
		
		if($error) {
			// ОШИБКА ПЛАТЕЖА
			$order = Order::where('id', $result['ORDER_ORDER_ID'])->first();
			$order->pay_success = 2;
			$order->save();
		}
  }
  
  public function usePromocode(Request $request) {
    $this->validate($request, [
      'promocode' => 'required'
    ]);
    if(Promocode::exists($request->promocode)) {
      if(!Promocode::expired($request->promocode)) {
        $promocode = Promocode::getPromocode($request->promocode);
        $totalPrice = round(session('total_sum') - session('total_sum') * ($promocode->percentage / 100));
        session(['total_sum' => $totalPrice]);
        session(['used_promocode' => true]);
        return response(['totalSum' => $totalPrice], 200);
      }
      return response(['errors' => ['promocode' => 'Срок действия промокода истек']], 422);
    }
    return response(['errors' => ['promocode' => 'Неверный промокод']], 422);
  }

  public function orderForm(Request $request) {

    if(count(preg_grep('/product./', array_keys([$_COOKIE]))) > 0) {
  		return redirect('/');
	   }
    $products = json_encode($request->product);
    $data = $this->layout();
    $data['products'] = $products;
    $data['total_sum'] = $request->total_sum;
    session(['total_sum' => $request->total_sum]);
    return view('orders.index', $data);
  }

  public function processOrder(Request $request) {
    $this->validate($request, [
      "name" => "required",
      "surname" => "required",
      "iin" => "required",
      "delivery" => "required",
      "payment" => "required",
      'products' => 'required'
    ]);

    $only_foo = array();
    $amounts = array();
    foreach ($_COOKIE as $key => $value) {
        if (strpos($key, 'product') === 0) {
          $only_foo[$key] = substr($key, strpos($key, "t") + 1);
          $amounts[substr($key, strpos($key, "t") + 1)] = $value;
        }
    }
    $only_foo = array_values($only_foo);
    $products = Product::find($only_foo);
    foreach($products as $product){
      if($product->price == ""){
        $product->price = 0;
      }
      $product->amount = $amounts[$product->id];
    }

    $order = Order::create([
      'user_id' => $request->user()->id,
			'kol_vo' => 0,
      'status' => 0,
			'sum' => 0,
      'pay_success' => 0,
      'name' => $request->name,
      'surname' => $request->surname,
      'iin' => $request->iin,
      'delivery_type' => config('app.deliveries')[$request->delivery - 1],
      'payment_type' =>  config('app.payments')[$request->payment - 1],
      'address' => $request->address ?: null
    ]);
    
    $count_all = 0;
    $price_all = session('total_sum');

    foreach($products as $product) {
			$count_all += $product->amount;
			
			$order_product = AboutOrder::create([
				'order_id' => $order->id,
				'user_id' => $request->user()->id,
				'product_id' => $product->id,
				'kol_vo' => $product->amount
			]);
		}
		
		$order->kol_vo = $count_all;
		$order->sum = $price_all;
    $order->save();
    
    $request->user()->balance->fill([
      'balance' => 0
    ])->save();


    if($request->payment == 2) {
      $path1			 = app_path() . "/../qazkom_pay/config.txt"; // Путь к файлу настроек config.dat
      $order_id		 = $order->id; // Порядковый номер заказа - преобразуется в формат "000001"
      $currency_id	 = "398"; // Шифр валюты  - 840-USD, 398-Tenge 643
      $amount = $price_all;
      $content = process_request($order_id, $currency_id, $amount, $path1);
      $data['pay_content'] = $content;
      return view('orders.epay_form', $data);
    }
    foreach($_COOKIE as $key => $value){
      if(strpos($key, 'product') !== false){
        setcookie($key, '', "-999999");
      }
    }
    if(session('bonus_spent')) {
      session()->forget('bonus_spent');
    }
    return redirect('/')->with('orderComplete', true);
  }

  public function promo($id)
  {
    $promo = DB::table('promocodes')->where('promocode', 'like', $id)->first();
        if(is_null($promo)){
            return 'no';
        }
        else{
      $todaysdate=date("Y/m/d");
      if(strtotime($promo->expire_at) >= strtotime($todaysdate)){
        return $promo->percentage;
      }
      else{
        return 'no';
      }
      return 0;
        }        
  }
}