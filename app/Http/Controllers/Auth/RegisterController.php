<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\UserBonus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
			'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {   
		$user_array = [
        'name'=>$request->name,
        'surname'=>$request->surname,
        'email'=>$request->email,
        'password'=>Hash::make($request->password),
		];
		if($request->dop_email){
			$user_array['dop_email']= $request->dop_email;
		}
		if($request->company){
			$user_array['company'] = $request->company;
		}
		if($request->address){
			$user_array['address'] = $request->address;
		}
		if($request->city){
			$user_array['city'] = $request->city;
		}

		if($request->index){
			$user_array['index'] = $request->index;
		}
		if($request->country){
			$user_array['country'] = $request->country;
		}
		if($request->region){
			$user_array['region'] = $request->region;
		}
		if($request->file('avatar')){
        	$url_avatar=$request->file('avatar')->store('users','public');
			$user_array['avatar'] = $url_avatar;
		}

		$user=User::create($user_array);
        UserBonus::create([
            'user_id' => $user->id,
            'balance' => 0
        ]);
		
		return $user;

    }
}
