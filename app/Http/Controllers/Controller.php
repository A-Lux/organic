<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\MainPageCategory;
use App\Ocategory;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function layout(){

        $links=DB::select("SELECT * FROM links ORDER BY num DESC limit 5");
        $data["links"]=$links;
    	$head_sections=DB::select("SELECT sections.id,name FROM sections INNER JOIN heads ON sections.id=heads.id ORDER BY num LIMIT 4");
        $categories= Ocategory::join('main_page_categories', 'main_page_categories.category_id', '=', 'ocategories.id')
            ->select('ocategories.id', 'name')
            ->orderBy('position')
            ->get();
        //DB::select("SELECT id,name FROM ocategories LIMIT 5");

        $section_categories=DB::select("SELECT id,name FROM ocategories");

    	$data["head_sections"]=$head_sections;
    	$data["categories"]=$categories;
        $data["section_categories"]=$section_categories;
        $data['countries']=DB::select("SELECT id,name FROM countries");
        $data['regions']=DB::select("SELECT id,name FROM regions");

    	foreach ($section_categories as $key => $value) {
    		$subcategories[$value->id]=DB::select("SELECT id,name,cat_type,sorting FROM subcategories WHERE cat_type=".$value->id);
				usort($subcategories[$value->id], function ($a, $b) {
					return strcmp($a->sorting, $b->sorting);
				});
    	}
    	$data["subcategories"]=$subcategories;

        if (Auth::check()) {
            $user=Auth::user()->name;
        }
        else{
            $user="Вход";
        }
        // session()->forget('basket');
        // session()->forget('basket_total');
        if (session()->has("basket_total")) {
          $basket_total=session("basket_total");
        }
        else{
          $basket_total=0;
        }

        if (session()->has('likes')) {
          $likes=session('likes');
        }
        else{
            if (Auth::check()) {

            $liked_products=DB::select('SELECT id FROM likes WHERE user_id='.Auth::user()->id);
            if (count($liked_products)>0) {
            $likes=count($liked_products);
            session(['likes'=>$likes]);
            }
            else{
              $likes=0;
            }

            }
            else{
                $likes=0;
            }


        }

        if (session()->has('comparisons')) {
          $comparisons=session('comparisons');
        }
        else{
            if (Auth::check()) {

            $comparison_products=DB::select('SELECT id FROM comparisons WHERE user_id='.Auth::user()->id);
            if (count($comparison_products)>0) {
            $comparisons=count($comparison_products);
            session(['comparisons'=>$comparisons]);
            }
            else{
              $comparisons=0;
            }

            }
            else{
                $comparisons=0;
            }


        }

        $data["basket_total"]=$basket_total;
        $data["likes"]=$likes;
        $data["comparisons"]=$comparisons;
        $data["user"]=$user;

    	return $data;
    }


}
