<?php

namespace App\Http\Controllers;

use App\Comment;
use App\UserBonus;
use App\Subcategory;
use App\BonusesAmount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function show(Request $request){
    	$user=Auth::user();

    	$data=$this->layout();
    	$data["profile"]=$user;
        $data['countries']=DB::select("SELECT id,name FROM countries");
        $data['regions']=DB::select("SELECT id,name FROM regions");
    	return view("profile",$data);
    }

    public function review(){

    	$data=$this->layout();
    	$data["profile"]=Auth::user();

    	$data["comments"]=DB::table('comments')->join('products','comments.product_id','products.id')
        ->select('comments.text','comments.created_at','products.name as product_name','products.id as product_id')
        ->where('comments.user_id',Auth::user()->id)->where('published',1)->paginate(5);


    	return view("review",$data);

    }

    public function contact(){
    	$data=$this->layout();

    	return view("contact",$data);
    }

    public function purchase (Request $request) {
        $data=$this->layout();
        $orders = $request->user()->orders()->orderBy('created_at', 'desc')->get()->load('products');

        $data['orders'] = $orders;

    	return view("purchase-history",$data);
    }

    public function bonuses (Request $request) {
        $bonus_await = 0;
        $userBonus = $request
            ->user()
            ->balance
            ->balance;

        $orders = $request
            ->user()
            ->orders()
            ->where('status', 0)
            ->get();
        foreach($orders as $order) {
            $bonus = BonusesAmount::where('amount', '<=', $order->sum)->first();
            if($bonus) {
                $bonus_await += $bonus->percentage / 100 * $order->sum;
            }
        }

        $data=$this->layout();

        $data['orders']=DB::select('SELECT * FROM bonuses WHERE user_id = '.Auth::id().' ORDER BY id DESC');
        $data['bonus_await'] = $bonus_await;
        $data['userBonus'] = $userBonus;

        return view("bonus",$data);
    }

    public function question(Request $request){
        DB::table('questions')->insert([
            "name"=>$request->name,
            "mobile"=>$request->mobile,
            "created_at"=>now(),
            "status"=>2
        ]);

        return redirect("/")->with("message","question_sent");
    }

    public function logout(){

        Auth::logout();
        session()->flush();

        return redirect('/');
    }

    public function subscription(){
        DB::insert("INSERT INTO subscriptions (email) VALUES ('".$_POST["email"]."')");
        return back()->with("message",'subscribed');
    }

    public function account(Request $request){

        $data=$this->layout();
        $data["user_data"]=Auth::user();

        $data['countries']=DB::select("SELECT id,name FROM countries");
        $data['regions']=DB::select("SELECT id,name FROM regions");
        $data["views"]=DB::select("SELECT DISTINCT products.id,sale,cover,text_cover,name,volume,description,discount,f_price,s_price FROM products INNER JOIN views ON products.id=views.product_id WHERE views.user_id=".Auth::user()->id." ORDER BY views.id DESC");

        return view('account',$data);
    }

    public function update(Request $request){

        $data=[

        'name'=>$request->name,
        'surname'=>$request->surname,
        'email'=>$request->email,

        'dop_email'=>$request->dop_email,
        'company'=>$request->company,
        'address'=>$request->address,
        'city'=>$request->city,

        'index'=>$request->index,
        'country'=>$request->country,
        'region'=>$request->region

        ];

        if ($request->hasFile('avatar')) {
            $avatar=$request->file('avatar')->store('users','public');
            $data['avatar']=$avatar;
        }
        if (!empty($request->password)) {
            $data['password']=Hash::make($request->password);
        }


        DB::table('users')->where('id',$request->id)->update($data);
        return redirect ('/home/account')->with('updated',true);
    }

    public function call(Request $request){

        DB::table('calls')->insert([
            'name'=>$request->name,
            'mobile'=>$request->mobile,
            'text' => $request->text,
            'created_at'=>NOW(),
            'status'=>0
        ]);

        return back()->with('called',true);

    }

    public function recomended(Request $request) {

        $data = $this->layout();
        $orders = $request->user()->orders->load('products');
        $ordersCategories = [];
        foreach($orders as $order) {
            foreach($order->products as $product) {
                if(!in_array($product->sub_type, $ordersCategories)) {
                    $ordersCategories[] = $product->sub_type;
                }
            }
        }
        $ordersCategories = Subcategory::select(['id', 'name'])->whereIn('id', $ordersCategories)->get();
        $data['orders_categories'] = $ordersCategories;
        return view('profile.recomended', $data);
    }
}
