<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ComparisonsController extends Controller
{   

public function add(){

  $user_id=Auth::id();
  $product_id=$_GET["id"];

  $comparisons=DB::select('SELECT id FROM comparisons WHERE user_id='.$user_id);

  if (count($comparisons)>=5) {
   die('777');
  }

  $select=DB::select("SELECT id FROM comparisons WHERE user_id=".$user_id." AND product_id=".$product_id);
  if (empty($select)) {
    
    if (session()->has('comparisons')) {
      session(['comparisons'=>session('comparisons')+1]);
    }
    else{
      session(['comparisons'=>1]);
    }
    
    DB::insert("INSERT INTO comparisons (user_id,product_id,created_at) VALUES (".$user_id.",".$product_id.",NOW())");
    echo session('comparisons');
  }
  else{
    echo 0;
  }

  
  
}

public function del(Request $request){

  $user_id=Auth::id();

  $delete=DB::delete("DELETE FROM comparisons WHERE product_id=".$request->id." AND user_id=".$user_id);
  session(['comparisons'=>session('comparisons')-1]);
  return redirect('/comparisons')->with('deleted',true);

}

public function show(){
  
if (Auth::check()) {
  $data=$this->layout();
  $user_id=Auth::id();

  $data["products"]=DB::select("SELECT products.id,products.name,products.cover,products.description,products.final_price,products.count,brands.country
        FROM products INNER JOIN comparisons ON comparisons.product_id=products.id INNER JOIN brands ON products.brand=brands.id WHERE user_id=".$user_id);


  return view('comparisons',$data);
}
else{
   return back()->with('not_autorized',true);
}
}

}
