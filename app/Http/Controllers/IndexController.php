<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function show(){
    	$data=$this->layout();

    	$day_products=DB::select("SELECT id,name,volume,description,f_price,cover FROM products WHERE day=1 ORDER BY id DESC");
    	$sale_products= Product::where('sale', 1)->limit(6)->get();// DB::select("SELECT id,name,volume,description,f_price,cover FROM products WHERE sale=1 ORDER BY id DESC LIMIT 6");
    	$new_products=DB::select("SELECT id,name,volume,description,f_price,cover FROM products WHERE new=1 ORDER BY id DESC LIMIT 6");
    	$hit_products=DB::select("SELECT id,name,volume,description,f_price,cover FROM products WHERE hit=1 ORDER BY id DESC LIMIT 6");
        $news=DB::select("SELECT id,title,body,cover FROM news ORDER BY id DESC LIMIT 2");
        $comments=DB::select("SELECT users.name,users.avatar,comments.text,comments.created_at,comments.id FROM comments INNER JOIN users ON users.id=comments.user_id WHERE published=1 AND main=1 ORDER BY id DESC LIMIT 3");

        $banners=DB::select("SELECT * FROM banners ORDER BY num");

    	$data["day_products"]=$day_products;
    	$data["sale_products"]=$sale_products;
    	$data["new_products"]=$new_products;
    	$data["hit_products"]=$hit_products;
        $data["banners"]=$banners;
        $data["news"]=$news;
        $data["comments"]=$comments;



    	return view("index",$data);
    }
}
