<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Bonus extends Model
{
    protected $fillable = [
        'user_id',
        'order_id',
        'bonus',
        'enrol_date'];    
}
