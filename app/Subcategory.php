<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Subcategory extends Model
{
    public $fillable = [
		'name',
		'cat_type',
		'paloma_temp',
	];
}
