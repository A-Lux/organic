<?php

namespace App\Traits;

trait ShowsViewedProducts {
    static public function addToViewed($id) {
        if(!session()->has('viewed')) {
            session(['viewed' => []]);
        }
        if(!in_array($id, session('viewed'))) {
            session()->push('viewed', $id);
        }
    }

    static private function getViewedIds() {
        return session('viewed');
    }

    static public function getViewedProducts() {
        if(static::getViewedIds()) {
            return static::whereIn('id', static::getViewedIds())->get();
        }
    }
}