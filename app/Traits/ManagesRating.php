<?php

namespace App\Traits;

use App\Comment;

trait ManagesRating {
    private static function getRatings($id) {
        $comments = Comment::select(['rating'])
            ->where('published', 1)
            ->whereNotNull('rating')
            ->where('product_id', $id)
            ->get();
    
        return $comments->pluck('rating');
    }
    public static function calculateRating($id) {
        $ratingCalculated = 0;
        $ratings = static::getRatings($id);
        foreach($ratings as $rating) {
            $ratingCalculated += $rating;
        }

        $ratingCalculated != 0 ? $ratingCalculated /= $ratings->count() : null;
        return $ratingCalculated;
    }
}