<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait ManagesPromocodes {
    static private $promocodes = [];
    
    static public function generate($amount = 1, $percentage = 1,$length = 12) {
        for($i = 0; $i < $amount; $i++) {
            static::$promocodes[] = ['promocode' => Str::random($length), 'percentage' => $percentage];
        }
        return static::$promocodes;
    }

    static public function expired($promocode) {
        $p = static::where('promocode', $promocode)->first();
        return $p->expire_at < (new \DateTime('',new \DateTimeZone("Asia/Almaty")))->format('Y-m-d h:m:s');
    }

    static public function exists($promocode) {
        return static::where('promocode', $promocode)->exists();
    }

    static public function getPromocode($promocode) {
        return static::where('promocode', $promocode)->first();
    }
}