<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ActivePromocode extends Model
{
    protected $fillable = ['promocode_id'];
    public function promocode() {
        return $this->belongsTo(\App\Promocode::class);
    }
}
