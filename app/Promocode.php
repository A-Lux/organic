<?php

namespace App;

use App\Traits\ManagesPromocodes;
use Illuminate\Database\Eloquent\Model;


class Promocode extends Model
{
    use ManagesPromocodes;
    protected $fillable = ['promocode', 'percentage', 'expire_at'];   
}
