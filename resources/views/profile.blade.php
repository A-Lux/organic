@extends("layout")

@section('body')
 <!-- MAIN -->
  <main class="main">
    <!-- BREAD CRUMBS -->
    <ul class="bread-crumbs container">
      <li><a href="/">Главная</a></li>
      <li><a href="/home">Личный кабинет</a></li>
    </ul>
    <!-- END BREAD CRUMBS -->

    <!-- PERSONAL CABINET -->
    <div class="personal-cabinet">
      <div class="personal-row container">
        <div class="profile-left-side">
          <div class="profile-left-side__photo-and-name">
            <div class="profile-left-side__img">
              <img src="/storage/{{$profile->avatar}}" style="width:50px;height: 50px;">
            </div>
            <div class="profile-left-side__name">
              <h3>{{$profile->name}}</h3>
              <p>Ваш логин/e-mail</p>
            </div>
            <a href="/home/logout" class="logout">
              <img src="images/sign.png" />
            </a>
          </div>
          <div class="mail-text-field">
            <input type="text" value="{{$profile->email}}" >
          </div>
          <button class="btn-personal-datas" type="button" name="button">
            <a href="/home/account">ЛИЧНЫЕ ДАННЫЕ</a>
          </button>
        </div>
        <div class="profile-right-side">
          <h3>Личный кабинет</h3>
          <div class="profile-menu-wrap">
            <a href="/home/account" class="profile-menu-item">
              <span class="menu-img"><img src="/images/contacts.svg" /></span>
              <p>Контакты</p>
            </a>
            <a href="/likes" class="profile-menu-item">
              <span class="menu-img"><img src="/images/bookmarks.svg" /></span>
              <p>Закладки</p>
            </a>
            <a href="/home/reviews" class="profile-menu-item">
              <span class="menu-img"><img src="/images/reviews.svg" /></span>
              <p>Отзывы</p>
            </a>
            <a href="/home/purchase" class="profile-menu-item">
              <span class="menu-img"><img src="/images/orders.svg" /></span>
              <p>Заказы</p>
            </a>
            <a href="/home/bonuses" class="profile-menu-item">
              <span class="menu-img"><img src="/images/bonus.svg" /></span>
              <p>Бонусная программа</p>
            </a>
            <a href="{{ route('user.recomended') }}" class="profile-menu-item">
              <span class="menu-img"><img src="/images/recommend.svg" /></span>
              <p>Рекоммендуемое</p>
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- END PERSONAL CABINET -->

    <div class="empty-space">
      <div class="question-block container">
        <div class="question-absolute wow rotateInUpLeft" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: rotateInUpLeft;">
          <h3>Хотите задать вопрос?</h3>
          <p>Позвоните нам на номер +7(707) 222 - 0524<br>
          или заполните форму и мы перезвоним в течении трёх минут!</p>
          <div class="question-inputs">
            <form action="#Отправка" method="post">
              <input type="text" placeholder="Ваше имя">
              <input type="tel" placeholder="Номер телефона: 8-777-777-77-77" pattern="+[0-9]{1}-[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}" required style="width: 290px;" maxlength="16">
              <button type="button" class="btn-form" name="button">отправить</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </main>
  <!-- END MAIN -->
@endsection