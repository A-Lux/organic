@extends("layout")

@section('body')
  <div class="container company">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="company__head">
          <p class="company__title">{{$section->name}}</p>
        </div>
        <div class="company__body">
          <div style="word-wrap: break-word; ">
              {!!$section->body!!}
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN -->
@endsection