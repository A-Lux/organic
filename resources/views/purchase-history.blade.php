@extends("layout")

@section('body')
 <!-- MAIN -->
  <main class="main">
    <!-- BREAD CRUMBS -->
    <ul class="bread-crumbs container">
      <li><a href="/home">Личный кабинет</a></li>
      <li><a href="#">Заказы</a></li>
    </ul>
    <!-- END BREAD CRUMBS -->

    <!-- PURCHASE HISTORY -->
    <div class="purchase-history">
      <div class="container">
        <div class="title">
          <h3>История заказов</h3>
        </div>
        <div class="purchase-history__table">
          <ul>
            <li>
              <span>№ заказа</span>
              <span>Дата заказа</span>
              <span>Дата отправки</span>
              <span>Статус</span>
              <span>Итого</span>
            </li>
            @foreach($orders as $order)
            <li>
              <span class="order-btn-more" data-tab="order{{ $loop->iteration }}">#{{$order->id}}</span>
              <span>{{$order->created_at}}</span>
              <span>{{$order->send_date}}</span>
              <span>{{ config('app.status')[$order->status] }}</span>
              <span>{{$order->sum}} тг</span>
            </li>
            <ul id="order{{ $loop->iteration }}" style="display: none;">
              <li>
                <span></span>
                <span>№ заказа</span>
                <span>Наименование</span>
                <span>Количество</span>
                <span>Цена за шт.</span>
                <span>Итого</span>
              </li>

              @foreach ($order->products as $product)
                <li>
                    <span></span>
                  <span>#{{ $order->id }}</span>
                  <span>{{ $product->name }}</span>
                  <span>{{ $order->kol_vo }}</span>
                  <span>{{ $product->f_price }} тг.</span>
                  <span>{{ $product->f_price * $order->kol_vo }} тг.</span>
                </li>
              @endforeach
            </ul>
            @endforeach
          </ul>
        </div>
        <a class="prev-btn" href="{{ route('profile') }}">Назад</a>
      </div>
    </div>
    <!-- END PURCHASE HISTORY -->
  </main>
@endsection
