@extends("layout")

@section('body')

@if(session()->has('send_message'))
<script type="text/javascript">
  sweetalert('success','Ваше сообщение успешно отправлено в обработку!',3000);
</script>
@endif
<section class="contacts-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <p class="contacts-page__title">
                    Контактная информация
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="contacts-page__wrapper">
                    <div class="col-lg-7 col-sm-12">
                        <div class="col-lg-12 col-sm-12 px-0">
                            <p class="contacts-page__wrapper-title">Мы ценим Ваше время и рады ответить на любой Ваш
                                вопрос:</p>
                        </div>
                        <div class="col-lg-12 col-sm-12 px-0">
                            <ul class="contacts-page__wrapper-feedback">
                                <li>
                                    <p>{{ $contact->phone }}</p>
                                </li>
                                <li>
                                    <a href="#" class="btn-call-us">ПЕРЕЗВОНИТЕ МНЕ</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-12 col-sm-12 px-0">
                            <ul class="contacts-page__feedback-info">
                                <li>
                                    <p>Время работы: {{ $contact->weekday_start }} - {{ $contact->weekday_end }} с {{ $contact->work_start }} до {{ $contact->work_end }}</p>
                                </li>
                                <li>
                                    <p>По всем вопросам: <a href="#">{{ $contact->email }}</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-5 col-sm-12">
                        <div class="contacts-page__feedback-girl">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div id="wrapper-9cd199b9cc5410cd3b1ad21cab2e54d3" class="feedback-map">
                    <div id="map-9cd199b9cc5410cd3b1ad21cab2e54d3"></div>
                    <script>
                        (function () {
                            var setting = {
                                "height": 500,
                                "width": 100000,
                                "zoom": 16,
                                "queryString": "ТЦ Fashion Avenue Бутик 11 Bigsport.kz, проспект Сейфуллина, Алматы, Казахстан",
                                "place_id": "ChIJBbUlp4BvgzgROTuj3gS83qw",
                                "satellite": false,
                                "centerCoord": [43.25164205658531, 76.93379535740962],
                                "cid": "0xacdebc04dea33b39",
                                "cityUrl": "/kazakhstan/almaty-10297",
                                "id": "map-9cd199b9cc5410cd3b1ad21cab2e54d3",
                                "embed_id": "66407"
                            };
                            var d = document;
                            var s = d.createElement('script');
                            s.src = 'https://1map.com/js/script-for-user.js?embed_id=66407';
                            s.async = true;
                            s.onload = function (e) {
                                window.OneMap.initMap(setting)
                            };
                            var to = d.getElementsByTagName('script')[0];
                            to.parentNode.insertBefore(s, to);
                        })();
                    </script>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection