@extends("layout")

@section('body')
  <!-- MAIN -->
  <main class="main">
    <!-- BREAD CRUMBS -->
    @if(session()->has('deleted'))
<script type="text/javascript">
  sweetalert('success','Товар успешно удален с избранных!',3000);
</script>
@endif
    <!-- END BREAD CRUMBS -->
        <ul class="bread-crumbs container">
      <li><a href="/">Главная</a></li>
      <li><a href="#">Избранные</a></li>
    </ul>
    <!-- ORGANIC CATALOG -->
    <div class="organic-catalog">
      <div class="container">
        <div class="catalog-section">
          <div class="title">
            <h3>Избранные товары</h3>
          </div>
        </div>
              @php
              $i=1;
              @endphp

              @if(empty($products))

              <p style="color: red;">Нет товаров!</p>
              
              @endif

        <div class="catalog-wrapper">
          <div class="catalog_right-side">
                        <div class="catalog-products-wrapper">
                        @foreach($products as $key=>$product)
                        <div class="catalog-product-item">
                          <div class="catalog-img">
                            <a href="/product-card/{{$product->id}}"><img src="/storage/{{$product->cover}}"></a>
                            @if(!empty($product->discount))
                            <span class="catalog-stock">-{{$product->discount}}</span>
                            @endif
                          </div>
                          <h4><a href="/product-card/{{$product->id}}">{{$product->name}}</a> / {{$product->volume}} мл.</h4>
                          <p>{{$product->text_cover}}</p>
                           @if(!empty($product->s_price))
                          <div class="price">{{$product->s_price}} тнг
                            <span>
                            {{$product->f_price}} тнг
                            </span>
                          </div>
                          @else
                          <div class="price">{{$product->f_price}} тнг </div>
                          @endif
                          
                          @php 
                          $rating = \App\Product::calculateRating($product->id);
                          @endphp
                          <div class="star-line">
                            <ul>
                              @for($i = 1; $i <= 5; $i++)
                              @if($i <= $rating)
                              <li style="color: #eeb900">&#9733;</li>
                              @else
                              <li style="color: #d4d4d4;">&#9733;</li>
                              @endif
                              @endfor
                            </ul>
                          </div>
                          <button class="btn-basket" name="btn-basket"     id="{{$product->id}}"   onclick="add_basket(this.id)">
                            В КОРЗИНУ
                          </button>
                          <a class="delete" style="position: relative; top:1rem;" href="/del_like?id={{$product->id}}">Удалить</a>
                        </div>

                        @php

                        unset($products[$key]);
                        if($i==8){
                        break;
                        }
                        else{
                        $i++;
                        }
                        
                        @endphp
                        
                        @endforeach
                      </div>
        <!-- БЛОК ПОКАЗАТЬ ЕЩЕ -->
         @if(!empty($products))
        <div class="catalog-products-wrapper-more" id="moreProduct" style="display: none;">
          @foreach($products as $product)
          <div class="catalog-product-item">
                          <div class="catalog-img">
                            <a href="/product-card/{{$product->id}}"><img src="/storage/{{$product->cover}}"></a>
                            @if(!empty($product->discount))
                            <span class="catalog-stock">-{{$product->discount}}</span>
                            @endif
                          </div>
                          <h4><a href="/product-card/{{$product->id}}">{{$product->name}}</a> / {{$product->volume}} мл.</h4>
                          <p>{{$product->text_cover}}</p>
                           @if(!empty($product->s_price))
                          <div class="price">{{$product->s_price}} тнг
                            <span>
                            {{$product->f_price}} тнг
                            </span>
                          </div>
                          @else
                          <div class="price">{{$product->f_price}} тнг </div>
                          @endif
                          
                          @php 
                          $rating = \App\Product::calculateRating($product->id);
                          @endphp
                          <div class="star-line">
                            <ul>
                              @for($i = 1; $i <= 5; $i++)
                              @if($i <= $rating)
                              <li style="color: #eeb900">&#9733;</li>
                              @else
                              <li style="color: #d4d4d4;">&#9733;</li>
                              @endif
                              @endfor
                            </ul>
                          </div>
                          <button class="btn-basket" name="btn-basket"     id="{{$product->id}}"   onclick="add_basket(this.id)">
                            В КОРЗИНУ
                          </button>
                          <a class="delete" href="/del_like?id={{$product->id}}" style="position: relative; top:1rem;">Удалить</a>
                        </div>
                        @endforeach
        </div>
                   
                      <div class="show-all-products">
                        <a href="#moreProduct" class="more-btn" onclick="show_hide(this.name)" name='0' id="show_more">ПОКАЗАТЬ ЕЩЁ</a>
                      </div>
           @endif
          </div>
        </div>
      </div>
    </div>
    <!-- END ORGANIC CATALOG -->


    <!-- SUSCRIPTION BLOCK -->
    <div class="white-bg-wrapp">
      <div class="subscription-block container wow rotateInUpLeft" data-wow-duration="1s">
        <div class="subscription-absolute">
          <div class="subscription-content">
            <h3>Хотите быть в курсе всех новостей?</h3>
            <span>Подпишитесь на рассылку!</span>
            <div class="subscription-wrapp">
              <div class="sub-social">
                <h4>Следите за нами в соц сетях:</h4>
                <ul class="social-networks">
                   @foreach($links as $link)
                  <li><a href="{{$link->link}}"><img src="/images/{{$link->type}}.png"></a></li>
                  @endforeach
                </ul>
              </div>
              <div class="sub-row">
                <div class="sub-button-input">
                  <form action="/subscription" method="POST">
                    @csrf
                    <input type="text" name="email" placeholder="Введите Электронную почту">
                    <input class="sub-btn" type="submit" value="ПОДПИСАТЬСЯ">
                  </form>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END SUSCRIPTION BLOCK -->
  </main>
  <!-- END MAIN -->
@endsection