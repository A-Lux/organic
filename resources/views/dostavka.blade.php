@extends("layout")

@section('body')

    <!-- ###########################   ДОСТАВКА    ###########################-->
    <section class="dostavka">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h1 class="dostavka-title">Доставка в любую точку Казахстана.</h1>
                    <p class="dostavka-title__small" style="line-height:1.5">Прием заказов на сайте 24 часа в сутки.
Менеджер оформляет заказы по мере загруженности  Пн.-Вс.. с 11.00 до 19.00</p>
					
					<h3 style="font-weight:600;font-size: 20px;line-height: 1.2">Доставка по Алматы:</h3>
					<p class="dostavka-title__small" style="line-height:1.5">Доставка по Алматы осуществляется в день заказа или на следующий день после оформления заказа, это зависит от времени поступления заказа и загруженности курьеров.</p>
					<p class="dostavka-title__small" style="line-height:1.5">Стоимость доставки по Алматы 700 тг, заказы свыше 7000 тенге - доставляются бесплатно.</p>
					<p class="dostavka-title__small" style="line-height:1.5">Заказы сделанные в воскресенье или праздничные дни могут быть доставлены в рабочие дни. </p>
					<h3 style="font-weight:600;font-size: 20px;line-height: 1.2">Доставка по Казахстану:</h3>
					<p class="dostavka-title__small" style="line-height:1.5">Доставка осуществляется курьерской службой до двери (исключение мелкие отдаленные населенные пункты, в данных населенных пунктах выдача посылки осуществляется в почтовом отделение) стоимость доставки от 1000 тг, заказы свыше 10000 тенге - доставляются бесплатно.</p>
					<p class="dostavka-title__small" style="line-height:1.5">Заказы сделанные в выходные и праздничнее дни обрабатываются день в день, но отправлены будут в первый рабочий день.</p>
                    <h1 class="dostavka-title">Оплата</h1>
                    <div class="icons-information">
                    	<div class="icons-item">
                    		<img src="/images/icon1.png" alt="">
                    		<span>На сайте картой VISA/Mastercard</span>
                    	</div>
                    	<div class="icons-item">
                    		<img src="/images/icon2.png" alt="">
                    		<span>Наличными курьеру</span>
                    	</div>
                    	<div class="icons-item">
                    		<img src="/images/icon3.png" alt="">
                    		<span>Оплата при самовывозе</span>
                    	</div>
                    	<div class="icons-item">
                    		<img src="/images/icon4.png" alt="">
                    		<span>На карту “Каспи”</span>
                    	</div>
                    	<div class="icons-item">
                    		<img src="/images/icon5.png" alt="">
                    		<span>На карту “Казком”</span>
                    	</div>
                    	<div class="icons-item">
                    		<img src="/images/icon6.png" alt="">
                    		<span>Кошелек QIWI</span>
                    	</div>
                    </div>
                    <h1 class="dostavka-title">Доставка</h1>
                    <div class="icons-information">
                    	<div class="icons-item">
                    		<img src="/images/icon1_d.png" alt="">
                    		<span>Бесплатная доставка по Алматы при заказе от 7 000 тнг</span>
                    	</div>
                    	<div class="icons-item">
                    		<img src="/images/icon2_d.png" alt="">
                    		<span>Бесплатная доставка по РК при заказе от 10 000 тнг</span>
                    	</div>
                    	<div class="icons-item">
                    		<img src="/images/icon3_d.png" alt="">
                    		<span>Доставка до двери</span>
                    	</div>
                    	<div class="icons-item">
                    		<img src="/images/icon4_d.png" alt="">
                    		<span>Минимальные сроки (по Алматы 1раб день, по РК 2-4 раб дня)</span>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection