<form name="SendOrder" method="post" action="https://epay.kkb.kz/jsp/process/logon.jsp">
    <input type="hidden" name="Signed_Order_B64" value="{{$pay_content}}">
    <input type="hidden" name="email" size=50 maxlength=50 value="">
    <input type="hidden" name="Language" value="rus">
    <input type="hidden" name="BackLink" value="{{url('/basket/pay-result')}}">
    <input type="hidden" name="FailurePostLink" value="{{url('/basket/pay-result')}}">
    <input type="hidden" name="PostLink" value="{{url('/basket/pay-result')}}">
    <input type="submit" id="pay" name="GotoPay" value="" style="display: none;">
</form>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        document.getElementById('pay').click();
    });
</script>