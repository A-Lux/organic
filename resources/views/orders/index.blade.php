@extends("layout")

@section('body')
<!-- PAYMENT SECTION -->

<section class="payment">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="payment-price__title">
                    <p>Всего к оплате : <span>{{ $total_sum }}тг</span></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="payment-price__wrapper-input_main">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <form action="{{ route('process-order') }}" method="POST">
                            @csrf
                            <input type="hidden" name="products" value="{{ $products }}">
                            <div class="payment-price__wrapper-input">
                                <input type="text" name="name" id="" placeholder="Введите имя" value="{{ Auth::user()->name }}">
                                <input type="text" name="surname" id="" placeholder="Введите фамилию" value="{{ Auth::user()->surname }}">
                                <input type="text" name="iin" id="" placeholder="Введите телефон">
                            </div>
                            <div class="payment-price__wrapper-select">
                                <div class="input-group mb-3 payment-select">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputGroupSelect01">Способ
                                            доставки</label>
                                    </div>
                                    <select class="custom-select" name="delivery" id="inputGroupSelect01">
                                        <option selected>Choose...</option>
                                        <option value="1">Курьером</option>
                                        <option value="2">Самовывоз</option>
                                    </select>
                                </div>
                            </div>
                            <div class="addBlock" style="display: none;" id="1">
                                <input type="text" name="address" id="" placeholder="Адрес">
                            </div>
                            <div class="addBlock" style="display: none;" id="2">
                                <p>г.Алматы, Сейфуллина 498/1, уг.ул. Богенбай батыра, ТЦ "Fashion Avenue", 25 бутик
                                </p>
                            </div>
                            <div class="payment-price__wrapper-select">
                                <div class="input-group mb-3 payment-select">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="inputGroupSelect01">Способ
                                            оплаты</label>
                                    </div>
                                    <select class="custom-select" name="payment" id="inputGroupSelect02">
                                        <option selected>Choose...</option>
                                        <option value="1">По факту</option>
                                        <option value="2">Онлайн</option>
                                    </select>
                                </div>
                            </div>
                            <input type="submit" value="Отправить">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- END PAYMENT SECTION -->
@push('scripts')
<script>
    $(document).ready(function () {
        $('#inputGroupSelect01').change(function () {
            var select = $(this).val();
            var activeBlock = $('.addBlock');
            $('.addBlock').removeClass('active-form');

            for (const i of activeBlock) {
                var numAttr = i.getAttribute('id');
                if (numAttr == select) {
                    i.classList.add('active-form');
                }
            }
        });
    });
</script>
@endpush
@endsection