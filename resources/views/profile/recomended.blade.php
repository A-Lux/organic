@extends("layout")

@section('body')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <ul>
            @foreach ($orders_categories as $category)
                <li class="profile_recomended_category" id="{{ $category->id }}" style="cursor: pointer">{{ $category->name }}</li>
            @endforeach
            </ul>
        </div>
        <div class="catalog_right-side">
            <div class="catalog-products-wrapper" id="products_wrapper">

            </div>
        </div>
    </div>
</div>
@endsection