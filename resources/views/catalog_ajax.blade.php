@if(empty($products))
    <h3 class="no-products-title">По данному запросу нет товаров!</h3>
@endif

@php
    $i=1;
@endphp

<div class="catalog-products-wrapper">
    @foreach($products as $key=>$product)
        <div class="catalog-product-item">
            <div class="catalog-img">
                @if(!empty($product->discount))
                    <span class="catalog-stock">-{{$product->discount}}</span>
                @endif
            </div>
            <h4>
                <a href="/product-card/{{$product->id}}">
                    {{$product->name}}
                </a> / {{$product->volume}} мл.
            </h4>
            <p>
                {{
                    substr(
                            $product->text_cover,
                            0,
                            strrpos(
                                substr($product->text_cover, 0, 50),
                                ' '
                            )
                    )
                }}
            </p>
            @if(!empty($product->s_price))
                @if(!empty($product->s_price))
                    @if($product->s_price != $product->f_price)
                        <div class="price">{{$product->s_price}} тнг
                            @if(trim($product->f_price) != 0)
                                <span>
                                    {{$product->f_price}} тнг
                                </span>
                            @endif
                        </div>
                    @else
                        <div class="price">{{$product->f_price}} тнг </div>
                    @endif
                @endif
            @endif
            @php
                $product->rating = \App\Product::calculateRating($product->id);
            @endphp
            <ul style="display: flex">
                @for($i = 1; $i <= 5; $i++)
                    @if($i <= $product->rating)
                        <li style="color: #eeb900">&#9733;</li>
                    @else
                        <li style="color: #d4d4d4;">&#9733;</li>
                    @endif
                @endfor
            </ul>
            <button class="btn-basket" name="btn-basket"     id="{{$product->id}}"   onclick="add_basket(this.id)">
                В КОРЗИНУ
            </button>
        </div>
        @php

            unset($products[$key]);

            if($i==9){
                break;
            }
            else{
                $i++;
            }

        @endphp

    @endforeach
</div>

<!-- БЛОК ПОКАЗАТЬ ЕЩЕ -->

@if(!empty($products))
    <div class="catalog-products-wrapper-more" id="moreProduct" style="display: none;">
        @foreach($products as $product)
            <div class="catalog-product-item">
                <div class="catalog-img">
                    <a href="/product-card/{{$product->id}}">
                        <img src="/storage/{{$product->cover}}">
                    </a>
                    @if(!empty($product->discount))
                        <span class="catalog-stock">-{{$product->discount}}</span>
                    @endif
                </div>
                <h4>
                    <a href="/product-card/{{$product->id}}">
                        {{$product->name}}
                    </a> / {{$product->volume}} мл.
                </h4>
                <p>
                    {{$product->text_cover}}
                </p>
                @if(!empty($product->s_price))
                    <div class="price">{{$product->s_price}} тнг
                        <span>
                            {{$product->f_price}} тнг
                        </span>
                    </div>
                @else
                    <div class="price">
                        {{$product->f_price}} тнг
                    </div>
                @endif
                @php
                    $product->rating = \App\Product::calculateRating($product->id);
                @endphp
                <div class="star-line">
                    <ul style="display: flex">
                        @for($i = 1; $i <= 5; $i++)
                        @if($i <= $product->rating)
                        <li style="color: #eeb900">&#9733;</li>
                        @else
                        <li style="color: #d4d4d4;">&#9733;</li>
                        @endif
                        @endfor
                    </ul>
                </div>
                <button class="btn-basket"
                        name="btn-basket"
                        id="{{$product->id}}"
                        onclick="add_basket(this.id)">
                    В КОРЗИНУ
                </button>
            </div>
        @endforeach
    </div>

    <div class="show-all-products">
    <!--<a href="#moreProduct" class="more-btn">ПОКАЗАТЬ ЕЩЁ</a>-->
        {{ $products->links() }}
        <span id="currentURL" style="display: none">{{ $url }}</span>
    </div>
@endif
