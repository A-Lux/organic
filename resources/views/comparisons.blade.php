@extends("layout")

@section('body')

@if(session()->has('deleted'))
<script type="text/javascript">
  sweetalert('success','Товар успешно удален с сравнения!',3000);
</script>
@endif
<div class="main-srav">
    <ul class="bread-crumbs container">
      <li><a href="/">Главная</a></li>
      <li><a href="#">Сравнение</a></li>
    </ul>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            @if(empty($products))
                <h3 style="font-family: 'Poppins', sans-serif; color: red; font-size: 36px; margin-bottom: 40px; margin-top: 30px;">Нет товаров!</h3>
            @else
        </div>
    </div>
</div>
    <section class="srav">
        <div class="container">
            <div class="row">
                <div class="srav-title">
                    <h3>Сравнение товаров</h3>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <td class="comparison-table__feature-name"></td>
                                @foreach($products as $product)
                                <td>
                                    <div class="comparison-table__product-title">
                                        <a class="comparison-table__product-delete" href="/del_comparison?id={{$product->id}}">x</a>
                                        <div class="comparison-table__product">
                                            <div class="compras-srav-head" style="display: flex;flex-direction: column;justify-content: center;align-items: center;">
                                            <img
                                                src="{{Voyager::image($product->cover)}}">

                                            <a href="/product-card/{{ $product->id }}" class="comparison-table__product-name">{{$product->name}}</a>
                                            <p class="comparison-table__product-price">{{$product->final_price}} тг</p>
</div>
                                            <button class="comparison-table__product-buy" onclick="addToCart({{$product->id}})">Купить</button>
                                        </div>
                                    </div>
                                </td>
                                @endforeach

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row" class="comparison-table__feature-name">Цена : </th>
                                 @foreach($products as $product)
                                <td>{{$product->final_price}} тг</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th scope="row" class="comparison-table__feature-name">Производитель : </th>
                                @foreach($products as $product)
                                <td>{{$product->country}}</td>
                                @endforeach
                            </tr>
                            <tr>
                                <th scope="row" class="comparison-table__feature-name">Доступность : </th>
                                @foreach($products as $product)
                                @if($product->count>0)
                                <td>В наличии</td>
                                @else
                                <td>Нет в наличии</td>
                                @endif
                                @endforeach
                            </tr>
                            <!-- <tr>
                                <th scope="row" class="comparison-table__feature-name">Рейтинг: </th>
                                <td>На основании 0 отзыва(ов)</td>
                                <td>На основании 0 отзыва(ов)</td>
                            </tr> -->
                            <tr>
                                <th scope="row" class="comparison-table__feature-name">Краткое описание : </th>
                                @foreach($products as $product)
                                <td>{{$product->description}}</td>
                              @endforeach
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    @endif
@endsection
