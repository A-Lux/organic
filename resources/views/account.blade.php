@extends("layout")

@section('body')
  <!-- MAIN -->
  <main class="main">
    <!-- BREAD CRUMBS -->
    <ul class="bread-crumbs container">
      <li><a href="/home">Личный кабинет</a></li>
      <li><a href="/home/account">Редактировать профиль</a></li>
    </ul>
    <!-- END BREAD CRUMBS -->
    @if(session()->has('updated'))
    <script type="text/javascript">
      sweetalert('success','Данные успешно сохранены!',3000);
    </script>
    @endif
    <!-- ACCOUNT -->
    <div class="account-block">
      <div class="container">
        <div class="title">
          <h3>Ваша учетная запись</h3>
        </div>
        <div class="account-form">
          <form action="/home/update" method="post" enctype="multipart/form-data">
            @csrf
            <input type="text" name="id" placeholder="Имя" value="{{ $user_data->id }}" hidden>
            <input type="text" name="name" placeholder="Имя" value="{{ $user_data->name }}">
            <input type="text" name="surname" placeholder="Фамилия" value="{{ $user_data->surname }}">
            <input type="email" name="email" placeholder="E-mail" value="{{ $user_data->email }}">
            <input type="password" name="password" minlength="8" placeholder="При неизменении пароля оставьте поле пустым!">

            <input type="text" name="company" pattern="[A-Za-zА-Яа-яЁё]{1,}" placeholder="Компания" value="{{ $user_data->company }}">

            <input type="text" name="address" placeholder="Адрес" value="{{ $user_data->address }}">
            <input type="text" name="city" pattern="[A-Za-zА-Яа-яЁё]{1,}" placeholder="Город" value="{{ $user_data->city }}">
            <input type="text" name="index" pattern="[0-9]{6}" placeholder="Почтовый индекс" value="{{ $user_data->index }}">

<!--             <input type="email" name="country" placeholder="Страна" value="{{ $user_data->country }}">
            
            <input type="tel" name="region" placeholder="Регион" value="{{ $user_data->region }}"> -->
            <div class="col modal-select" >
            <select name="country">
              @foreach($countries as $country)
              @if($country->id == $user_data->country)
              <option value="{{$country->id}}" selected="">{{$country->name}}</option>
              @else
              <option value="{{$country->id}}">{{$country->name}}</option>
              @endif
              @endforeach
            </select>
          </div>
          <div class="col modal-select" >
            <select name="region">
              @foreach($regions as $region)
               @if($region->id == $user_data->region)
              <option value="{{$region->id}}" selected="">{{$region->name}}</option>
              @else
               <option value="{{$region->id}}">{{$region->name}}</option>
              @endif
              @endforeach
            </select>
          </div>
          <img src="/storage/{{$user_data->avatar}}" style="width: 100px;height: 100px;">
            <input type="file" name="avatar" placeholder="Аватар" value="asd" accept=".jpg, .jpeg, .png">
            <div class="account-form__buttons">
              <a href="/home">
                Назад
              </a>
              <input type="submit" name="" value="Сохранить">
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- END ACCOUNT -->

  </main>
  <!-- END MAIN -->
@endsection