@extends("layout")

@section('body')
  <!-- MAIN -->
  <main class="main">
    <!-- BREAD CRUMBS -->
    <ul class="bread-crumbs container">
      <li><a href="/">Главная</a></li>
      @if(!isset($stock))
      <li><a class="now-page" href="#">{{$main_data->name}}</a></li>
      @else
       <li><a href="#">{{$stock}}</a></li>
      @endif
    </ul>
    <!-- END BREAD CRUMBS -->
    @if(isset($action))
    <script type="text/javascript">
      window.action='{{$action}}';
    </script>
    @else
    <script type="text/javascript">
      window.action='/products/stock';
    </script>
    @endif
    <!-- ORGANIC CATALOG -->
    <div class="organic-catalog">
      <div class="container">
        <div class="catalog-section">
          <div class="title">
            @if(!isset($stock))
            <h3>{{$main_data->name}}</h3>
            <p style="word-wrap: break-word;">{{$main_data->description}}</p>
            @else
            <h3>{{$stock}}</h3>
            @endif
          </div>
        </div>

        <div class="catalog-wrapper">
          <div class="catalog_left-side">
            <div class="panel">
              <!-- first panel start here -->
              <div class="tab_panel sections">
                <div class="tab_heading">
                  <h4>Разделы</h4>
                  <span><i class="material-icons">keyboard_arrow_up</i></span>
                </div>
                <div class="tab_content">
                  <ul>
                    @foreach($sales as $sale)
                    <li class="sale_category" data-id="{{$sale->id}}">{{$sale->name}}</li>
                    @endforeach
                  </ul>
                </div>
              </div>  
            </div> <!-- main panel close here -->
          </div>

          <div class="catalog_right-side">
            <div id="catalog_products"> 
              <div class="catalog-products-wrapper" id="products_wrapper">
              </div>
            <div class="catalog-products-wrapper-more" id="moreProduct" style="display: none;">
              
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END ORGANIC CATALOG -->

    <!-- SUSCRIPTION BLOCK -->
    <div class="white-bg-wrapp">
      <div class="subscription-block container wow rotateInUpLeft" data-wow-duration="1s">
        <div class="subscription-absolute">
          <div class="subscription-content">
            <h3>Хотите быть в курсе всех новостей?</h3>
            <span>Подпишитесь на рассылку!</span>
            <div class="subscription-wrapp">
              <div class="sub-social">
                <h4>Следите за нами в соц сетях:</h4>
                <ul class="social-networks">
                   @foreach($links as $link)
                  <li><a href="{{$link->link}}"><img src="/images/{{$link->type}}.png"></a></li>
                  @endforeach
                </ul>
              </div>
              <div class="sub-row">
                <div class="sub-button-input">
                  <form action="/subscription" method="POST">
                    @csrf
                    <input type="email" name="email" placeholder="Введите Электронную почту">
                    <input type="submit" class="sub-btn" value="ПОДПИСАТЬСЯ">
                  </form>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END SUSCRIPTION BLOCK -->
  </main>
  <!-- END MAIN -->
@endsection