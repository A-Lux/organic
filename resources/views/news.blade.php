@extends("layout")

@section('body')
<main>
	  <div class="container new3">
    <div class="row">
      <div class="col-sm-12"> 
        <p class="title wow fadeInLeft"><a href="/news">Все новости</a>/{{$news->title}}</p>
        <span class="line wow fadeInLeft"></span>
        <div class="box wow fadeInLeft">
          <!-- <div class="box-video">
            <img src="images/play-button.png">
            <a align="center" href="#">Organic Market</a>
          </div> -->
          <div class="box-text" style="word-wrap: break-word;">
            {!!$news->body!!}
          </div>
          <div class="page">
            @if(session('hide')!='prev')
            <a id="down" class="wow fadeInLeft" href="/news-card/{{ $news->id }}?type=prev">Предыдущая новость</a>
            @endif

            @if(session('hide')!='next')
            <a id="up" class=" wow fadeInRight" href="/news-card/{{ $news->id }}?type=next">Следующая новость</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@endsection