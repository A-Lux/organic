@extends("layout")

@section('body')
<!-- MAIN -->
<main class="main">
  <!-- BREAD CRUMBS -->
  <ul class="bread-crumbs container">
    <li><a href="/">Главная</a></li>
    <li><a href="http://organic.ibeacon.kz/products/category/23">Продукты</a></li>
    <li><a href="/products/category/{{ $product->category->id }}">{{$product->category->name}}</a></li>
    <li><a href="/products/subcategory/{{ $product->sub_type }}">{{$product->subcategory}}</a></li>
    <li><a href="#">{{$product->name}}</a></li>
  </ul>
  <!-- END BREAD CRUMBS -->

  <!-- PRODUCT CARD -->
  <div class="product-card">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="product-card-title">
            <h3>{{$product->name}}</h3>
          </div>
        </div>
        <div class="col-sm-5 col-md-12 col-lg-5">
          <div class="product-card-wrapper">
            <div class="product-card-images">
              @if(!empty($product->images))

              <?php

              $images = json_decode($product->images);
              $max = count($images) - 1;
              if ($max > 4) {
                $max = 4;
              }
              ?>

              <?php $i = 0; ?>

              @if(count($images)>1)
              <div class="product-card__mini-items">

                @foreach($images as $image)
                <a class="products-cards__item sb" href="/storage/{{$image}}"><img src="/storage/{{$image}}" style="width: 71px;height: 83px"></a>
                <?php
                unset($images[$i]);
                $i++;
                if ($i == $max) {
                  break;
                }
                ?>
                @endforeach
              </div>
              <div class="product-card__big-img">
                <a class="sb" href="/storage/{{$images[$max]}}">
                  <img id="expandedImg" src="/storage/{{$images[$max]}}" style="width: 396px;height: 329px;">
                  <span class="zoom">
                    <p data-zoom="1">Увеличить <img src="/images/search.png" alt=""></p>
                  </span>
                </a>
              </div>
              @else
              <div class="product-card__big-img">
                <a class="sb" href="/storage/{{$images[0]}}">
                  <img id="expandedImg" src="/storage/{{$images[0]}}" style="width: 396px;height: 329px;">
                  <span class="zoom">
                    <p>Увеличить <img src="/images/search.png" alt=""></p>
                  </span>
                </a>
              </div>
              @endif
              @endif
            </div>
          </div>
          <div class="share">
            <p>Поделиться:</p>
            <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="https://yastatic.net/share2/share.js"></script>
            <div class="ya-share2" data-services="vkontakte,facebook,twitter,whatsapp,telegram"></div>
            <!-- <ul class="social-networks">
            @foreach($links as $link)
            <li><a href="{{$link->link}}"><img src="/images/{{$link->type}}.png"></a></li>
            @endforeach
          </ul> -->
          </div>
        </div>

        <div class="col-sm-7 col-md-12 col-lg-7">
          <div class="product-card-wrapper">
            <div class="product-card-info">
              <div class="product-card__brand mb-3">
                <p>Штрих-код:</p>
                <p>{{$product->shtrih}}</p>
              </div>
              <div class="product-card__brand">
                <p>Бренд:</p>
                <a href="#">{{$product->brand}}</a>
                <div class="brand-img">
                  <img src="/storage/{{$product->icon}}" style="width: 115px;height: 49px;">
                </div>
                <p>{{$product->country}}</p>
                <div class="country-img">
                  <img src="/storage/{{$product->flag}}" style="width: 30px;height: 16px;">
                </div>
              </div>
              <div class="star-line">
                <ul>
                  @for($i = 1; $i <= 5; $i++) @if($i <=$rating) <li style="color: #eeb900">&#9733;</li>
                    @else
                    <li style="color: #d4d4d4;">&#9733;</li>
                    @endif
                    @endfor
                </ul>
              </div>
              <div class="price">
                @if(!empty($product->s_price))
                <h6>{{$product->s_price}} тнг</h6>
                <p>
                  <span>{{$product->f_price}} тнг</span>
                  / {{$product->volume}} мл</p>
                @else
                <h6>{{$product->f_price}} тнг</h6>
                @endif
              </div>
              <div class="count">
                <div class="number-input operations-block">
                  <span onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="btn-operation"><img src="/images/minus.png" alt=""></span>
                  <input class="quantity input-text" min="1" name="count" value="1" type="number" id="card_count">
                  <span onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="btn-operation"><img src="/images/plus.png" alt=""></span>
                </div>
                @if(!empty($product->count) AND $product->count!=0)
                <p><img src="/images/ok-sm.png" alt="">В наличии</p>
                @else
                <p><img src="/images/ok-sm.png" alt="">Нет в наличии</p>
                @endif
              </div>
              @if(!empty($product->count) AND $product->count!=0)
                <div class="purchase-row">
                    <div class="basket-btn">
                    <button id="{{$product->id}}" onclick="addToCart(this.id,card_count.value)">В КОРЗИНУ</button>
                    </div>
                    <div class="one-click">
                    <a data-toggle="modal" data-target="#popUpWindow" href="#"><img src="/images/click.png" alt="">Заказ в
                        1 клик</a>
                    </div>
                    <!-- popUpWindow zakaz v 1 klik -->
                    <div class="modal fade" id="popUpWindow">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="container">
                            <div class="row">
                            <div class="col-sm-12">
                                <!-- header -->
                                <div class="modal-header">
                                <h5 class="modal-title">Заказать</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <!-- body -->
                                <div class="modal-header">
                                <form role="form" id="one_click_form">
                                    <div class="form-group">
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                    <input type="text" class="form-control" name="name" placeholder="Ваше имя" />
                                    <input type="text" class="form-control" name="phone" placeholder="Номер телефона">
                                    <textarea name="message" placeholder="Сообщение"></textarea>
                                    </div>
                                </form>
                                </div>
                                <!-- footer -->
                                <div class="modal-footer">
                                <a href="#" id="one_click_order">Заказать</a>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <!-- popUpWindow zakaz v 1 klik -->
                    <div class="add-btns">
                    <a href="#"><img src="/images/libra-btn.png" alt="" onclick="add_comparison('{{$product->id}}')"></a>
                    <a href="#"><img src="/images/like-btn-green.png" alt="" id="{{$product->id}}" onclick="add_like(this.id)"></a>
                    </div>

                </div>
              @endif
              <div class="product-tabs">
                <div class="item active-product">
                  <a href="#tab1" onclick="e.preventDefault();">Состав</a>
                </div>
                <div class="item">
                  <a href="#tab2" onclick="e.preventDefault();">Способ использования</a>
                </div>
              </div>
              <div class="product-tabs-content">
                <div class="product-tab-item product-tab-item-active" id="tab1">
                  {{$product->composition}}
                </div>
                <div class="product-tab-item" id="tab2">
                  {{$product->mode}}
                </div>
              </div>
              <!-- <div class="product-description">
                  <h5>Описание товара:</h5>
                  <p>{!! $desc1 !!} </p>
                  <div class="show-more">
                    <p data-toggle="collapse" data-tab="1" data-target="#demo">Узнать больше <img src="/images/collapse-arrow.png" alt=""></p>
                  </div>
                  <div id="demo" class="collapse product-description">
                    <p>{!!$desc2!!}</p>
                  </div>
                </div> -->
            </div>
          </div>
        </div>
        <div class="col-xl-12">
          <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Преимущества </a>
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Оплата</a>
              <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Доставка
              </a>
            </div>
          </nav>
          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
              <div class="row">
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item">
                    <div class="icon">
                      <img src="/images/tab-icon1.png" alt="">
                    </div>
                    <div class="text-block">
                      <h6>Оперативно</h6>
                      <p>Товар на складе,доставим <br>за минимальный срок </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 ">
                  <div class="tab-item">
                    <div class="icon">
                      <img src="/images/tab-icon2.png" alt="">
                    </div>
                    <div class="text-block">
                      <h6>Богатый ассортимент</h6>
                      <p>Поступления каждую неделю, <br>у нас самая свежая продукция</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item">
                    <div class="icon">
                      <img src="/images/tab-icon3.png" alt="">
                    </div>
                    <div class="text-block">
                      <h6>Приятный сюрприз</h6>
                      <p>Положим подарочек в каждый заказ </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="tab-item">
                    <div class="icon">
                      <img src="/images/tab-icon4.png" alt="">
                    </div>
                    <div class="text-block">
                      <h6>Вкусная цена</h6>
                      <p>Поступления каждую неделю,у нас самая свежая продукция</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab" style="padding: 1.5rem 3rem 2rem 2rem !important;">
              <div class="row">
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/dostavka1.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>На сайте картой VISA/Mastercard</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/dostavka2.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Наличными курьеру</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/dostavka3.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Оплата при самовывозе</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/dostavka4.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>На карту “Каспи”</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
              <div class="row">
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/advantage1.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Бесплатная доставка по Алматы при заказе от 7 000 тнг </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/advantage2.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Бесплатная доставка по РК при заказе от 10 000 тнг</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/advantage3.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Доставка до двери</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="tab-item" style="margin-bottom:0;">
                    <div class="icon">
                      <img src="/images/advantage4.png" alt="" style="width: 6rem;">
                    </div>
                    <div class="text-block" style="display:flex; align-items: center;">
                      <p>Минимальные сроки (по Алматы 1раб день, по РК 2-4 раб дня)</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-12 col-lg-12">


            <!-- ОТЗЫВЫ!НАДО ДОБАВИТЬ СТИЛИ -->
            @foreach($comments as $comment)
            <div class="main-box__review">
              <img src="{{Voyager::image($comment->avatar)}}" alt="..." style="margin-top: 7px;">
              <div class="review__info">
                <p class="name">{{$comment->name}}</p>
                <p class="reviews__text">{{$comment->text}}</p>
                <p class="date">{{$comment->created_at}}</p>
              </div>
            </div>
            @endforeach
            <!-- //// -->

            <div class="add-review">
              <button onclick="diplay_hide('#block_id');return false;"><img src="/images/quote-icon.png" alt="">Добавить
                отзыв</button>
            </div>
            <div class="container comment" id="block_id" style="display: none;">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form__box">
                    <p>Оставить отзыв</p>
                    <div class="rating">
                      <div class="rating-item active" data-rate="1">&#9733;</div>
                      <div class="rating-item" data-rate="2">&#9733;</div>
                      <div class="rating-item" data-rate="3">&#9733;</div>
                      <div class="rating-item" data-rate="4">&#9733;</div>
                      <div class="rating-item" data-rate="5">&#9733;</div>
                    </div>
                    <form action="/product-card/comment/{{$product->id}}" method="POST">
                      @csrf
                      <input type="text" name="rating" required style="width: 1px; height:1px; padding: 0" id="rate_field">
                      <textarea type="textarea" placeholder="Сообщение" style="height: 150px;" name="text" required></textarea>
                      <input type="submit" class="button" name="" value="Отправить">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-4 col-md-12 col-lg-12 col-xl-12 mt-2">
          <div class="advised-title">
            <h5>Товары, схожие по результату действия:</h5>
          </div>
          <a
            href="#"
            class="product-arrow__left"
            style="position:absolute; top: 10rem; left: -3rem;">
              <img src="{{ asset('/images/slide-prev.png') }}" />
          </a>
          <div id="products-wrapper" class="owl-carousel owl-theme container wow zoomIn">
            <div class="advised-item">
              <a href="/product-card/{{$product->id}}" class='products-wrapper-link'>
                <div class="icon">
                  <img src="images/" alt="" style="width: 100%;height: 100%">
                </div>
                <p>Продукт1</p>
              </a>
            </div>
            <div class="advised-item">
              <a href="/product-card/{{$product->id}}" class='products-wrapper-link'>
                <div class="icon">
                  <img src="images/" alt="" style="width: 100%;height: 100%">
                </div>
                <p>Продукт1</p>
              </a>
            </div>
            <div class="advised-item">
              <a href="/product-card/{{$product->id}}" class='products-wrapper-link'>
                <div class="icon">
                  <img src="images/company_img.jpg" alt="" style="width: 100%;height: 100%">
                </div>
                <p>Продукт1</p>
              </a>
            </div>
            <div class="advised-item">
              <a href="/product-card/{{$product->id}}" class='products-wrapper-link'>
                <div class="icon">
                  <img src="images/" alt="" style="width: 100%;height: 100%">
                </div>
                <p>Продукт1</p>
              </a>
            </div>
            <div class="advised-item">
              <a href="/product-card/{{$product->id}}" class='products-wrapper-link'>
                <div class="icon">
                  <img src="images/" alt="" style="width: 100%;height: 100%">
                </div>
                <p>Продукт1</p>
              </a>
            </div>
            <div class="advised-item">
              <a href="/product-card/{{$product->id}}" class='products-wrapper-link'>
                <div class="icon">
                  <img src="images/" alt="" style="width: 100%;height: 100%">
                </div>
                <p>Продукт1</p>
              </a>
            </div>
          </div>
          <a
            href="#"
            class="product-arrow__right"
            style="position:absolute; top: 10rem; right: -3rem;">
              <img src="{{ asset('/images/slide-nxt.png') }}" />
          </a>
          <!-- @foreach($similar_products as $product)
          <div class="advised-item">
            <a href="/product-card/{{$product->id}}">
              <div class="icon">
                <img src="{{Voyager::image($product->cover)}}" alt="" style="width: 100%;height: 100%">
              </div>
              <p>{{$product->name}}</p>
            </a>
          </div>
          @endforeach -->
          <!-- <div class="subscribe">
            <form action="/home/subscription" method="POST">
              @csrf
              <h6>Хотите быть в курсе <br> всех новостей?</h6>
              <p>Подпишитесь на рассылку!</p>
              <input type="email" name="email" placeholder="Введите Электронную почту" required>
              <input type="submit" value="ПОДПИСАТЬСЯ">
            </form>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <!-- END PRODUCT CARD -->
</main>


@if(Session::has('send'))
<script type="text/javascript">
  sweetalert('success', 'Спасибо,ваш отзыв находится в обработке!', 3000);
</script>
@endif
<!-- END MAIN -->
@endsection
