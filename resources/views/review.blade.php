@extends("layout")

@section('body')
<div class="container review">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <ul class="all-review__box">
        @if(!empty($comments))
        @foreach($comments as $comment)
        <li>
          <div class="main-box__review">
            <img src="/storage/{{$profile->avatar}}" alt="...">
            <div class="main__review-info">
              <div class="review__info">
                <p class="name">{{$profile->name}}</p>
                <p class="reviews__text">{{$comment->text}}</p>
                <p class="date">{{$comment->created_at}}</p>
              </div>
              <div class="review-product-info">
                <a href="/product-card/{{$comment->product_id}}">{{$comment->product_name}}</a>
              </div>
            </div>
          </div>
        </li>
        @endforeach
        @endif
      </ul>
      <nav aria-label="Page navigation example">
        <?php echo $comments->render(); ?>
      </nav>
    </div>
  </div>
</div>
<!-- END MAIN -->
@endsection