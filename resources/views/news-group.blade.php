@extends("layout")

@section('body')
  <!-- group news -->
  <div class="container card-news">
    <h2>Блог</h2>
    <div class="row">
      @foreach ($news as $new)
        <div class="card col-lg-3 col-md-4 col-sm-12">
          <img src="/storage/{{$new->cover}}" alt="...">
          <div class="card-body">
            <p class="date">{{$new->created_at}}</p>
            <div class="card-text">
              <p>{{$new->title}}</p>
              <a href="/news-card/{{$new->id}}">Узнать больше</a>
            </div>
          </div>
        </div>
        @endforeach
    </div> 
  </div>
  <!-- end group news -->
@endsection