@extends("layout")

@section('body')
  <div class="container company">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="company__head pay-page__main-title" style="display: flex; justify-content: center;">
          <p class="company__title">Всего к оплте: {{$order->sum}} тг.</p>
        </div>
        <div class="company__body">
          <div class="pay-page-input-wrapper">
            <div class="col-lg-4 col-md-6 col-sm-12">
              <form>
                <div class="form-group">
                  <label for="Input1">Фамилия</label>
                  <input type="text" placeholder="Фамилия" class="form-control" id="Input1" pattern="[A-Za-zА-Яа-яЁё]{1,}" name="surname" value="{{ Auth::user()->surname }}" required>
                </div>
                <div class="form-group">
                  <label for="Input2">Имя</label>
                  <input type="text" placeholder="Имя" class="form-control" id="Input2" pattern="[A-Za-zА-Яа-яЁё]{1,}" name="name"  value="{{ Auth::user()->name }}"  required>
                </div>
                <div class="form-group">
                  <label for="Input3">Отчество</label>
                  <input type="text" placeholder="Отчество" class="form-control" id="Input3" pattern="[A-Za-zА-Яа-яЁё]{1,}" name="fathername" required>
                </div>
                <div class="form-group">
                  <label for="Input4">ИИН</label>
                  <input type="tel" placeholder="ИИН" name="mobile" maxlength="12" pattern="[0-9]{12}">
                </div>
                <div class="form-group">
                  <label for="Input5">Номер телефона</label>
                  <input type="tel" placeholder="Номер телефона" name="mobile" maxlength="16" required>
                </div>
                <div class="form-group">
                  <label for="Input6">Адрес</label>
                  <input type="text" placeholder="Адрес"  name="name" required>
                </div>
              </form>
            </div>
          </div>
          <div class="pay-page__main-wrapper" style="word-wrap: break-word; ">
			      <form name="SendOrder" method="post" action="https://epay.kkb.kz/jsp/process/logon.jsp">
				      <input type="hidden" name="Signed_Order_B64" value="{{$pay_content}}">
				      <input type="hidden" name="email" size=50 maxlength=50 value="">
				      <input type="hidden" name="Language" value="rus">
				      <input type="hidden" name="BackLink" value="{{url('/basket/pay-result')}}">
				      <input type="hidden" name="FailurePostLink" value="{{url('/basket/pay-result')}}">
				      <input type="hidden" name="PostLink" value="{{url('/basket/pay-result')}}">
				      <input type="submit" id="pay" name="GotoPay" value="" style="display: none;">
				      <button  type="button" class="btn" onclick="document.getElementById('pay').click()">Оплатить онлайн</button>
            </form>
            <button  type="button" style="margin: 0 3rem;" class="btn pay-samov">Самовывоз</button>
            <button  type="button" class="btn pay-courier">Оплата курьеру</button>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection