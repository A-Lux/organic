@extends("layout")

@section('body')

    <div class="modal-bonus-info">
      <div class="close-modal-btn">
        &#10006;
      </div>
      <h5>Информация</h5>
      <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odit, tempora quas atque mollitia quasi vero dolorem
        labore consequatur deserunt dignissimos corrupti alias optio repellendus pariatur laborum blanditiis ab. Eum,
        alias?</p>
    </div>

    <!-- END MODAL -->

  <!-- MAIN -->
  <main class="main">
    <!-- BREAD CRUMBS -->
    <ul class="bread-crumbs container">
      <li><a href="/home">Личный кабинет</a></li>
      <li><a href="#">Бонусные баллы</a></li>
    </ul>
    <!-- END BREAD CRUMBS -->
     <!-- BONUS SECTION -->
     <section class="bonus-section">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
              <div class="bonus-section__title">
                <p>Бонусы</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
              <div class="bonus-section__title-points">
                <p>Ваши бонусы</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
              <div class="bonus-section__title-score">
                <div class="col-lg-6 col-md-6 col-sm-12 px-0">
                  <p class="bonus-section__title-score_1">На вашем счете</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 px-0">
                  <p class="bonus-section__title-score_2">Ожидают подтверждения</p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
              <div class="bonus-section__title-bonus">
                <div class="col-lg-6 col-md-6 col-sm-12 px-0">
                  <div class="bonus-section__title-bonus_1">
                    <ul>
                      <li>
                        <img src="{{ asset('images/bonus-img.png') }}">
                      </li>
                      <li>
                        <img src="{{ asset('images/bonus-img.png') }}">
                      </li>
                    </ul>
                    <p>{{ $userBonus }} бонуса(-ов)</p>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 px-0">
                  <div class="bonus-section__title-bonus_2">
                    <ul>
                      <li class="wait-image">
                        <img src="{{ asset('images/bonus-img-1.png') }}">
                      </li>
                      <li class="done-image" style="display: none;">
                        <img src="{{ asset('images/bonus-img.png') }}">
                      </li>
                    </ul>
                    <p class="wait">{{ $bonus_await }} бонуса(-ов)</p>
                    <!-- добавить класс done и удалить wait, когда прошло подтверждение (убрать opacity) -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
              <div class="bonus-section__title-info">
                <div class="col-lg-9 col-md-9 col-sm-12 px-0">
                  <div class="bonus-section__title-info_1">
                    <ul>
                      <li>
                        <p>Возвращаем % с каждой покупкой</p>
                      </li>
                      <li>
                        <a href="#" class="bonus-btn-more-info">?</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 px-0">
                  <div class="bonus-section__title-info_2">
                    <ul>
                      <li>
                        <p>1 балл</p>
                      </li>
                      <li>
                        <p>-</p>
                      </li>
                      <li>
                        <p>1 тг</p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="bonus-section__line">
                <ul>
                  <li data-img="shape1" data-active="1">
                    <span>10 000 тг</span>
                    <a href="#" style="background-image: url({{asset('images/shape1.png')}});">
                      3%
                    </a>
                  </li>
                  <li data-img="shape2" data-active="1"> 
                    <span>20 000 тг</span>
                    <a href="#" style="background-image: url({{ asset('images/shape2.png') }});">
                      5%
                    </a>
                  </li>
                  <li data-img="shape3" data-active="1">
                    <span>50 000 тг</span>
                    <a href="#" style="background-image: url({{ asset('images/shape3.png') }});">
                      10%
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END BONUS SECTION -->
  
      <!-- MODAL -->
      <div class="overlay">
  
      </div>
      <div class="modal-bonus-info">
        <div class="close-modal-btn">
          &#10006;
        </div>
        <h5>Информация</h5>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Odit, tempora quas atque mollitia quasi vero dolorem
          labore consequatur deserunt dignissimos corrupti alias optio repellendus pariatur laborum blanditiis ab. Eum,
          alias?</p>
      </div>
  
      <!-- END MODAL -->
  
  </main>
  <!-- END MAIN -->
@endsection