@extends("layout")

@section('body')
<main class="main">
    <!-- BANNER -->
    <div id="carouselOne" class="organic-banner owl-carousel owl-theme container wow zoomIn">
      @foreach($banners as $banner)
      <div class="banner-item">
        <img src="/storage/{{$banner->image}}">
      </div>
      @endforeach
    </div>
    <!-- END BANNER -->
    <!-- ADVANTAGES -->
    <div class="advantages container wow fadeInUp" data-wow-duration="1.5s">
      <ul>
        <li>
          <img src="images/advantage1.png">
          <p>100%</p>
          <span>Натуральная косметика</span>
        </li>
        <li>
          <img src="images/advantage2.png">
          <p>Выгодная цена</p>
          <span>Прямые поставки от производителя</span>
        </li>
        <li>
          <img src="images/advantage3.png">
          <p>Бесплатная</p>
          <span>Быстрая доставка</span>
        </li>
        <li>
          <img src="images/advantage4.png">
          <p>Скидки</p>

          <span>Постоянным клиентам</span>
        </li>
        <li>
          <img src="images/advantage5.png">
          <p>С гарантией</p>
          <span>Лёгкого возврата в течении 14 дней</span>
        </li>
      </ul>
    </div>
    <!-- END ADVANTAGES -->

    <!-- OFFERS -->
    <div class="offers">
      <div class="title">
        <h3>Предложение дня</h3>
      </div>
      <div id="carouselExampleControls" class="carousel slide wow zoomIn" data-wow-duration="1.5s" data-ride="carousel">
        <div class="carousel-inner">
		@foreach(collect($day_products)->chunk(4) as $day_products)
			<div class="carousel-item {{ $loop->first ? 'active' : '' }}">

				<div class="container">
				<div class="offers-slide-wrapp">
				@foreach($day_products as $day_product)
				<div class="offers-slide-item">
                  <div class="offers-item-img" style="padding: 10px;">
                    <a href="/product-card/{{$day_product->id}}"><img src="/storage/{{$day_product->cover}}"></a>
                  </div>
                  <div class="offers-item-content">
                    <div class="product-name">
                      <a href="/product-card/{{$day_product->id}}">{{$day_product->name}}/</a>
                      <span>{{$day_product->volume}} мл</span>
                    </div>
                <!--    <div class="product-about">
                     <span>{{$day_product->description}}</span>
                    </div> -->
                    <div class="product-mark-price">

                        @php
                        $rating = \App\Product::calculateRating($day_product->id);
                        @endphp
                        <div class="star-line">
                          <ul>
                            @for($i = 1; $i <= 5; $i++)
                            @if($i <= $rating)
                            <li style="color: #eeb900">&#9733;</li>
                            @else
                            <li style="color: #d4d4d4;">&#9733;</li>
                            @endif
                            @endfor
                          </ul>
                        </div>
                      <div class="price">
                        <span>{{$day_product->f_price}} тнг</span>
                      </div>
                    </div>
                    <div class="product-button">
                      <div class="product-buttons-row">
                        <button class="btn-like" name="btn-like" id="{{$day_product->id}}"  onclick="add_like(this.id)">
                          <img src="images/like-btn.png">
                        </button>
                        <button class="btn-libra" name="btn-libra" onclick="add_comparison('{{$day_product->id}}')">
                          <img src="images/libra-btn.png">
                        </button>
                      </div>
                      <button class="btn-basket" name="btn-basket"     id="{{$day_product->id}}"   onclick="addToCart(this.id)">
                        В КОРЗИНУ
                      </button>

                    </div>

                  </div>
                </div>
				@endforeach
				</div></div>
			</div>
		@endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <button>
            <img src="/images/slide-left.png">
          </button>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <button>
            <img src="/images/slide-left.png">
          </button>
        </a>
      </div>
      <div class="offer-stocks">
        <div class="container">
          <ul class="tabs">
        		<li class="tab-link current" data-tab="tab-1">
              <img src="images/bakset.png">
              <span>Распродажа</span>
            </li>
        		<li class="tab-link" data-tab="tab-2">
              <img src="images/news.png">
              <span>Новинки</span>
            </li>
        		<li class="tab-link" data-tab="tab-3">
              <img src="images/favorite.png">
              <span>Хиты продаж</span>
            </li>
        	</ul>

        	<div id="tab-1" class="tab-content current wow fadeInUp">
		{{-- @if($sale_products) --}}
           @foreach ($sale_products as $sale_product )
            <div class="offers-slide-item">
              <div class="offers-item-img" style="padding-right: 10px;">
                <a href="/product-card/{{$sale_product->id}}"><img src="/storage/{{$sale_product->cover}}"></a>
              </div>
              <div class="offers-item-content">
                <div class="product-name">
                  <a href="/product-card/{{$sale_product->id}}">{{$sale_product->name}} /</a>
                  <span>{{$sale_product->volume}} мл</span>
                </div>
              <!--  <div class="product-about">
                  <span>{{$sale_product->description}}</span>
                </div> -->
                <div class="product-mark-price">
                  <div class="price">
                    <span>{{$sale_product->f_price}} тнг</span>
                  </div>
                </div>
				  <div class="d-flex">
					<div class="product-button">
					  <button class="btn-basket" name="btn-basket"     id="{{$sale_product->id}}"   onclick="addToCart(this.id)">
							В КОРЗИНУ
						  </button>
					</div>
					  <div class="add-btns d-flex">
						  <a href="javascript:void()"><img src="/images/libra-btn.png" alt="" onclick="add_comparison('{{$sale_product->id}}')"></a>
						  <a href="javascript:void()"><img src="/images/like-btn-green.png" alt="" id="{{$sale_product->id}}"
										   onclick="add_like(this.id)"></a>
				  	</div>
				  </div>
              </div>
            </div>
            @endforeach
				{{-- @endif --}}
        	</div>
        	<div id="tab-2" class="tab-content wow fadeInUp">
            @foreach ($new_products as $new_product )
            <div class="offers-slide-item">
              <div class="offers-item-img" style="padding-right: 10px;">
                <a href="/product-card/{{$new_product->id}}"><img src="/storage/{{$new_product->cover}}"></a>
              </div>
              <div class="offers-item-content">
                <div class="product-name">
                  <a href="/product-card/{{$new_product->id}}">{{$new_product->name}} /</a>
                  <span>{{$new_product->volume}} мл</span>
                </div>
              <!--  <div class="product-about">
                  <span>{{$new_product->description}}</span>
                </div> -->
                <div class="product-mark-price">
                  <div class="price">
                    <span>{{$new_product->f_price}} тнг</span>
                  </div>
                </div>
                <div class="product-button">
                  <button class="btn-basket" name="btn-basket"     id="{{$new_product->id}}"   onclick="addToCart(this.id)">
                        В КОРЗИНУ
                      </button>
                </div>
              </div>
            </div>
            @endforeach
        	</div>
        	<div id="tab-3" class="tab-content wow fadeInUp">
            @foreach ($hit_products as $hit_product)
            <div class="offers-slide-item">
              <div class="offers-item-img" style="padding-right: 10px;">
                <a href="/product-card/{{$hit_product->id}}"><img src="/storage/{{$hit_product->cover}}"></a>
              </div>
              <div class="offers-item-content">
                <div class="product-name">
                  <a href="/product-card/{{$hit_product->id}}">{{$hit_product->name}} /</a>
                  <span>{{$hit_product->volume}} мл</span>
                </div>
               <!-- <div class="product-about">
                  <span>{{$hit_product->description}}</span>
                </div> -->
                <div class="product-mark-price">
                  <div class="price">
                    <span>{{$hit_product->f_price}} тнг</span>
                  </div>
                </div>
                <div class="product-button">
                  <button class="btn-basket" name="btn-basket"     id="{{$hit_product->id}}"   onclick="addToCart(this.id)">
                        В КОРЗИНУ
                      </button>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <!-- END OFFERS -->

    <!-- WANT question-block -->
    <div class="question-block container">
      <div class="question-absolute wow rotateInUpLeft" data-wow-duration="1.5s">
        <h3>Хотите задать вопрос?</h3>
        <p>Позвоните нам на номер  +7(747) 257 - 6949<br>
или заполните форму и мы перезвоним в течении трёх минут!</p>
        <div class="question-inputs">
          <form action="/home/question" method="get">
            <input type="text" placeholder="Ваше имя" pattern="[A-Za-zА-Яа-яЁё]{1,}" name='name' required>
            <input type="tel" placeholder="Номер телефона: 8-777-777-77-77" name='mobile' pattern="[0-9]{1}-[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}" required style="width: 290px;" maxlength="16">
            <input type="submit" class="btn-form" value="отправить">
          </form>
        </div>
      </div>
    </div>
    <!-- END WANT question-block -->


    <!-- REVIEWS AND NEWS -->
    <div class="reviews-and-news">
      <div class="container">
        <div class="title">
          <h3>Отзывы и новости</h3>
        </div>
        <div class="full-side">
          <div class="left-side" data-wow-duration="1.5s">
            @foreach($news as $new)
            <div class="news-item">
              <div class="news-img">
                <img src="{{Voyager::image($new->cover)}}">
              </div>
              <h4>{{$new->title}}</h4>
              <!-- <div style="word-wrap: break-word !important;">{!! substr($new->body,0,79) !!} ...</div> -->
              <a href="/news-card/{{ $new->id }}">ЧИТАТЬ ДАЛЕЕ</a>
            </div>
            @endforeach
          </div>
          <div class="right-side" data-wow-duration="1.5s">
            <ul class="reviews-imgs">
            @php
            $i=1;
            @endphp
              @foreach($comments as $comment)
              @if($i==2)
              <li class="tab-link active" review-tab="review-{{ $comment->id }}">
              @else
              <li class="tab-link" review-tab="review-{{ $comment->id }}">
              @endif
              @php
              $i++;
              @endphp
                <img src="{{ Voyager::image($comment->avatar) }}">
              </li>
              @endforeach
            </ul>
            @php
            $i=1;
            @endphp
            @foreach($comments as $comment)
            @if($i==2)
            <div id="review-{{ $comment->id }}" class="review-content active">
            @else
            <div id="review-{{ $comment->id }}" class="review-content">
            @endif
             @php
              $i++;
            @endphp
              <p>{{ substr($comment->text,0,150) }}</p>
              <span>{{$comment->name}}</span>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <!-- END REVIEWS AND NEWS -->


    <!-- SUSCRIPTION BLOCK -->
    <div class="white-bg-wrapp">
      <div class="subscription-block container" data-wow-duration="1s">
        <div class="subscription-absolute">
          <div class="subscription-content">
            <h3>Хотите быть в курсе всех новостей?</h3>
            <span>Подпишитесь на рассылку!</span>
            <div class="subscription-wrapp">
              <div class="sub-social">
                <h4>Следите за нами в соц сетях:</h4>
                <ul class="social-networks">
                   @foreach($links as $link)
                  <li><a href="{{$link->link}}"><img src="images/{{$link->type}}.png"></a></li>
                  @endforeach
                </ul>
              </div>
              <div class="sub-row">
                <div class="sub-button-input">
                  <form action="/home/subscription" method="POST">
                    @csrf
                    <input type="email" name="email" placeholder="Введите Электронную почту" required>
                    <input type="submit" class="sub-btn" value="ПОДПИСАТЬСЯ">
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END SUSCRIPTION BLOCK -->
  </main>
  <!-- END MAIN -->
@endsection
