@extends("layout")

@section('body')
  <!-- MAIN -->
  

<style>
	.catalog_right-side .catalog-filter .filter-wrap.showAmount li::after {
		display: none;
	}
</style>

  <main class="main">
    <!-- BREAD CRUMBS -->
    <ul class="bread-crumbs container">
      <li><a href="/">Главная</a></li>
      @if(!isset($stock))
      <li><a class="now-page" href="#">{{$main_data->name}}</a></li>
      @else
       <li><a href="#">{{$stock}}</a></li>
      @endif
    </ul>
    <!-- END BREAD CRUMBS -->
    @if(isset($action))
    <script type="text/javascript">
      window.action='{{$action}}';
    </script>
    @else
    <script type="text/javascript">
      window.action='/products/stock';
    </script>
    @endif
    <!-- ORGANIC CATALOG -->
    <div class="organic-catalog">
      <div class="container">
        <div class="catalog-section">
          <div class="title">
            @if(!isset($stock))
            <h3>{{$main_data->name}}</h3>
            <p style="word-wrap: break-word;">{{ $main_data->description}}</p>
            @else
            <h3>{{$stock}}</h3>
            @endif
          </div>
        </div>

        <div class="catalog-wrapper">
          <div class="catalog_left-side">
            <div class="panel">
              <!-- first panel start here -->
              <div class="tab_panel sections">
                <div class="tab_heading">
                  <h4>Разделы</h4>
                  <span><i class="material-icons">keyboard_arrow_up</i></span>
                </div>
                <div class="tab_content">
                  <ul>
                    @foreach($section_categories as $category)
                    @if(!empty($subcategories[$category->id]))
                    <li id="toggle-btn" data-toggle="collapse" data-target="#toggle-example{{$loop->iteration}}">
						<span>{{$category->name}}</span>
                     
                    </li>
					   <div id="toggle-example{{$loop->iteration}}" class="collapse in">
                        <ul>
                          @foreach($subcategories[$category->id] as $subcategory)
                          <li><a class="prevent_collapse" onclick="catalog_filter('/products/subcategory/{{$subcategory->id}}',0)"><span>{{$subcategory->name}}</span></a></li>
                          @endforeach
                        </ul>
                      </div>
                    @else
                    <li><a href="/products/category/{{$category->id}}"><span>{{$category->name}}</span></a></li>
                    @endif
                    @endforeach
                  </ul>
                </div>
              </div>

              <!-- second panel start here -->
              <div class="tab_panel brands">
                <div class="tab_heading">
                  <h4>Бренды</h4>
                  <span><i class="material-icons">keyboard_arrow_down</i></span>
                </div>
                <div class="tab_content">
                  <ul class="checkbox-list">
                    @foreach($brands as $brand)
                    <li>
                      <div class="round">
                        <input type="checkbox" id="{{ $brand->id }}" onclick="catalog_filter(0,'{{ $brand->id }}')"/>
                        <label for="{{ $brand->id }}"></label>
                      </div>
                      <p>{{$brand->name}}</p>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>

            <!-- three panel start here -->
            <div class="tab_panel prices">
              <div class="tab_heading">
                <h4>Цена (тнг.)</h4>
                <span><i class="material-icons">keyboard_arrow_down</i></span>
              </div>
              <div class="tab_content">
                <div slider id="slider-distance">
                  <!-- <div>
                    <div inverse-left style="width:70%;"></div>
                    <div inverse-right style="width:70%;"></div>
                    <div range style="left:30%;right:40%;"></div>
                    <span thumb style="left:30%;"></span>
                    <span thumb style="left:60%;"></span>
                    <div sign style="left:30%;">
                      <span id="value">30</span>
                    </div>
                    <div sign style="left:60%;">
                      <span id="value">60</span>
                    </div>
                  </div> -->
                  <!-- <input type="range" tabindex="0" value="30" max="100" min="0" step="1" oninput="
                  this.value=Math.min(this.value,this.parentNode.childNodes[5].value-1);
                  var value=(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.value)-(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.min);
                  var children = this.parentNode.childNodes[1].childNodes;
                  children[1].style.width=value+'%';
                  children[5].style.left=value+'%';
                  children[7].style.left=value+'%';children[11].style.left=value+'%';
                  children[11].childNodes[1].innerHTML=this.value;" />

                  <input type="range" tabindex="0" value="60" max="100" min="0" step="1" oninput="
                  this.value=Math.max(this.value,this.parentNode.childNodes[3].value-(-1));
                  var value=(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.value)-(100/(parseInt(this.max)-parseInt(this.min)))*parseInt(this.min);
                  var children = this.parentNode.childNodes[1].childNodes;
                  children[3].style.width=(100-value)+'%';
                  children[5].style.right=(100-value)+'%';
                  children[9].style.left=value+'%';children[13].style.left=value+'%';
                  children[13].childNodes[1].innerHTML=this.value;" /> -->
                </div>
                <div class="slider-price">
                  <div class="slider-price-input">
                    <input type="text" id="amount_1">
                    <input type="text" id="amount_2">
                  </div>
                    <div class="slide" id="slider-range"></div>
                </div>
                <div class="prices-buttons">
                  <button type="button" name="price-btn" onclick="catalog_filter(0,0)">
                    Применить
                  </button>
                  <button type="button" name="default-btn" onclick="sbros()">
                    Сбросить
                  </button>
                </div>
              </div>
            </div>
            </div> <!-- main panel close here -->
          </div>

          <div class="catalog_right-side">
            <div class="catalog-filter">
              <p>Сортировать по:</p>
              <ul class="filter-wrap">
                <li onclick="catalog_sort(this.value,this.id)" id="seen" value="0">Популярности</li>
                <li onclick="catalog_sort(this.value,this.id)" id="final_price" value="0">Цене</li>
                <li onclick="catalog_sort(this.value,this.id)" id="name" value="0">Названию</li>
                <li onclick="catalog_sort(this.value,this.id)" id="in_stock" value="0">В наличии</li>
                <li onclick="catalog_sort(this.value,this.id)" id="discount" value="0">Скидки</li>
                <!-- <li onclick="catalog_sort(this.value,this.id)" id="skin_type" value="0">Типу кожи</li> -->
              </ul>
            </div>
			<div class="catalog_right-side">
            {{-- <div class="catalog-filter">
              <p>Показывать по:</p>
              <ul class="filter-wrap showAmount">
                <li class="@if(Session::get('itemsAmount') == '50' || is_null(Session::get('itemsAmount'))) active @endif" onclick="catalog_show(this.value,this.id)" id="amount50" value="50">50</li>
                <li class="@if(Session::get('itemsAmount') == '100') active @endif" onclick="catalog_show(this.value,this.id)" id="amount100" value="100">100</li>
                <li class="@if(Session::get('itemsAmount') == '200') active @endif" onclick="catalog_show(this.value,this.id)" id="amount200" value="200">200</li>
              </ul>
            </div> --}}
              @php
              $i=1;
              @endphp
            <div id="catalog_products"> 

              <!-- ЕСЛИ НЕ ТОВАРОВ В ДАННОЙ КАТЕГОРИИ -->
                @if(empty($products))
                <h3 class="no-products-title">В данной категории нет товаров!</h3>
                @endif


                        <div class="catalog-products-wrapper">
                        @foreach($products as $key=>$product)
                        <div class="catalog-product-item">
                          <div class="catalog-img">
                            <a href="/product-card/{{$product->id}}"><img src="/storage/{{$product->cover}}"></a>
                            @if(!empty($product->discount))
                            <span class="catalog-stock">-{{$product->discount}}</span>
                            @endif
                          </div>
                          <h4><a href="/product-card/{{$product->id}}">{{$product->name}}</a> / {{$product->volume}} мл.</h4>
                          <p>{{ substr($product->text_cover, 0, strrpos(substr($product->text_cover, 0, 50), ' ')) }}</p>
                           @if(!empty($product->s_price))
							@if($product->s_price != $product->f_price)
                          <div class="price">{{$product->s_price}} тнг
                            <span>
                            	{{$product->f_price}} тнг
                            </span>
                          </div>
							@else
								<div class="price">{{$product->f_price}} тнг </div>
							@endif
                          @else
                          <div class="price">{{$product->f_price}} тнг </div>
                          @endif
							@if(!empty($product->count) AND $product->count!=0)
							<p><img src="/images/ok-sm.png" alt="">В наличии</p>
							@else
							<p><img src="/images/notok-sm.png" alt="">Нет в наличии</p>
							@endif
                          @php 
                          $rating = \App\Product::calculateRating($product->id);
                          @endphp
                          <div class="star-line">
                            <ul>
                              @for($i = 1; $i <= 5; $i++)
                              @if($i <= $rating)
                              <li style="color: #eeb900">&#9733;</li>
                              @else
                              <li style="color: #d4d4d4;">&#9733;</li>
                              @endif
                              @endfor
                            </ul>
                          </div>
							<div class="correction__wrapper">
								@if(!empty($product->count) AND $product->count!=0)
							  <button class="btn-basket" name="btn-basket"     id="{{$product->id}}"   onclick="addToCart(this.id)">
								В КОРЗИНУ
							  </button>
							@endif
								<a href="/likes"><img src="/images/heart.png"></a>
							</div>
                        </div>

                        @php

                        unset($products[$key]);
                        if($i==9){
                        break;
                        }
                        else{
                        $i++;
                        }
                        
                        @endphp
                        
                        @endforeach
                      </div>
        <!-- БЛОК ПОКАЗАТЬ ЕЩЕ -->
         @if(!empty($products))
        <div class="catalog-products-wrapper-more" id="moreProduct" style="display: none;">
          @foreach($products as $product)
          <div class="catalog-product-item">
                          <div class="catalog-img">
                            <a href="/product-card/{{$product->id}}"><img src="/storage/{{$product->cover}}"></a>
                            @if(!empty($product->discount))
                            <span class="catalog-stock">-{{$product->discount}}</span>
                            @endif
                          </div>
                          <h4><a href="/product-card/{{$product->id}}">{{$product->name}}</a> / {{$product->volume}} мл.</h4>
                          <p>{{$product->text_cover}}</p>
                           @if(!empty($product->s_price))
                          <div class="price">{{$product->s_price}} тнг
                            <span>
                            {{$product->f_price}} тнг
                            </span>
                          </div>
                          @else
                          <div class="price">{{$product->f_price}} тнг </div>
                          @endif
                        
                          @php 
                          $rating = \App\Product::calculateRating($product->id);
                          @endphp
                          <div class="star-line">
                            <ul>
                              @for($i = 1; $i <= 5; $i++)
                              @if($i <= $rating)
                              <li style="color: #eeb900">&#9733;</li>
                              @else
                              <li style="color: #d4d4d4;">&#9733;</li>
                              @endif
                              @endfor
                            </ul>
                          </div>
                          <button class="btn-basket" name="btn-basket"     id="{{$product->id}}"   onclick="addToCart(this.id)">
                            В КОРЗИНУ
                          </button>
                        </div>
                        @endforeach
        </div>
					  <!-- Fixing pagination-->                    
                      <div class="show-all-products">
						  
                        <!--<a href="#moreProduct" class="more-btn">ПОКАЗАТЬ ЕЩЁ</a>-->
						  {{ $products->links() }}
						  <span id="currentURL" style="display: none">{{ $url }}</span>
                      </div>
           @endif
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- END ORGANIC CATALOG -->

    <!-- SUSCRIPTION BLOCK -->
    <div class="white-bg-wrapp">
      <div class="subscription-block container wow rotateInUpLeft" data-wow-duration="1s">
        <div class="subscription-absolute">
          <div class="subscription-content">
            <h3>Хотите быть в курсе всех новостей?</h3>
            <span>Подпишитесь на рассылку!</span>
            <div class="subscription-wrapp">
              <div class="sub-social">
                <h4>Следите за нами в соц сетях:</h4>
                <ul class="social-networks">
                   @foreach($links as $link)
                  <li><a href="{{$link->link}}"><img src="/images/{{$link->type}}.png"></a></li>
                  @endforeach
                </ul>
              </div>
              <div class="sub-row">
                <div class="sub-button-input">
                  <form action="/subscription" method="POST">
                    @csrf
                    <input type="email" name="email" placeholder="Введите Электронную почту">
                    <input type="submit" class="sub-btn" value="ПОДПИСАТЬСЯ">
                  </form>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END SUSCRIPTION BLOCK -->
  </main>
  <!-- END MAIN -->
@endsection
