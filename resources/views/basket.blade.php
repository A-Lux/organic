@extends("layout")

@section('body')
<main class="main">
    <!-- BREAD CRUMBS -->
    <ul class="bread-crumbs container">
      <li><a href="/">Главная</a></li>
      <li><a href="#">Корзина</a></li>
    </ul>
    <!-- END BREAD CRUMBS -->

<!-- basket -->
<div class="container-fluid basket">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <p class="basket-title">Корзина</p>
        <div class="promo-box">
        <div>
              <input type="text" placeholder="Промокод" name="promocode" class="promo-input" id="promo">
              <button onclick="getPromo()">Использовать</button>
              </div>
              <div>
              <img src="/images/ajax-loader.gif" id="waitGIF" style="display: none">
              Ваша скидка по промокоду составляет: <span id="discount"></span>
              </div>
              @if(Auth::check())
              @if(!session('bonus_spent'))
                <div id="bonus_container">
                  Баланс бонусов: <span id="user_balance">{{ $balance }}</span>
                  <button onclick="spendBonuses()" id="spend_user_balance" class="btn" style="border-radius: 100em">Потратить бонусы</button>
                </div>
              @endif
            @endif
        </div>
        <form action="/order" method="GET" id="basket_form">
        <div class="box-cart">

          @foreach($products as $product)
          <div class="product-kainar" id="productString{{$product['id']}}">
            <div class="cart-body">
              <img class="product" src="/storage/{{$product['cover']}}">
              <input type="text" name="product_id" value="{{$product['id']}}" hidden="true">
              <p>{{$product["name"]}}</p>
              <div class="quantity">
                <p>Количество</p>
                <div class="input">
                  <input type="number" id="inputAmount{{$product['id']}}" class="form-control" disabled="" value="{{ $product['amount'] }}" name="product[{{$product['id']}}][count]">
                  <span class="up"   onclick="plusAmount({{$product['id']}})"><img src="/images/up.jpg"></span>
                  <span class="down" onclick="minusAmount({{$product['id']}})"><img src="/images/down.jpg"></span>
                </div>
              </div>
              <p id="one_product_price_{{$product['id']}}" hidden="true">{{$product["one_product_price"]}}</p>
              <input type="text" class="price" id="result{{$product['id']}}" name="product[{{$product['id']}}][price]" value='{{$product["one_product_price"]*$product['amount'].' тг.'}}' style="border: 0px;" readonly="true">
              <a class="close" href="#" onclick="eraseCookie({{$product['id']}})"><img src="images/close.jpg"></a>
            </div>
          </div>
          @endforeach

        </div>
        <div class="footer-cart">
          <div class="total-price">
            <p class="total" id="total_sum">Итого : <span id="FinalResult"></span></p>
            <input type="hidden" id="FinalResultInput" name="total_sum"></span></p>
            <!-- <p class="delivery">Доставка : 14 600 руб.</p> -->
            <button type="button" class="buy btn" onclick="eraseProducts()">Очистить корзину</button>

          </div>
          <input type="submit" class="buy btn"  value="Оформить заказ">
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var products = [];
        @foreach($products as $product)
      products.push({
        price: {{ $product->one_product_price }},
        amount: @if($product->amount){{ $product->amount }} @else 0 @endif,
        id: {{ $product->id }}
      });
        @endforeach

    var totalSum = 0;

    @if(isset($discount[0]))
      var discount = {{ $discount[0]->discount }};
    @else
      var discount = 0;
    @endif
function getPromo(){
      if($('#promo').val() != ""){
        document.getElementById('waitGIF').style.display = 'block';
        $.get('/promo/'+$('#promo').val()).done(function(data) {
          if(parseInt(data) != 0){
            if(data == 'no'){
              document.getElementById('waitGIF').style.display = 'none';
              Swal.fire({
                text: "Промокод не найден",
                  type: 'warning',
                  showCancelButton: false,
                  confirmButtonColor: '#ea893a'
              })
            }
            else{
              discount = parseInt(data);
              document.getElementById('discount').innerHTML=discount+'%';
              document.getElementById('waitGIF').style.display = 'none';
              refreshTotalSum();
            }
          }                
          });
      }
          };
          var bonuses = 0;
    function spendBonuses() {
        var bonusContainer = document.getElementById('bonus_container');
        axios.post('spend-bonuses')
            .then(r => r.data.bonuses)
            .then(data => {
                bonusContainer.remove();
                bonuses = data;
                refreshTotalSum();
            });
    }
    function refreshTotalSum(){
      totalSum = 0;
      for (i=0; i<products.length; i++){
        totalSum=totalSum+products[i].price*products[i].amount;
      }

      totalSum = totalSum*(100-discount)/100;
      totalSum = totalSum - bonuses;
      totalSum= Math.round(totalSum);
      document.getElementById('FinalResult').innerHTML=totalSum+' тг.';
      document.getElementById('FinalResultInput').value=totalSum;
       
      // document.getElementById('FormSum').value=totalSum; 
    }
    
    function eraseCookie(id) {   
      document.cookie = 'product'+id+'=; Max-Age=-99999999;';
      var index = getIndexByValue(products, id);
      products[index].amount = 0;
      refreshTotalSum();
      refreshCartNumber();
      $('#productString'+id).remove()
    }
    function eraseProducts() {
      for(i=0; i<products.length; i++){
        for (i=0; i<products.length; i++){
          document.cookie = 'product'+products[i].id+'=; Max-Age=-99999999;';
          products[i].amount=0;
          $('#productString'+products[i].id).remove()
        }
        refreshTotalSum();
        refreshCartNumber();
        document.getElementById('finalResult').innerHTML = '0 тг.';
      }     
    }
    function getIndexByValue(arr, value) {
      for (var i=0, iLen=arr.length; i<iLen; i++) {
      if (arr[i].id == value) return i;
      }
    }

    function changeAmount(id){
      var obj = document.getElementById('inputAmount'+id)
      var index = getIndexByValue(products, id);
      document.getElementById('result'+id).innerHTML = products[index].price*obj.value + ' тг.';
      products[index].amount = obj.value;
      refreshTotalSum();
      setCookie('product'+id, obj.value, 7)
    }

    function plusAmount(id){
      var obj = document.getElementById('inputAmount'+id)
      obj.stepUp()
      var index = getIndexByValue(products, id);
      document.getElementById('result'+id).value = products[index].price*obj.value + ' тг.';
      products[index].amount = products[index].amount + 1;
      refreshTotalSum();
      setCookie('product'+id, obj.value, 7)
    }
    
    function minusAmount(id){
      var obj = document.getElementById('inputAmount'+id)
      if(obj.value>1){
        obj.stepDown()
        var index = getIndexByValue(products, id);
        document.getElementById('result'+id).value = products[index].price*obj.value + ' тг.';
        products[index].amount = products[index].amount - 1;
        refreshTotalSum();
        setCookie('product'+id, obj.value, 7)
      }
    }

    refreshTotalSum();
</script>
</main>
@endsection