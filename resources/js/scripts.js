const spendBalanceBtn = document.getElementById('spend_user_balance');
const bonusContainer = document.getElementById('bonus_container');
const sum = document.getElementById('total');
const oneClickOrderBtn = document.getElementById('one_click_order');
const usePromocodeBtn = document.getElementById('use_promocode_btn');
const promocodeForm = document.getElementById('promocode_form');
const countryDropdown = document.getElementById("country_dropdown");
const regionDropdown = document.getElementById("region_dropdown");
const cityDropdown = document.getElementById("city_dropdown");
const recomededCategories = document.getElementsByClassName('profile_recomended_category');
const recomendedProducts = document.getElementById('products_wrapper');
const saleCategoryBtn = document.getElementsByClassName('sale_category');

if(spendBalanceBtn) {
    spendBalanceBtn.addEventListener('click', e => {
        axios.post('spend-bonuses')
            .then(r => r.data.totalSum)
            .then(data => {
                bonusContainer.remove();
                sum.innerHTML = data;
            })
    });
}

if(oneClickOrderBtn) {
    oneClickOrderBtn.addEventListener('click', e => {
        const data = new FormData(document.getElementById('one_click_form'));
        axios.post('/order/one-click', data)
            .then(r => r.data)
            .then(data => console.log(data));
    });
}

if(usePromocodeBtn) {
    usePromocodeBtn.addEventListener('click', e => {
        e.preventDefault();
        var data = new FormData(promocodeForm);
        axios.post('/use-promocode', data)
            .then(r => r.data.totalSum)
            .then(data => {
                promocodeForm.remove();
                sum.innerHTML = data;
            });
    });
}

// Stars
var rating = document.querySelector('.rating'),
    ratingItem = document.querySelectorAll('.rating-item');

if(rating) {
    rating.onclick = function(e){
        var target = e.target;
        var rateField = document.getElementById('rate_field');
        if(target.classList.contains('rating-item')){
            removeClass(ratingItem,'current-active')
            target.classList.add('active','current-active');
            rateField.value = target.getAttribute('data-rate'); 
        }
    }
      
    rating.onmouseover = function(e) {
        var target = e.target;
        if(target.classList.contains('rating-item')){
            removeClass(ratingItem,'active')
            target.classList.add('active');
            mouseOverActiveClass(ratingItem)
        }
    }
    rating.onmouseout = function(){
        addClass(ratingItem,'active');
        mouseOutActiveClas(ratingItem);
    }
}


function removeClass(arr) {
  for(var i = 0, iLen = arr.length; i <iLen; i ++) {
    for(var j = 1; j < arguments.length; j ++) {
      ratingItem[i].classList.remove(arguments[j]);
    }
  }
}
function addClass(arr) {
  for(var i = 0, iLen = arr.length; i <iLen; i ++) {
    for(var j = 1; j < arguments.length; j ++) {
      ratingItem[i].classList.add(arguments[j]);
    }
  }
}

function mouseOverActiveClass(arr){
  for(var i = 0, iLen = arr.length; i < iLen; i++) {
    if(arr[i].classList.contains('active')){
      break;
    }else {
      arr[i].classList.add('active');
    }
  }
}

function mouseOutActiveClas(arr){
  for(var i = arr.length-1; i >=1; i--) {
    if(arr[i].classList.contains('current-active')){
      break;
    }else {
      arr[i].classList.remove('active');
    }
  }
}

// contryDropdown
if(countryDropdown) {
    countryDropdown.addEventListener('change', e => {
        axios.get(`/regions?country=${e.target.value}`)
            .then(r => r.data)
            .then(data => {
                for (const [i, v] of Array.from(regionDropdown.children).entries()) {
                    if(i != 0) {
                        v.remove();
                    }
                }
                regionDropdown.removeAttribute('disabled');
                if(!cityDropdown.hasAttribute('disabled')) {
                    cityDropdown.setAttribute('disabled', true);
                }
                for (const iterator of data) {
                    const option = document.createElement('option');
                    option.value = iterator.id;
                    option.innerText = iterator.name;
                    regionDropdown.appendChild(option);
                }
            });
    });
}
// cityDropdown
if(regionDropdown) {
    regionDropdown.addEventListener('change', e => {
        axios.get(`/cities?region=${e.target.value}`)
            .then(r => r.data)
            .then(data => {
                for (const [i, v] of Array.from(cityDropdown.children).entries()) {
                    if(i != 0) {
                        v.remove();
                    }
                }
                cityDropdown.removeAttribute('disabled');
                for (const iterator of data) {
                    const option = document.createElement('option');
                    option.value = iterator.id;
                    option.innerText = iterator.name;
                    cityDropdown.appendChild(option);
                }
            });
    });
}

document.addEventListener("DOMContentLoaded", e => {
    if(recomededCategories.length > 0) {
        loadRecomended(recomededCategories[0].id, 'categories')
        for (const v of recomededCategories) {
            v.addEventListener('click', event => {
                loadRecomended(event.target.id, 'categories');
            });
        }
    }

    if(saleCategoryBtn.length > 0) {
        for (const v of saleCategoryBtn) {
            loadRecomended(saleCategoryBtn[0].getAttribute('data-id'), 'sales');
            v.addEventListener('click', event => {
                loadRecomended(event.target.getAttribute('data-id'), 'sales');
            });
        }
    }
});

function loadRecomended(id, url) {
    axios.get(`/${url}/${id}`)
        .then(r => r.data)
        .then(data => {
            recomendedProducts.innerHTML = '';
            for (const iterator of data) {
                recomendedProducts.innerHTML += renderProduct(iterator);
            } 
        });
}

function renderProduct(product) {
    let html = `<div class="catalog-product-item">
        <div class="catalog-img">
        <a href="/product-card/${product.id}"><img class="img-fluid" src="/storage/${product.cover}"></a>
                    </div>
        <h4><a href="/product-card/${product.id}">${product.name}</a> / ${product.volume ? product.volume : ''} мл.</h4>
        <p>${product.text_cover ? product.text_cover : ''}</p>`;
        if(product.s_price) {
            html += `<div class="price">${product.s_price} тнг
                <span>
                ${product.f_price} тнг
                </span>
            </div>`;
        }else {
            html += `<div class="price">${product.f_price} тнг </div>`;
        }
        html += `<button class="btn-basket" name="btn-basket" id="${product.id}" onclick="add_basket(this.id)">
        В КОРЗИНУ
        </button>
    </div>`;

    return html;
}